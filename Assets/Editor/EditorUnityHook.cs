﻿using System.Reflection;
using UnityEditor;

[InitializeOnLoad]
public static class EditorUnityHook
{
    static EditorUnityHook()
    {
        var t = typeof ( EditorGUI );
        var attr = BindingFlags.Static | BindingFlags.NonPublic;
        t.GetField( "kFloatFieldFormatString", attr ).SetValue( null, "0.#######" );
        t.GetField( "kDoubleFieldFormatString", attr ).SetValue( null, "0.###############" );
    }
}