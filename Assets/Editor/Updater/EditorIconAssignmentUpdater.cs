using System;
using System.IO;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Main;

/// <summary>
/// アイコンを自動で設定
/// </summary>
public class EditorIconAssignmentUpdater : AssetPostprocessor
{
    private const string IOS_ROOT_PATH = "Assets/Icon/ios/AppIcon.appiconset/";
    private const string ANDROID_ROOT_PATH = "Assets/Icon/android/";

    private readonly static string[] IOS_ICON_NAME = new string[]
    {
        "Icon-App-60x60@3x.png",
        "Icon-App-60x60@2x.png",
        "Icon-App-83.5x83.5@2x.png",
        "Icon-App-76x76@2x.png",
        "Icon-App-76x76@1x.png",
        "Icon-App-40x40@3x.png",
        "Icon-App-40x40@2x.png",
        "Icon-App-40x40@2x.png",
        "Icon-App-40x40@1x.png",
        "Icon-App-29x29@3x.png",
        "Icon-App-29x29@2x.png",
        "Icon-App-29x29@1x.png",
        "Icon-App-29x29@2x.png",
        "Icon-App-29x29@1x.png",
        "Icon-App-20x20@3x.png",
        "Icon-App-20x20@2x.png",
        "Icon-App-20x20@2x.png",
        "Icon-App-20x20@1x.png",
        "ItunesArtwork@2x.png",
    };

    private readonly static string[] ANDROID_ICON_NAME = new string[]
    {
        "mipmap-xxxhdpi/ic_launcher.png",
        "mipmap-xxhdpi/ic_launcher.png",
        "mipmap-xhdpi/ic_launcher.png",
        "mipmap-hdpi/ic_launcher.png",
        "mipmap-mdpi/ic_launcher.png",
        "mipmap-ldpi/ic_launcher.png",
    };

    private static void OnPostprocessAllAssets (string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromAssetPaths)
    {
        for (int i = 0; i < importedAssets.Length; i++)
        {
            if (importedAssets[i].IsMatch(IOS_ROOT_PATH) | importedAssets[i].IsMatch(ANDROID_ROOT_PATH))
            {
                SetIcons();
            }
        }
    }

    [MenuItem("Custom/Update/Icons Set PlayerSettings")]
    private static void SetIcons()
    {
        SetPlatformIcons(BuildTargetGroup.iOS);
        SetPlatformIcons(BuildTargetGroup.Android);

        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
    }

    private static void SetPlatformIcons(BuildTargetGroup platform)
    {
        #if UNITY_2019_OR_NEWER
        var textures = new List<Texture2D>();

        var iconSizes = PlayerSettings.GetIconSizesForTargetGroup(platform);
        var getIcons = PlayerSettings.GetIconsForTargetGroup(platform);

        for (int i = 0; i < iconSizes.Length; i++)
        {
            var t = null as Texture2D;
            var path = platform == BuildTargetGroup.iOS ? 
                $"{IOS_ROOT_PATH}{IOS_ICON_NAME[i]}" : 
                $"{ANDROID_ROOT_PATH}{ANDROID_ICON_NAME[i]}";

            if (File.Exists(path))
            {
                if (getIcons[i] == null)
                    t = AssetDatabase.LoadAssetAtPath(path, typeof(Texture2D)) as Texture2D;
                else
                    t = getIcons[i];
            }

            textures.Add(t);
        }

        PlayerSettings.SetIconsForTargetGroup(platform, textures.ToArray());
        #endif
    }
}
