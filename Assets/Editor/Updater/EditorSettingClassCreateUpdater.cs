﻿/*ブログ説明用
DirectoryPath.TOP_RESOURCES = "Assets/Resources/";
FilePath.SCENE_DATA = "SceneData"
Extension.ASSET = ".asset";
*/

using System;
using System.IO;
using System.Text;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

/// <summary>
/// タグ、レイヤー、シーン、ソーティングレイヤー名を定数で管理するクラスを自動で作成するスクリプト
/// </summary>
public class EditorSettingClassCreateUpdater : AssetPostprocessor
{
  //変更を監視するディレクトリ名
  private const string TARGET_DIRECTORY_NAME = "ProjectSettings";

  //コマンド名
  private const string COMMAND_NAME = "Custom/Update/Create Setting Class";

  static bool ExistsDirectoryInAssets(List<string[]> assetsList, List<string> targetDirectoryNameList)
  {
    return assetsList
      .Any (assets => assets                                       //入力されたassetsListに以下の条件を満たすか要素が含まれているか判定
      .Select (asset => System.IO.Path.GetDirectoryName (asset))   //assetsに含まれているファイルのディレクトリ名だけをリストにして取得
      .Intersect (targetDirectoryNameList)                         //上記のリストと入力されたディレクトリ名のリストの一致している物のリストを取得
      .Count () > 0);                                              //一致している物があるか
  }

  //ProjectSettings以下の設定が編集されたら自動で各スクリプトを作成
  private static void OnPostprocessAllAssets (string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromAssetPaths)
  {
    List<string[]> assetsList = new List<string[]> ()
    {
      importedAssets
    };

    List<string> targetDirectoryNameList = new List<string> ()
    {
      TARGET_DIRECTORY_NAME
    };

    if(ExistsDirectoryInAssets(assetsList, targetDirectoryNameList))
    {
      Create ();
    }
  }

  //スクリプトを作成します
  [MenuItem(COMMAND_NAME)]
  private static void Create()
  {
    //タグ
    Dictionary<string, string> tagDic = InternalEditorUtility.tags.ToDictionary(value => value);
    ConstantsClassCreator.Create ("TagName", "タグ名を定数で管理するクラス", tagDic);

    //シーン
    Dictionary<string, string> scenesNameDic = new Dictionary<string, string>();
    Dictionary<string, int>    scenesNoDic   = new Dictionary<string, int>();

    for(int i = 0; i < EditorBuildSettings.scenes.Count(); i++)
    {
      string sceneName = Path.GetFileNameWithoutExtension (EditorBuildSettings.scenes [i].path);
      scenesNameDic [sceneName] = sceneName;
      scenesNoDic   [sceneName] = i;
    }
    ConstantsClassCreator.Create ("SceneName", "シーン名を定数で管理するクラス",  scenesNameDic);
    ConstantsClassCreator.Create ("SceneNo"  , "シーン番号を定数で管理するクラス", scenesNoDic);

    //レイヤーとレイヤーマスク
    Dictionary<string, int> layerNoDic     = InternalEditorUtility.layers.ToDictionary(layer => layer, layer => LayerMask.NameToLayer (layer));
    Dictionary<string, int> layerMaskNoDic = InternalEditorUtility.layers.ToDictionary(layer => layer, layer => 1 <<  LayerMask.NameToLayer (layer));
    ConstantsClassCreator.Create ("LayerNo",     "レイヤー番号を定数で管理するクラス",      layerNoDic);
    ConstantsClassCreator.Create ("LayerMaskNo", "レイヤーマスク番号を定数で管理するクラス", layerMaskNoDic);

    //ソーティングレイヤー
    Dictionary<string, string> sortingLayerDic = GetSortingLayerNames ().ToDictionary(value => value);
    ConstantsClassCreator.Create ("SortingLayerName", "ソーティングレイヤー名を定数で管理するクラス", sortingLayerDic);

    Debug.Log ("定数を管理するクラスの作成が完了しました");

#if UseGA
    var settings = Resources.Load<GameAnalyticsSDK.Setup.Settings>("GameAnalytics/Settings");

    if(settings == null) return;

    var isChange = false;
    var ver = PlayerSettings.bundleVersion;

    for(int i = 0; i < settings.Platforms.Count; i++)
    {
      if(settings.Build[i] != ver)
      {
          settings.Build[i] = ver;
          isChange = true;
      }
    }

    if(isChange)
    {
      Debug.Log("GameAnalyticsの設定を更新しました。");
      EditorUtility.SetDirty(settings);
    }
#endif
  }

  //sortinglayerの名前一覧を取得
  private static string[] GetSortingLayerNames() 
  {
    Type internalEditorUtilityType = typeof(InternalEditorUtility);
    PropertyInfo sortingLayersProperty = internalEditorUtilityType.GetProperty("sortingLayerNames", BindingFlags.Static | BindingFlags.NonPublic);
    return (string[])sortingLayersProperty.GetValue(null, new object[0]);
  }
}