﻿using System;
using System.IO;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[InitializeOnLoad] // 起動時に実行.
public class EditorScriptableObjectCreator : EditorWindow
{
    const string InputPath = "Assets/Scripts/Data"; // スクリプトの入力パス.
    const string OutputPath = "Assets/Resources/Data"; // アセットの出力パス.

    const string ScriptExtension = ".cs"; // スクリプトの拡張子.
    const string AssetExtension = ".asset"; // アセットの拡張子.

    // // コンストラクタ（起動時に呼び出される）.
    // static EditorScriptableObjectCreator()
    // {
    //     UpdateAssets();
    // }

    // // アセット更新時に実行.
    // public static void OnPostprocessAllAssets(string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromAssetsPath)
    // {
    //     UpdateAssets();
    // }

    // メニューにアイテムを追加.
    [MenuItem("Custom/Create ScriptableObject")]
    private static void Create()
    {
        CheckFile();
        
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
    }

    // 入力元をチェック.
    private static void CheckFile()
    {
        // ファイルパスの配列.
        var files = Directory.GetFiles(InputPath, $"*{ScriptExtension}", SearchOption.AllDirectories)
            .ToList()
            .FindAll(x => Path.GetExtension(x) == ScriptExtension)
            .FindAll(x => EditorTypeUtils.GetType( Path.GetFileNameWithoutExtension(x) ).IsSubclassOf( typeof(ScriptableObject) ));

        UpdateAsset(files);
    }

    private static void DeleteAsset(List<string> files)
    {
        var assets = Directory.GetFiles(OutputPath, $"*{AssetExtension}", SearchOption.AllDirectories)
            .ToList()
            .FindAll(x => Path.GetExtension(x) == AssetExtension);

        foreach (var asset in assets)
        {
            string assetName = Path.GetFileNameWithoutExtension(asset);
            if (!files.Exists(x => Path.GetFileNameWithoutExtension(x) == assetName))
            {
                AssetDatabase.DeleteAsset(asset);

                Debug.Log("ScriptableObject Deleteed : " + asset);
            }
        }
    }

    // アセットを生成.
    private static void UpdateAsset(List<string> files)
    {
        // フォルダが存在しなかったら作成
        if (!Directory.Exists(OutputPath))
        {
            Directory.CreateDirectory(OutputPath);
        }

        DeleteAsset(files);

        foreach (var targetPath in files)
        {
            string assetName = Path.GetFileNameWithoutExtension(targetPath);
            string fileName = assetName + AssetExtension;
            string exportPath = $"{OutputPath}/{fileName}";
            
            if (!File.Exists(exportPath))
            {
                var obj = ScriptableObject.CreateInstance(assetName);
                AssetDatabase.CreateAsset(obj, exportPath);

                Debug.Log("ScriptableObject Created : " + exportPath);
            }
            else
            {
                var asset = AssetDatabase.LoadAssetAtPath(exportPath, EditorTypeUtils.GetType(assetName));
                EditorUtility.SetDirty(asset);

                Debug.Log("ScriptableObject Update : " + exportPath);
            }
        }
    }
}
