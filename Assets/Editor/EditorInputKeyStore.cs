﻿using System;
using System.IO;
using UnityEngine;
using UnityEditor;
using UnityEditor.Build;

[InitializeOnLoad]
public class EditorInputKeyStore
{
    public const string KeystorePathName = "user.keystore";
    public const string KeystorePass = "";
    public const string KeyaliasPass = "";
    public const string KeyaliasName = "";

    static EditorInputKeyStore()
    {
        InputKey();
    }

    [MenuItem("Custom/Input KeyStore")]
    static void InputKey()
    {
        string pathName = Directory.GetCurrentDirectory() + "/" + KeystorePathName;

        if(!File.Exists(pathName))
        {
            Debug.Log("keystoreファイルがありません。");
            return;
        }

        if(
            string.IsNullOrWhiteSpace(KeystorePass) || 
            string.IsNullOrWhiteSpace(KeyaliasPass) || 
            string.IsNullOrEmpty(KeyaliasName))
        {
            Debug.LogError("keystoreのAlias名またはパスワードが不正な文字列です。");
            return;
        }

#if UNITY_2019_OR_NEWER
        PlayerSettings.Android.useCustomKeystore = true;
#endif

        PlayerSettings.Android.keystoreName = pathName;
        PlayerSettings.Android.keystorePass = KeystorePass;
        PlayerSettings.Android.keyaliasName = KeyaliasName;
        PlayerSettings.Android.keyaliasPass = KeyaliasPass;
    }
}