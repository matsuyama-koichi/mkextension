﻿using UnityEditor;
using UnityEngine;

public static class EditorHierarchySetActive
{
    private const int WIDTH = 16;
    private const int MARGIN_X = 20;

    [InitializeOnLoadMethod]
    private static void Show()
    {
        EditorApplication.hierarchyWindowItemOnGUI += OnGUI;
    }
    
    private static void OnGUI( int instanceID, Rect selectionRect )
    {
        var go = EditorUtility.InstanceIDToObject( instanceID ) as GameObject;

        if ( go == null )
        {
            return;
        }

        var pos     = selectionRect;
        pos.x       = pos.xMax - WIDTH - MARGIN_X;
        pos.width   = WIDTH;

        var newActive = GUI.Toggle( pos, go.activeSelf, string.Empty );

        if ( newActive == go.activeSelf )
        {
            return;
        }

        go.SetActive( newActive );
    }
}