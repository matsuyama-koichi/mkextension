﻿//  SceneOperationWindow.cs
//  http://kan-kikuchi.hatenablog.com/entry/SceneOperationWindow
//
//  Created by kan.kikuchi on 2019.02.05.

using System.IO;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEditor;
using UnityEditor.SceneManagement;

/// <summary>
/// エディタ上でシーンの変更や再生、選択を行うためのクラス
/// </summary>
public class EditorSceneOperation : EditorWindow {

  //スクロール位置
  private Vector2 _scrollPosition = Vector2.zero;

  //=================================================================================
  //初期化
  //=================================================================================

  //メニューからウィンドウを表示
  [MenuItem("Custom/Scene Operation Window", false, 200)]
  public static void Open() {
    var window = GetWindow(typeof(EditorSceneOperation));
    window.minSize = new Vector2(600f, 200f);
  }

  //=================================================================================
  //表示するGUIの設定
  //=================================================================================

  private void OnGUI() {
    //描画範囲が足りなければスクロール出来るように
    _scrollPosition = EditorGUILayout.BeginScrollView(_scrollPosition, GUI.skin.scrollView);

    //ビルドに設定されているシーンを取得
    List<string> settingScenePathList = EditorBuildSettings.scenes.Select(settingScene => settingScene.path).ToList<string>();

    //シーンのアセットのGUIDを全て取得、さらにGUIDからパスに変換しListにする(ビルドに設定されているシーンは除く)
    List<string> scenePathList = AssetDatabase.FindAssets("t:SceneAsset")
                                              .Select(guid => AssetDatabase.GUIDToAssetPath(guid))
                                              .Where(path => !settingScenePathList.Contains(path))
                                              .ToList<string>();

    //シーンごとにUIを表示する
    EditorGUILayout.BeginVertical(GUI.skin.box);

    ShowScenesUI("ビルドに設定されているシーン", settingScenePathList, isSettingScene: true);
    GUILayout.Space(10);
    ShowScenesUI("それ以外のシーン", scenePathList, isSettingScene: false);

    EditorGUILayout.EndVertical();

    //スクロール箇所終了
    EditorGUILayout.EndScrollView();
  }

  //複数のシーンごとのUIを表示
  private void ShowScenesUI(string tilte, List<string> scenePathList, bool isSettingScene) {
    EditorGUILayout.BeginVertical(GUI.skin.box);
    EditorGUILayout.LabelField(tilte);

    foreach (string scenePath in scenePathList) {
      EditorGUILayout.BeginHorizontal(GUI.skin.box);
      ShowSceneUI(scenePath, isSettingScene);
      EditorGUILayout.EndHorizontal();
    }

    EditorGUILayout.EndVertical();
  }

  //シーンごとのUIを表示
  private void ShowSceneUI(string scenePath, bool isSettingScene) {
    //シーン名を表示
    EditorGUILayout.LabelField(Path.GetFileNameWithoutExtension(scenePath), GUILayout.Width(80));

    GUILayout.Space(10);

    //押したらそのシーンを初期シーンとして、エディタの再生を開始するボタン
    if (!Application.isPlaying && GUILayout.Button("再生", GUILayout.Width(70))) {
      EditorPlayer.Play(scenePath);
    }
    //エディタ実行中は設定に含まれているシーンは移動用のボタンを表示し、それ以外はボタンを表示しないように
    if (Application.isPlaying) {
      if (isSettingScene && GUILayout.Button("移動", GUILayout.Width(70))) {
        SceneManager.LoadScene(Path.GetFileNameWithoutExtension(scenePath));
      }
      else if (!isSettingScene) {
#if UNITY_2018_3_OR_NEWER
        if (GUILayout.Button("移動", GUILayout.Width(70))) {
          EditorSceneManager.LoadSceneInPlayMode(scenePath, new LoadSceneParameters(LoadSceneMode.Single));
        }
#else
        EditorGUILayout.LabelField("　再生", GUILayout.Width(70));
#endif
      }
    }

    GUILayout.Space(10);

    //シーンをエディタ上で開くボタン(エディタ実行中は使えないように)
    if(Application.isPlaying){
      EditorGUILayout.LabelField("　読み込み", GUILayout.Width(70));
    }
    else if (GUILayout.Button("読み込み", GUILayout.Width(70))) {
      //現在のシーンに変更があった場合、保存するか確認のウィンドウを出す(キャンセルされたら読み込みをしない)
      if (!EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo()) {
        return;
      }
      EditorSceneManager.OpenScene(scenePath);
    }

    GUILayout.Space(10);

    //Project上で該当シーンを選択するボタン
    if (GUILayout.Button("選択", GUILayout.Width(70))) {
      Selection.activeObject = AssetDatabase.LoadAssetAtPath<SceneAsset>(scenePath);
    }

    GUILayout.Space(10);

    //パスを表示
    EditorGUILayout.LabelField(scenePath);
  }

}