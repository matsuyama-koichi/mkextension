﻿using UnityEditor;
using UnityEngine;

public static class EditorHierarchyLockInspector
{
    private const int WIDTH = 16;

    [InitializeOnLoadMethod]
    private static void Show()
    {
        EditorApplication.hierarchyWindowItemOnGUI += OnGUI;
    }
    
    private static void OnGUI( int instanceID, Rect selectionRect )
    {
        var go = EditorUtility.InstanceIDToObject( instanceID ) as GameObject;

        if ( go == null )
        {
            return;
        }

        var pos     = selectionRect;
        pos.x       = pos.xMax - WIDTH;
        pos.width   = WIDTH;

        var oldLock = ( go.hideFlags & HideFlags.NotEditable ) != 0;
        var newLock = GUI.Toggle( pos, oldLock, string.Empty, "IN LockButton" );

        if ( newLock == oldLock )
        {
            return;
        }

        if ( newLock )
        {
            go.hideFlags |= HideFlags.NotEditable;
        }
        else
        {
            go.hideFlags &= ~HideFlags.NotEditable;
        }
    }
}