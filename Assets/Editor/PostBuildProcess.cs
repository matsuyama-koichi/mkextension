﻿#define DefaultLanguageJapanese
using UnityEngine;
using UnityEditor;
using UnityEditor.iOS.Xcode;
using UnityEditor.iOS.Xcode.Extensions;
using UnityEditor.Callbacks;
using System.IO;
using System.Collections.Generic;
using System.Xml;
using System.Linq;
using System.Text;

#if UNITY_IOS
public class PostBuildProcess
{
    [PostProcessBuild]
    public static void OnPostProcessBuild(BuildTarget buildTarget, string path)
    {
        if (buildTarget == BuildTarget.iOS)
        {
            Reset(path);

            string projPath = PBXProject.GetPBXProjectPath(path);
            PBXProject proj = new PBXProject();

            proj.ReadFromString(File.ReadAllText(projPath));
#if UNITY_2019_3_OR_NEWER
            string target = proj.GetUnityFrameworkTargetGuid();
#else
            string target = proj.TargetGuidByName("Unity-iPhone");
#endif

            // add system framework
            proj.AddFrameworkToProject(target, "CoreTelephony.framework", false);
            proj.AddFrameworkToProject(target, "Social.framework", false);
            proj.AddFrameworkToProject(target, "StoreKit.framework", false);
            proj.AddFrameworkToProject(target, "EventKit.framework", false);
            proj.AddFrameworkToProject(target, "EventKitUI.framework", false);
            proj.AddFrameworkToProject(target, "AdSupport.framework", true);
            //  AdColony
            if (_IsEnableAdcolonyIronSource)
            {
                proj.AddFrameworkToProject(target, "AdSupport.framework", true);
                proj.AddFrameworkToProject(target, "AudioToolbox.framework", false);
                proj.AddFrameworkToProject(target, "AVFoundation.framework", false);
                proj.AddFrameworkToProject(target, "CoreGraphics.framework", false);
                proj.AddFrameworkToProject(target, "CoreMedia.framework", false);
                proj.AddFrameworkToProject(target, "CoreTelephony.framework", false);
                proj.AddFrameworkToProject(target, "EventKit.framework", false);
                proj.AddFrameworkToProject(target, "EventKitUI.framework", false);
                proj.AddFrameworkToProject(target, "MediaPlayer.framework", false);
                proj.AddFrameworkToProject(target, "MessageUI.framework", false);
                proj.AddFrameworkToProject(target, "QuartzCore.framework", false);
                proj.AddFrameworkToProject(target, "Social.framework", false);
                proj.AddFrameworkToProject(target, "StoreKit.framework", false);
                proj.AddFrameworkToProject(target, "SystemConfiguration.framework", false);
                proj.AddFrameworkToProject(target, "WebKit.framework", false);
            }
            // InMobi
            if (_IsEnableInMobi)
            {
                proj.AddFileToBuild(target, proj.AddFile("usr/lib/libxml2.2.tbd", "Frameworks/libxml2.2.tbd", PBXSourceTree.Sdk));
            }
#if UseTenjin
            proj.AddFrameworkToProject(target, "AdSupport.framework", true);
            proj.AddFrameworkToProject(target, "iAd.framework", false);
            proj.AddFrameworkToProject(target, "StoreKit.framework", false);
#endif

            // add framework
            Directory.GetDirectories(path.Substring(0, path.LastIndexOf("/")) + "/Framework", "*.framework").ToList().ForEach(file =>
            {
                var name = file.Substring(file.LastIndexOf("/") + 1);
                proj.AddFileToBuild(target, proj.AddFile(file, $"Frameworks/{name}", PBXSourceTree.Source));
            });
            Directory.GetDirectories(path.Substring(0, path.LastIndexOf("/")) + "/Framework", "*.bundle").ToList().ForEach(file =>
            {
                var name = file.Substring(file.LastIndexOf("/") + 1);
                proj.AddFileToBuild(target, proj.AddFile(file, $"Frameworks/{name}", PBXSourceTree.Source));
            });

            // add embedded
            AddEmbedded(ref proj, target, path);

            // localization
            AddLocalization(ref proj, path);

            // add framework path
            proj.AddBuildProperty(target, "FRAMEWORK_SEARCH_PATHS", "$(PROJECT_DIR)/../Framework");

            //  add tbd
            proj.AddFileToBuild(target, proj.AddFile("usr/lib/libsqlite3.0.tbd", "Frameworks/libsqlite3.0.tbd", PBXSourceTree.Sdk));
            proj.AddFileToBuild(target, proj.AddFile("usr/lib/libz.1.2.5.tbd", "Frameworks/libz.1.2.5.tbd", PBXSourceTree.Sdk));

            //  add -ovjc
            proj.AddBuildProperty(target, "OTHER_LDFLAGS", "-ObjC");

            //  BITCODE NO
            proj.SetBuildProperty(target, "ENABLE_BITCODE", "NO");

            //  CLANG_ENABLE_MODULES
            proj.SetBuildProperty(target, "CLANG_ENABLE_MODULES", "YES");

            // capability
            AddCapability(ref proj, target);

            //  Info.plist
            SetInfoplist(path);

            // output
            var textProj = proj.WriteToString();
            File.WriteAllText(projPath, textProj);
        }
    }

#region flag
    internal static bool _IsEnablePurchase;
    internal static bool _IsEnableRemoteNotification;
    internal static bool _IsEnableAdcolonyIronSource;
    internal static bool _IsEnableAdmob;
    internal static bool _IsEnableAppLovin;
    internal static bool _IsEnableInMobi;
    internal static bool _IsEbableIronSource;

    internal static void Reset(string path)
    {
        var root = path.Substring(0, path.LastIndexOf("/"));

        _IsEnablePurchase = Directory.Exists($"{root}/Assets/Plugins/UnityPurchasing");
        _IsEnableRemoteNotification = Directory.Exists($"{root}/Assets/OneSignal");
        _IsEnableAdcolonyIronSource = File.Exists($"{root}/Assets/IronSource/Editor/ISAdColonyAdapterDependencies.xml");
        _IsEnableAdmob = File.Exists($"{root}/Assets/IronSource/Editor/ISAdMobAdapterDependencies.xml");
        _IsEnableAppLovin = File.Exists($"{root}/Assets/IronSource/Editor/ISAppLovinAdapterDependencies.xml");
        _IsEnableInMobi = Directory.Exists($"{root}/Assets/IronSource/Editor/ISInMobiAdapterDependencies.xml");
        _IsEbableIronSource = Directory.Exists($"{root}/Assets/IronSource");
    }
#endregion

    internal static void CopyAndReplaceDirectory(string srcPath, string dstPath)
    {
        try
        {
            if (Directory.Exists(dstPath))
                Directory.Delete(dstPath);
            if (File.Exists(dstPath))
                File.Delete(dstPath);

            Directory.CreateDirectory(dstPath);

            foreach (var file in Directory.GetFiles(srcPath))
                File.Copy(file, Path.Combine(dstPath, Path.GetFileName(file)));

            foreach (var dir in Directory.GetDirectories(srcPath))
                CopyAndReplaceDirectory(dir, Path.Combine(dstPath, Path.GetFileName(dir)));
        }
        catch { }
    }

    internal static void AddEmbedded(ref PBXProject proj, string target, string path)
    {
        var dir = path.Substring(0, path.LastIndexOf("/")) + "/Framework/Embedded";
        if (!Directory.Exists(dir)) return;

        var files = Directory.GetDirectories(dir, "*.framework");
        foreach (var file in files)
        {
            var name = file.Substring(file.LastIndexOf("/") + 1);
            string fileGuid = proj.AddFile(file, $"Frameworks/{name}", PBXSourceTree.Source);
            proj.AddFileToBuild(target, fileGuid);

            string embedPhase = proj.AddCopyFilesBuildPhase(target, "Embed Frameworks", "", "10");
            proj.AddFileToBuildSection(target, embedPhase, fileGuid);
            PBXProjectExtensions.AddFileToEmbedFrameworks(proj, target, fileGuid);
            proj.AddBuildProperty(target, "LD_RUNPATH_SEARCH_PATHS", "$(inherited) @executable_path/Frameworks");
            proj.AddBuildProperty(target, "FRAMEWORK_SEARCH_PATHS", "$(SRCROOT)/Frameworks/");
        }

        //  ALWAYS_EMBED_SWIFT_STANDARD_LIBRARIES
        proj.SetBuildProperty(target, "ALWAYS_EMBED_SWIFT_STANDARD_LIBRARIES", "YES");
    }

    internal static void SetInfoplist(string path)
    {
        const string KeyType = "CFBundleURLTypes";
        const string KeyScheme = "CFBundleURLSchemes";
        const string KeyLSApp = "LSApplicationQueriesSchemes";
        const string KeyLocalization = "CFBundleDevelopmentRegion";

        var path_info = Path.Combine(path, "Info.plist");
        PlistDocument plist = new PlistDocument();
        plist.ReadFromFile(path_info);

#if false
        // url scheme
        PlistElementArray bundleURLTypesArray = plist.root[KeyType] as PlistElementArray;
        if (bundleURLTypesArray == null)
        {
            bundleURLTypesArray = plist.root.CreateArray(KeyType);
        }
        PlistElementDict dict = bundleURLTypesArray.AddDict();
        PlistElementArray bundleURLSchemesArray = dict.CreateArray(KeyScheme);
        var id = Application.identifier;
        id = id.Remove(0, id.LastIndexOf(".") + 1);
        bundleURLSchemesArray.AddString(id);
#endif

        // AdColony for IronSource
        if (_IsEnableAdcolonyIronSource)
        {
            var queriesSchemes = null as PlistElementArray;
            if (plist.root.values.ContainsKey(KeyLSApp))
                queriesSchemes = plist.root.values[KeyLSApp] as PlistElementArray;
            else
                queriesSchemes = plist.root.CreateArray(KeyLSApp);
            queriesSchemes.AddString("fb");
            queriesSchemes.AddString("instagram");
            queriesSchemes.AddString("tumblr");
            queriesSchemes.AddString("twitter");
        }

        var privacyDict = new Dictionary<string, string>()
        {
            { "NSPhotoLibraryUsageDescription",    "Taking selfies" },
            { "NSPhotoLibraryAddUsageDescription", "Taking selfies" },
            { "NSLocationWhenInUseUsageDescription", "Adding events" },
            { "NSCalendarsUsageDescription", "Adding events" },
            { "NSCameraUsageDescription", "Taking selfies" },
            { "NSMotionUsageDescription ", "Interactive ad controls" },
        };

        if (_IsEbableIronSource)
        {
            privacyDict.Add( "SKAdNetworkIdentifier ", "SU67R6K2V3.skadnetwork" );

            const string Key = "NSAppTransportSecurity";
            if (plist.root[Key] == null)
                plist.root.CreateDict(Key);
            var sec = plist.root[Key].AsDict();
            sec.SetBoolean("NSAllowsArbitraryLoads", true);
        }

        foreach (var kvp in privacyDict)
        {
            if (plist.root[kvp.Key] == null)
            {
                plist.root.SetString(kvp.Key, kvp.Value);
            }
        }

#if DefaultLanguageJapanese
        // default language
        plist.root.SetString(KeyLocalization, "ja_JP");
#endif

        // remote notification
        if (_IsEnableRemoteNotification)
        {
            const string Key = "UIBackgroundModes";
            if (!plist.root.values.ContainsKey(Key))
                plist.root.CreateArray(Key);
            plist.root[Key].AsArray().AddString("remote-notification");
        }

        if (_IsEnableAdmob)
        {
            const string Key = "GADApplicationIdentifier";
            if (!plist.root.values.ContainsKey(Key))
            {
                plist.root.SetString(Key, DefineSdk.IsAdmobAppId);
            }
        }
#if false
        if (_IsEnableAppLovin)
        {
            if (plist.root["AppLovinSdkKey"] == null)
            {
                plist.root.SetString("AppLovinSdkKey", App.DefineSdk.AppLovinKey);
            }
        }
#endif
        // add App Uses Non-Exempt Encryption
        const string KeyExitsITSAppUsesNonExemptEncryption = "ITSAppUsesNonExemptEncryption";
        if (!plist.root.values.ContainsKey(KeyExitsITSAppUsesNonExemptEncryption))
        {
            plist.root.SetBoolean(KeyExitsITSAppUsesNonExemptEncryption, false);
        }
        // remove ExitsOnSuspend
        const string KeyExitsOnSuspend = "UIApplicationExitsOnSuspend";
        if (plist.root.values.ContainsKey(KeyExitsOnSuspend))
        {
            plist.root.values.Remove(KeyExitsOnSuspend);
        }
        // remove UIRequiredDeviceCapabilities
        const string KeyRequiredDeviceCapabilities = "UIRequiredDeviceCapabilities";
        if (plist.root.values.ContainsKey(KeyRequiredDeviceCapabilities))
        {
            plist.root.values.Remove(KeyRequiredDeviceCapabilities);
        }

        plist.WriteToFile(path_info);
    }

    internal static void AddCapability(ref PBXProject proj, string target)
    {
        if (_IsEnablePurchase)
            proj.AddCapability(target, PBXCapabilityType.InAppPurchase);
        if (_IsEnableRemoteNotification)
        {
            proj.AddCapability(target, PBXCapabilityType.PushNotifications);
            proj.AddCapability(target, PBXCapabilityType.BackgroundModes);
        }
    }

    internal static void AddLocalization(ref PBXProject proj, string path)
    {
        var directory = path.Substring(0, path.LastIndexOf("/")) + "/Localization";
        if (!Directory.Exists(directory)) return;

        var filedic = new Dictionary<string, string>();
        var files = Directory.GetDirectories(directory, "*");
        files.ToList().ForEach(x =>
        {
            var name = x.Replace(@"\", "/");
            name = name.Substring(name.LastIndexOf("/") + 1);
            name = name.Substring(0, name.IndexOf("."));
            filedic.Add(name, GetGuid());
        });

        var text = proj.WriteToString();
        var variantGroup = GetGuid();
        var infoplistguid = GetGuid();

        // add file
        var pos = text.IndexOf("/* End PBXFileReference section */");
        if (pos >= 0)
        {
            var add = "";
            filedic.ToList().ForEach(x =>
                add += $"\n\t\t{x.Value} /* {x.Key} */ = {{ isa = PBXFileReference; lastKnownFileType = text.plist.strings; name = {x.Key}; path = ../Localization/{x.Key}.lproj/InfoPlist.strings; sourceTree = \"<group>\"; }};");

            // infoplist
            add += $"\n\t\t{infoplistguid} /* InfoPlist.strings in Resources */ = {{isa = PBXBuildFile; fileRef = {variantGroup} /* InfoPlist.strings */; }};";

            pos = text.LastIndexOf("};", pos) + "};".Length;
            text = text.Insert(pos, add);
        }

        // PBXVariantGroup
        pos = text.IndexOf("PBXVariantGroup");
        if (pos >= 0)
        {
            var add = $"\n\t\t{variantGroup} /* InfoPlist.strings */ = {{\n\t\t\tisa = PBXVariantGroup;\n\t\t\tchildren = (\n";
            filedic.ToList().ForEach(x => add += $"\t\t\t\t{x.Value} /* {x.Key} */,\n");
            add += $"\t\t\t);\n\t\t\tname = InfoPlist.strings;\n\t\t\tsourceTree = \"<group>\";\n\t\t}};\n";
            pos = text.IndexOf("};", pos) + "};".Length;
            text = text.Insert(pos, add);
        }

        // CustomTemplate
        pos = text.IndexOf("name = CustomTemplate;");
        if (pos >= 0)
        {
            pos = text.LastIndexOf(",", pos) + ",".Length;
            var add = $"\n\t\t\t\t{variantGroup} /* InfoPlist.strings */,";
            text = text.Insert(pos, add);
        }

        // PBXResourcesBuildPhase
        pos = text.IndexOf("isa = PBXResourcesBuildPhase;");
        if (pos >= 0)
        {
            pos = text.IndexOf(");", pos);
            pos = text.LastIndexOf(",", pos) + ",".Length;
            var add = $"\n\t\t\t\t{infoplistguid} /* InfoPlist.strings in Resources */,";
            text = text.Insert(pos, add);
        }

        proj.ReadFromString(text);
    }

    static string GetGuid()
    {
        return System.Guid.NewGuid().ToString("N").Substring(8).ToUpper();
    }
}

// #elif UNITY_ANDROID
// using UnityEditor.Android;
// public class PostBuildProcessor : IPostGenerateGradleAndroidProject
// {
//     public int callbackOrder => 999;

//     void IPostGenerateGradleAndroidProject.OnPostGenerateGradleAndroidProject(string path)
//     {
//         var gradlePropertiesFile = path + "/gradle.properties";
//         if (File.Exists(gradlePropertiesFile))
//             File.Delete(gradlePropertiesFile);

//         var writer = File.CreateText(gradlePropertiesFile);
//         writer.WriteLine("org.gradle.jvmargs=-Xmx4096M");
//         writer.WriteLine("org.gradle.parallel=true");
//         writer.WriteLine("android.useAndroidX=true");
//         writer.WriteLine("android.enableJetifier=true");
//         writer.Flush();
//         writer.Close();

// #if UNITY_2019_3_OR_NEWER
//         var launcherGradlePropertiesFile = path + "/../launcher/build.gradle";
//         var text = "";
//         using (var fs = File.OpenRead(launcherGradlePropertiesFile))
//             using (var sr = new StreamReader(fs, Encoding.GetEncoding("Shift-JIS")))
//                 text = sr.ReadToEnd();

//         {
//             var pos = text.IndexOf("dependencies");
//             if (pos < 0) return;
//             pos = text.IndexOf("}", pos);
//             if (pos < 0) return;
//             text = text.Insert(pos, "implementation 'androidx.multidex:multidex:2.0.1'\n    ");
//         }
//         {
//             var pos = text.IndexOf("defaultConfig");
//             if (pos < 0) return;
//             pos = text.IndexOf("{", pos);
//             if (pos < 0) return;
//             text = text.Insert(pos + 1, "\n        multiDexEnabled true");
//         }

//         using (var fs = File.OpenWrite(launcherGradlePropertiesFile))
//             using (var sw = new StreamWriter(fs, Encoding.GetEncoding("Shift-JIS")))
//                 sw.Write(text);
// #endif
//         Debug.Log("Completed! PostBuildProcessor");
//     }
// }
#endif