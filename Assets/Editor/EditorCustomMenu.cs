﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class EditorCustomMenu : EditorWindow
{
    [MenuItem("Custom/Delete All Data", false, 100)]
    private static void DeleteAllData()
    {
        PlayerPrefs.DeleteAll();
        ES3.DeleteFile();
        EditorUtility.DisplayDialog("", "データを削除しました。", "OK");
    }
}
