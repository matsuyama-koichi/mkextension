﻿using UnityEngine;
using System.Collections.Generic;

namespace Main
{
	/// <summary>
	/// GameObject クラスに関する汎用関数を管理するクラス
	/// </summary>
	public static class GameObjectUtils
	{
		/// <summary>
		/// 指定された名前のゲームオブジェクトが存在するかどうかを返します
		/// </summary>
		public static bool Exists( string name )
		{
			return GameObject.Find( name );
		}

		/// <summary>
		/// 指定されたタグ名のゲームオブジェクトが存在するかどうかを返します
		/// </summary>
		public static bool ExistsTag( string name )
		{
			return GameObject.FindWithTag( name );
		}

		/// <summary>
		/// 複数のタグを検索してリストで返します
		/// </summary>
		public static GameObject[] FindTagsGameObjects( IList<string> names )
        {
            var result = new List<GameObject>();
            for ( int i = 0; i < names.Count; i++ )
            {
                foreach ( var o in GameObject.FindGameObjectsWithTag( names[i] ) )
                    result.Add( o );
            }
            return result.ToArray();
        }

		/// <summary>
		/// 複数のタグを検索してリストで返します
		/// </summary>
		public static GameObject[] FindTagsGameObjects( params string[] names )
        {
            var result = new List<GameObject>();
            for ( int i = 0; i < names.Length; i++ )
            {
                foreach ( var o in GameObject.FindGameObjectsWithTag( names[i] ) )
                    result.Add( o );
            }
            return result.ToArray();
        }

		/// <summary>
		/// 名前で検索してコンポーネントを取得
		/// </summary>
		public static T FindGetComponent<T> ( string name ) where T : Component
		{
			var result = GameObject.Find( name );
			return result != null ? result.GetComponent<T>() : null;
		}

		/// <summary>
		/// タグ名で検索してコンポーネントを取得
		/// </summary>
		public static T FindTagGetComponent<T> ( string name ) where T : Component
		{
			var result = GameObject.FindWithTag( name );
			return result != null ? result.GetComponent<T>() : null;
		}

		/// <summary>
		/// Hierarchy 上のゲームオブジェクトを取得
		/// </summary>
		public static GameObject[] FindHierarchyGameObjects( bool includeInactive = false )
		{
			var result = new List<GameObject>();
			foreach ( GameObject obj in UnityEngine.Object.FindObjectsOfType(typeof(GameObject)) )
			{
				if ( includeInactive )
					result.Add( obj );
				else
				{
					if ( obj.activeInHierarchy )
						result.Add( obj );
				}
			}
			return result.ToArray();
		}
	}
}