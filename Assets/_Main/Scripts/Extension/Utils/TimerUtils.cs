﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

namespace Main
{
    public static class TimerUtils
    {
        static GameObject _go = null;
        static GameObject go { get { 
            if (_go == null)
                _go = new GameObject( typeof(TimerUtils).Name );
            return _go;
        } }

        public static void Set( float time, Action act )
        {
            Observable
                .Timer( TimeSpan.FromSeconds( time ) )
                .TakeUntilDestroy( go )
                .Subscribe( _ => act?.Invoke() )
                .AddTo( go );
        }

        public static void SetTakeCount( float time, int count, Action act )
        {
            Observable
                .Timer( TimeSpan.FromSeconds( time ) )
                .Repeat()
                .Take( count )
                .TakeUntilDestroy( go )
                .Subscribe( _ => act?.Invoke() )
                .AddTo( go );
        }

        public static void SetTakeCount( float time, int count, Action<int> act )
        {
            int index = 0;
            Observable
                .Timer( TimeSpan.FromSeconds( time ) )
                .Repeat()
                .Take( count )
                .TakeUntilDestroy( go )
                .Subscribe( _ => 
                {
                    act?.Invoke( index );
                    index++;
                })
                .AddTo( go );
        }

        public static void SetLoop( float time, Action act )
        {
            Observable
                .Timer( TimeSpan.FromSeconds( time ) )
                .Repeat()
                .TakeUntilDestroy( go )
                .Subscribe( _ => act?.Invoke() )
                .AddTo( go );
        }

        public static void SetLoop( float time, Action<int> act )
        {
            int index = 0;
            Observable
                .Timer( TimeSpan.FromSeconds( time ) )
                .Repeat()
                .TakeUntilDestroy( go )
                .Subscribe( _ => 
                {
                    act?.Invoke( index );
                    index++;
                })
                .AddTo( go );
        }

        public static void SetCondition( Func<bool> condition, Action act )
        {
            if ( condition() )
            {
                act?.Invoke();
                return;
            }
            
            Observable
                .FromCoroutine<int>( observer => DoCallWaitForCondition( observer, condition ) )
                .TakeUntilDestroy( go )
                .Subscribe( _ => act?.Invoke() )
                .AddTo( go );
        }

        private static IEnumerator DoCallWaitForCondition( IObserver<int> observer, Func<bool> condition )
        {
            while ( !condition() ) yield return 0;
            observer.OnNext( 0 );
            observer.OnCompleted();
        }
    }
}