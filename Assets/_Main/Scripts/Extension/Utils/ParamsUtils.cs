﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace Main
{
    public static class ParamsUtils
    {
        /// <summary>
        /// 可変長引数の合計を返す
        /// </summary>
        public static int Sum( params int[] array ) { return array.Sum(); }

        /// <summary>
        /// 可変長引数の合計を返す
        /// </summary>
        public static float Sum( params float[] array ) { return array.Sum(); }

        /// <summary>
        /// 可変長引数の最大値を返す
        /// </summary>
        public static int Max( params int[] array ) { return array.Max(); }

        /// <summary>
        /// 可変長引数の最大値を返す
        /// </summary>
        public static float Max( params float[] array ) { return array.Max(); }

        /// <summary>
        /// 可変長引数の最小値を返す
        /// </summary>
        public static int Min( params int[] array ) { return array.Min(); }

        /// <summary>
        /// 可変長引数の最小値を返す
        /// </summary>
        public static float Min( params float[] array ) { return array.Min(); }

        /// <summary>
        /// 可変長引数の平均値を返す (float型)
        /// </summary>
        public static float Average( params int[] array ) { return (float) array.Sum() / (float) array.Length; }

        /// <summary>
        /// 可変長引数の平均値を返す
        /// </summary>
        public static float Average( params float[] array ) { return array.Average(); }
    }
}