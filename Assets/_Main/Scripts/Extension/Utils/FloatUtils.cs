﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Main
{
    public static class FloatUtils
    {
        private const float m_minAngle = 0f;
        private const float m_maxAngle = 360f;

        /// <summary>
		/// 2点間の角度を測る
		/// </summary>
        public static float GetAngle(Vector2 p1, Vector2 p2)
        {
            float dx = p2.x - p1.x;
            float dy = p2.y - p1.y;
            float rad = Mathf.Atan2(dy, dx);
            return rad * Mathf.Rad2Deg;
        }

        /// <summary>
		/// 0 ~ 360 の角度の数字に変換する
		/// </summary>
        public static float Fix360(float self)
        {
			return self = self >= m_maxAngle ? self - (m_maxAngle * Mathf.FloorToInt(self / m_maxAngle)) : 
                        self < m_minAngle ? self + m_maxAngle * (Mathf.FloorToInt(Mathf.Abs(self) / m_maxAngle) + 1) : 
                        self;
        }

        /// <summary>
		/// 2点間の角度を360°で返す
		/// </summary>
        public static float GetAngle360(Vector2 p1, Vector2 p2)
        {
            return Fix360(GetAngle(p1, p2));
        }
    }
}

