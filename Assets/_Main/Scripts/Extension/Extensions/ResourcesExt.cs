﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace Main
{
    public static class ResourcesExt
    {
        public static T IfNullResourcesLoad<T>( this T self, string path ) where T : Object
        {
            if ( self != null )
                return self;

            return Resources.Load<T>( path );
        }

        public static T IfNullResourcesLoadAll<T>( this T self, string folderName, string fileName ) where T : Object
        {
            if ( self != null )
                return self;

            foreach ( var o in Resources.LoadAll(folderName, typeof(T)) )
            {
                if( o.name == fileName )
                {
                    return o as T;
                }
            }
            return null as T;
        }
    }
}