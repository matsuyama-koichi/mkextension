﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Main
{
    public static class Rigidbody2DExt
    {
        public static Rigidbody2D SetSpeed( this Rigidbody2D self, float speed )
        {
            var rad = Mathf.Atan2(self.velocity.y, self.velocity.x);
            var x = Mathf.Cos(rad) * speed;
            var y = Mathf.Sin(rad) * speed;
            self.velocity = new Vector2(x, y);
            return self;
        }

        public static Rigidbody2D SetSpeed( this Rigidbody2D self, float speed, float direction )
        {
            var x = Mathf.Cos (Mathf.Deg2Rad * direction) * speed;
            var y = Mathf.Sin (Mathf.Deg2Rad * direction) * speed;
            self.velocity = new Vector2(x, y);
            return self;
        }

        public static Rigidbody2D SetVelocity( this Rigidbody2D self, float x, float y )
        {
            self.velocity = new Vector2( x, y );
            return self;
        }

        public static Rigidbody2D SetVelocity( this Rigidbody2D self, Vector2 vec )
        {
            self.velocity = vec;
            return self;
        }

        public static Rigidbody2D SetVelocityX( this Rigidbody2D self, float x )
        {
            self.velocity = self.velocity.SetX( x );
            return self;
        }

        public static Rigidbody2D SetVelocityY( this Rigidbody2D self, float y )
        {
            self.velocity = self.velocity.SetY( y );
            return self;
        }

        public static Rigidbody2D AddVelocity( this Rigidbody2D self, float x, float y )
        {
            self.velocity += new Vector2( x, y );
            return self;
        }

        public static Rigidbody2D AddVelocity( this Rigidbody2D self, Vector2 vec )
        {
            self.velocity += vec;
            return self;
        }

        public static Rigidbody2D AddVelocityX( this Rigidbody2D self, float x )
        {
            self.velocity = self.velocity.AddX( x );
            return self;
        }

        public static Rigidbody2D AddVelocityY( this Rigidbody2D self, float y )
        {
            self.velocity = self.velocity.AddY( y );
            return self;
        }

        public static Rigidbody2D SetFreeze( this Rigidbody2D self, params RigidbodyConstraints2D[] constrains )
        {
            for ( int i = 0; i < constrains.Length; i++ )
            {
                var c = constrains[ i ];
                if (c == RigidbodyConstraints2D.None | c == RigidbodyConstraints2D.FreezeAll)
                    self.constraints = c;
                else
                    self.constraints |= c;
            }
            return self;
        }
    }
}
