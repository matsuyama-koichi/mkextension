﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Main
{
    public static class RigidbodyExt
    {
        public static Rigidbody SetVelocity( this Rigidbody self, float x, float y, float z )
        {
            self.velocity = new Vector3(x, y, z);
            return  self;
        }

        public static Rigidbody SetVelocity( this Rigidbody self, Vector3 vec )
        {
            self.velocity = vec;
            return self;
        }

        public static Rigidbody SetVelocityX( this Rigidbody self, float x )
        {
            self.velocity = self.velocity.SetX( x );
            return self;
        }

        public static Rigidbody SetVelocityY( this Rigidbody self, float y )
        {
            self.velocity = self.velocity.SetY( y );
            return self;
        }

        public static Rigidbody SetVelocityZ( this Rigidbody self, float z )
        {
            self.velocity = self.velocity.SetZ( z );
            return self;
        }

        public static Rigidbody AddVelocity( this Rigidbody self, float x, float y, float z )
        {
            self.velocity += new Vector3( x, y, z );
            return self;
        }

        public static Rigidbody AddVelocity( this Rigidbody self, Vector3 vec )
        {
            self.velocity += vec;
            return self;
        }

        public static Rigidbody AddVelocityX( this Rigidbody self, float x )
        {
            self.velocity = self.velocity.AddX( x );
            return self;
        }

        public static Rigidbody AddVelocityY( this Rigidbody self, float y )
        {
            self.velocity = self.velocity.AddY( y );
            return self;
        }

        public static Rigidbody AddVelocityZ( this Rigidbody self, float z )
        {
            self.velocity = self.velocity.AddZ( z );
            return self;
        }

        public static Rigidbody SetFreeze( this Rigidbody self, params RigidbodyConstraints[] constrains )
        {
            for ( int i = 0; i < constrains.Length; i++ )
            {
                var c = constrains[ i ];
                if (c == RigidbodyConstraints.None | c == RigidbodyConstraints.FreezeAll)
                    self.constraints = c;
                else
                    self.constraints |= c;
            }
            return self;
        }
    }
}