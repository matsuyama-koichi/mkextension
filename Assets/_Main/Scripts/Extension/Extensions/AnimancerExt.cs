using UnityEngine;
using System.Threading;
using Cysharp.Threading.Tasks;
using Animancer;

namespace Main
{
    public static class AnimancerExt
    {
        public static async UniTask AddTo(this AnimancerState state, CancellationToken ct)
        {
            await state.ToUniTask(cancellationToken: ct);
        }

        public static void Forget(this AnimancerState state, CancellationToken ct)
        {
            state.ToUniTask(cancellationToken: ct).Forget();
        }
    }
}
