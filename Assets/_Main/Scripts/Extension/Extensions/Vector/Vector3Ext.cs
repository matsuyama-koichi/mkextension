﻿using UnityEngine;
using UnityEngine.Assertions;

namespace Main
{
	/// <summary>
	/// Vector3 型の拡張メソッドを管理するクラス
	/// </summary>
	public static class Vector3Ext
	{
		/// <summary>
		/// x,y,zの値が等しいかを返します
		/// </summary>
		public static bool IsUniform( this Vector3 self )
		{
			return Mathf.Approximately( self.x, self.y ) && Mathf.Approximately( self.x, self.z );
		}
		
        /// <summary>
		/// 切り上げして返す
		/// </summary>
        public static Vector3 Ceil( this Vector3 self )
        {
            return new Vector3
			(
				Mathf.Ceil( self.x ), 
				Mathf.Ceil( self.y ), 
				Mathf.Ceil( self.z )
			);
        }

        /// <summary>
		/// 切り捨てして返す
		/// </summary>
        public static Vector3 Floor( this Vector3 self )
        {
            return new Vector3
			(
				Mathf.Floor( self.x ), 
				Mathf.Floor( self.y ), 
				Mathf.Floor( self.z )
			);
        }

        /// <summary>
		/// より近い整数の値を返す（0.5の場合は偶数になる）
		/// </summary>
        public static Vector3 Round( this Vector3 self )
        {
            return new Vector3
			(
				Mathf.Round( self.x ), 
				Mathf.Round( self.y ), 
				Mathf.Round( self.z )
			);
        }

		/// <summary>
		/// 小数点第 n 位で四捨五入して返す
		/// </summary>
        public static Vector3 Round( this Vector3 self, int n )
        {
            return new Vector3
			(
				self.x.Round( n ), 
				self.y.Round( n ), 
				self.z.Round( n )
			);
        }

       	/// <summary>
		/// 引数を比較して大きい方を返す
		/// </summary>
		public static Vector3 Max( this Vector3 self, float x, float y, float z )
		{
			return new Vector3
			(
				Mathf.Max( self.x, x ),
				Mathf.Max( self.y, y ),
				Mathf.Max( self.z, z )
			);
		}

       	/// <summary>
		/// 引数を比較して大きい方を返す
		/// </summary>
		public static Vector3 Max( this Vector3 self, float max )
		{
			return new Vector3
			(
				Mathf.Max( self.x, max ),
				Mathf.Max( self.y, max ),
				Mathf.Max( self.z, max )
			);
		}

       	/// <summary>
		/// 引数を比較して小さい方を返す
		/// </summary>
		public static Vector3 Min( this Vector3 self, float x, float y, float z )
		{
			return new Vector3
			(
				Mathf.Min( self.x, x ),
				Mathf.Min( self.y, y ),
				Mathf.Min( self.z, z )
			);
		}

       	/// <summary>
		/// 引数を比較して小さい方を返す
		/// </summary>
		public static Vector3 Min( this Vector3 self, float min )
		{
			return new Vector3
			(
				Mathf.Min( self.x, min ),
				Mathf.Min( self.y, min ),
				Mathf.Min( self.z, min )
			);
		}

        /// <summary>
		/// 切り上げしてVector3Intで返す
		/// </summary>
        public static Vector3Int CeilToInt( this Vector3 self )
        {
            return new Vector3Int
			(
				Mathf.CeilToInt( self.x ), 
				Mathf.CeilToInt( self.y ), 
				Mathf.CeilToInt( self.z )
			);
        }

        /// <summary>
		/// 切り捨てしてVector3Intで返す
		/// </summary>
        public static Vector3Int FloorToInt( this Vector3 self )
        {
            return new Vector3Int
			(
				Mathf.FloorToInt( self.x ), 
				Mathf.FloorToInt( self.y ), 
				Mathf.FloorToInt( self.z )
			);
        }

        /// <summary>
		/// より近い整数の値をVector3Intで返す（0.5の場合は偶数になる）
		/// </summary>
        public static Vector3Int RoundToInt( this Vector3 self )
        {
            return new Vector3Int
			(
				Mathf.RoundToInt( self.x ), 
				Mathf.RoundToInt( self.y) , 
				Mathf.RoundToInt( self.z )
			);
        }
		
        /// <summary>
        /// x座標を設定する
        /// </summary>
		public static Vector3 SetX( this Vector3 self, float x )
		{
			return new Vector3
			(
				x,
				self.y,
				self.z
			);
		}

        /// <summary>
        /// y座標を設定する
        /// </summary>
		public static Vector3 SetY( this Vector3 self, float y )
		{
			return new Vector3
			(
				self.x,
				y,
				self.z
			);
		}

       	/// <summary>
        /// z座標を設定する
        /// </summary>
		public static Vector3 SetZ( this Vector3 self, float z )
		{
			return new Vector3
			(
				self.x,
				self.y,
				z
			);
		}

		/// <summary>
		/// 位置を加算する
		/// </summary>
		public static Vector3 Add( this Vector3 self, Vector3 vec )
		{
			return new Vector3
			(
				self.x + vec.x,
				self.y + vec.y,
				self.z + vec.z
			);
		}

		/// <summary>
		/// 位置を加算する
		/// </summary>
		public static Vector3 Add( this Vector3 self, float x, float y, float z )
		{
			return new Vector3
			(
				self.x + x,
				self.y + y,
				self.z + z
			);
		}

        /// <summary>
        /// x座標を加算する
        /// </summary>
		public static Vector3 AddX( this Vector3 self, float x )
		{
			return new Vector3
			(
				self.x + x,
				self.y,
				self.z
			);
		}

        /// <summary>
        /// y座標を加算する
        /// </summary>
		public static Vector3 AddY( this Vector3 self, float y )
		{
			return new Vector3
			(
				self.x,
				self.y + y,
				self.z
			);
		}

        /// <summary>
        /// z座標を加算する
        /// </summary>
		public static Vector3 AddZ( this Vector3 self, float z )
		{
			return new Vector3
			(
				self.x,
				self.y,
				self.z + z
			);
		}

		/// <summary>
        /// 最小値と最大値の指定して乱数を加算する
        /// </summary>
        public static Vector3 RandomRange( this Vector3 self, float min, float max )
        {
            return new Vector3
            (
                self.x + RandomUtils.Range( min, max ),
                self.y + RandomUtils.Range( min, max ),
				self.z + RandomUtils.Range( min, max )
            );
        }

		/// <summary>
		/// 任意の座標に変換して返す
		/// </summary>
		public static to To( this Vector3 self ) => new to( self );
        public struct to
        {
            private Vector3 v;
            public to( Vector3 v ) { this.v = v; }
            public Vector2 Vec2() => new Vector2( v.x, v.y );
			public Vector3Int Int() => new Vector3Int( (int)v.x, (int)v.y, (int)v.z );
            public Vector2 XZ() => new Vector2( v.x, v.z );
            public Vector2 YX() => new Vector2( v.y, v.x );
            public Vector2 YZ() => new Vector2( v.y, v.z );
            public Vector2 ZX() => new Vector2( v.z, v.x );
            public Vector2 ZY() => new Vector2( v.z, v.y );
            public Vector3 XZY() => new Vector3( v.x, v.z, v.y );
            public Vector3 YXZ() => new Vector3( v.y, v.x, v.z );
            public Vector3 YZX() => new Vector3( v.y, v.z, v.x );
            public Vector3 ZXY() => new Vector3( v.z, v.x, v.y );
            public Vector3 ZYX() => new Vector3( v.z, v.y, v.x );
        }

		/// <summary>
		/// 文字列を座標に変換して返す
		/// </summary>
		public static Vector3 StringToVector3(this string input)
		{
			var elements = input.Trim('(', ')').Split(','); // 前後に丸括弧があれば削除し、カンマで分割
			var result = Vector3.zero;
			var elementCount = Mathf.Min(elements.Length, 3); // ループ回数をelementsの数以下かつ3以下にする
			for (var i = 0; i < elementCount; i++)
			{
				float value;

				float.TryParse(elements[i], out value); // 変換に失敗したときに例外が出る方が望ましければ、Parseを使うのがいいでしょう
				result[i] = value;
			}
			return result;
		}
	}
}