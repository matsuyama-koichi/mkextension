﻿using UnityEngine;
using UnityEngine.Assertions;

namespace Main
{
    public static class Vector3IntExt
    {
        /// <summary>
		/// 引数を比較して大きい方を返す
		/// </summary>
        public static Vector3Int Max(this Vector3Int self, int x, int y, int z)
        {
            return new Vector3Int
            (
                Mathf.Max(self.x, x), 
                Mathf.Max(self.y, y), 
                Mathf.Max(self.z, z)
            );
        }

       	/// <summary>
		/// 引数を比較して大きい方を返す
		/// </summary>
        public static Vector3Int Max(this Vector3Int self, int v)
        {
            return new Vector3Int
            (
                Mathf.Max(self.x, v), 
                Mathf.Max(self.y, v), 
                Mathf.Max(self.z, v)
            );
        }

       	/// <summary>
		/// 引数を比較して小さい方を返す
		/// </summary>
        public static Vector3Int Min(this Vector3Int self, int x, int y, int z)
        {
            return new Vector3Int
            (
                Mathf.Min(self.x, x), 
                Mathf.Min(self.y, y), 
                Mathf.Min(self.z, z)
            );
        }

       	/// <summary>
		/// 引数を比較して小さい方を返す
		/// </summary>
        public static Vector3Int Min(this Vector3Int self, int v)
        {
            return new Vector3Int
            (
                Mathf.Min(self.x, v), 
                Mathf.Min(self.y, v), 
                Mathf.Min(self.z, v)
            );
        }

        /// <summary>
        /// x座標を設定する
        /// </summary>
        public static Vector3Int SetX( this Vector3Int self, int v )
        {
            return new Vector3Int
            (
                v,
                self.y,
                self.z
            );
        }

        /// <summary>
        /// y座標を設定する
        /// </summary>
        public static Vector3Int SetY( this Vector3Int self, int v )
        {
            return new Vector3Int
            (
                self.x,
                v,
                self.z
            );
        }

        /// <summary>
        /// z座標を設定する
        /// </summary>
        public static Vector3Int SetZ( this Vector3Int self, int v )
        {
            return new Vector3Int
            (
                self.x,
                self.y,
                v
            );
        }

        /// <summary>
		/// 位置を加算する
		/// </summary>
		public static Vector3Int Add( this Vector3Int self, Vector3Int v )
		{
			return new Vector3Int
			(
				self.x + v.x,
				self.y + v.y,
				self.z + v.z
			);
		}

		/// <summary>
		/// 位置を加算する
		/// </summary>
		public static Vector3Int Add( this Vector3Int self, int x, int y, int z )
		{
			return new Vector3Int
			(
				self.x + x,
				self.y + y,
				self.z + z
			);
		}

        /// <summary>
        /// x座標を加算する
        /// </summary>
        public static Vector3Int AddX( this Vector3Int self, int v )
        {
            return new Vector3Int
            (
                self.x + v,
                self.y,
                self.z
            );
        }

        /// <summary>
        /// y座標を加算する
        /// </summary>
        public static Vector3Int AddY( this Vector3Int self, int v )
        {
            return new Vector3Int
            (
                self.x,
                self.y + v,
                self.z
            );
        }

        /// <summary>
        /// z座標を加算する
        /// </summary>
        public static Vector3Int AddZ( this Vector3Int self, int v )
        {
            return new Vector3Int
            (
                self.x,
                self.y,
                self.z + v
            );
        }

        /// <summary>
        /// 最小値と最大値の指定して乱数を加算する
        /// </summary>
        public static Vector3Int RandomRange( this Vector3Int self, int min, int max )
        {
            return new Vector3Int
            (
                self.x + RandomUtils.Range( min, max ),
                self.y + RandomUtils.Range( min, max ),
				self.z + RandomUtils.Range( min, max )
            );
        }

        /// <summary>
        /// 任意の座標に変換して返す
        /// </summary>
        public static to To( this Vector3Int self ) => new to( self );
        public struct to
        {
            private Vector3Int v;
            public to( Vector3Int v ) { this.v = v; }
            public Vector2Int Vec2Int() => new Vector2Int( v.x, v.y );
            public Vector3 Float() => new Vector3( (float)v.x, (float)v.y, (float)v.z );
            public Vector2Int XZ() => new Vector2Int( v.x, v.z );
            public Vector2Int YX() => new Vector2Int( v.y, v.x );
            public Vector2Int YZ() => new Vector2Int( v.y, v.z );
            public Vector2Int ZX() => new Vector2Int( v.z, v.x );
            public Vector2Int ZY() => new Vector2Int( v.z, v.y );
            public Vector3Int XZY() => new Vector3Int( v.x, v.z, v.y );
            public Vector3Int YXZ() => new Vector3Int( v.y, v.x, v.z );
            public Vector3Int YZX() => new Vector3Int( v.y, v.z, v.x );
            public Vector3Int ZXY() => new Vector3Int( v.z, v.x, v.y );
            public Vector3Int ZYX() => new Vector3Int( v.z, v.y, v.x );
        }
    }
}