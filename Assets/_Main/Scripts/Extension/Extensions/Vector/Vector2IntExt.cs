﻿using UnityEngine;
using UnityEngine.Assertions;

namespace Main
{
    public static class Vector2IntExt
    {
        /// <summary>
		/// 引数を比較して大きい方を返す
		/// </summary>
        public static Vector2Int Max( this Vector2Int self, int x, int y )
        {
            return new Vector2Int
            (
                Mathf.Max( self.x, x ),
                Mathf.Max( self.y, y )
            );
        }

        /// <summary>
		/// 引数を比較して大きい方を返す
		/// </summary>
        public static Vector2Int Max( this Vector2Int self, int max )
        {
            return new Vector2Int
            (
                Mathf.Max( self.x, max ),
                Mathf.Max( self.y, max )
            );
        }

       	/// <summary>
		/// 引数を比較して小さい方を返す
		/// </summary>
        public static Vector2Int Min( this Vector2Int self, int x, int y )
        {
            return new Vector2Int
            (
                Mathf.Min( self.x, x ),
                Mathf.Min( self.y, y )
            );
        }

       	/// <summary>
		/// 引数を比較して小さい方を返す
		/// </summary>
        public static Vector2Int Min( this Vector2Int self, int min )
        {
            return new Vector2Int
            (
                Mathf.Min( self.x, min ),
                Mathf.Min( self.y, min )
            );
        }

        /// <summary>
        /// x座標を設定する
        /// </summary>
        public static Vector2Int SetX( this Vector2Int self, int x )
        {
            return new Vector2Int
            (
                x,
                self.y
            );
        }

        /// <summary>
        /// y座標を設定する
        /// </summary>
        public static Vector2Int SetY( this Vector2Int self, int y )
        {
            return new Vector2Int
            (
                self.x,
                y
            );
        }

        /// <summary>
		/// 位置を加算する
		/// </summary>
        public static Vector2Int Add( this Vector2Int self, Vector2Int vec )
        {
            return new Vector2Int
            (
                self.x + vec.x,
                self.y + vec.y
            );
        }

		/// <summary>
		/// 位置を加算する
		/// </summary>
        public static Vector2Int Add( this Vector2Int self, int x, int y )
        {
            return new Vector2Int
            (
                self.x + x,
                self.y + y
            );
        }

        /// <summary>
        /// x座標を加算する
        /// </summary>
        public static Vector2Int AddX( this Vector2Int self, int x )
        {
            return new Vector2Int
            (
                self.x + x,
                self.y
            );
        }

        /// <summary>
        /// y座標を加算する
        /// </summary>
        public static Vector2Int AddY( this Vector2Int self, int y )
        {
            return new Vector2Int
            (
                self.x,
                self.y + y
            );
        }

        /// <summary>
        /// 最小値と最大値の指定して乱数を加算する
        /// </summary>
        public static Vector2Int RandomRange( this Vector2Int self, int min, int max )
        {
            return new Vector2Int
            (
                self.x + RandomUtils.Range( min, max ),
                self.y + RandomUtils.Range( min, max )
            );
        }

        /// <summary>
        /// 任意の座標に変換して返す
        /// </summary>
        public static to To( this Vector2Int self ) => new to( self );
        public struct to
        {
            private Vector2Int v;
            public to( Vector2Int v ) { this.v = v; }
            public Vector2 Float() => new Vector2( (float)v.x, (float)v.y );
            public Vector2Int YX() => new Vector2Int( v.y, v.x );
        }
    }
}