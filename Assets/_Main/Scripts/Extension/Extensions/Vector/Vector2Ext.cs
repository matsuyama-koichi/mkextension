﻿using UnityEngine;
using UnityEngine.Assertions;

namespace Main
{
    public static class Vector2Ext
    {
        /// <summary>
		/// x,yの値が等しいかを返します
		/// </summary>
		public static bool IsUniform( this Vector2 self )
		{
            return Mathf.Approximately( self.x, self.y );
		}

        /// <summary>
		/// 座標からの角度と距離で座標を計測する
		/// </summary>
        public static Vector2 PolarPoint( this Vector2 self, float angle, float radius )
        {
            var x = Mathf.Cos(angle * Mathf.Deg2Rad) * radius + self.x;
            var y = Mathf.Sin(angle * Mathf.Deg2Rad) * radius + self.y;
            return new Vector2( x, y );
        }

        /// <summary>
		/// RectTransformをワールド座標に変換する
		/// </summary>
        public static Vector2 RectTransformToWorldPoint( this RectTransform rect, Camera camera )
        {
            var screenPos = RectTransformUtility.WorldToScreenPoint(camera, rect.transform.position);
            var worldPos = Vector3.zero;
            RectTransformUtility.ScreenPointToWorldPointInRectangle(rect, screenPos, camera, out worldPos);
            return (Vector2) worldPos;
        }

        /// <summary>
		/// 切り上げして返す
		/// </summary>
        public static Vector2 Ceil( this Vector2 self )
        {
            return new Vector2
            (
                Mathf.Ceil( self.x ), 
                Mathf.Ceil( self.y )
            );
        }

        /// <summary>
		/// 切り捨てして返す
		/// </summary>
        public static Vector2 Floor( this Vector2 self )
        {
            return new Vector2
            (
                Mathf.Floor( self.x ), 
                Mathf.Floor( self.y )
            );
        }

        /// <summary>
		/// より近い整数の値を返す（0.5の場合は偶数になる）
		/// </summary>
        public static Vector2 Round( this Vector2 self )
        {
            return new Vector2
            (
                Mathf.Round( self.x ), 
                Mathf.Round( self.y )
            );
        }

        /// <summary>
		/// 小数点第 n 位で四捨五入して返す
		/// </summary>
        public static Vector2 Round( this Vector2 self, int n )
        {
            return new Vector2
			(
				self.x.Round( n ), 
				self.y.Round( n )			
            );
        }

        /// <summary>
		/// 引数を比較して大きい方を返す
		/// </summary>
        public static Vector2 Max( this Vector2 self, float x, float y )
        {
            return new Vector2
            (
                Mathf.Max( self.x, x ),
                Mathf.Max( self.y, y )
            );
        }

        /// <summary>
		/// 引数を比較して大きい方を返す
		/// </summary>
        public static Vector2 Max( this Vector2 self, float max )
        {
            return new Vector2
            (
                Mathf.Max( self.x, max ),
                Mathf.Max( self.y, max )
            );
        }

       	/// <summary>
		/// 引数を比較して小さい方を返す
		/// </summary>
        public static Vector2 Min( this Vector2 self, float x, float y )
        {
            return new Vector2
            (
                Mathf.Min( self.x, x ),
                Mathf.Min( self.y, y )
            );
        }

       	/// <summary>
		/// 引数を比較して小さい方を返す
		/// </summary>
        public static Vector2 Min( this Vector2 self, float min )
        {
            return new Vector2
            (
                Mathf.Min( self.x, min ),
                Mathf.Min( self.y, min )
            );
        }

        /// <summary>
		/// 切り上げしてVector2Intで返す
		/// </summary>
        public static Vector2Int CeilToInt( this Vector2 self )
        {
            return new Vector2Int
            (
                Mathf.CeilToInt( self.x ), 
                Mathf.CeilToInt( self.y )
            );
        }

        /// <summary>
		/// 切り捨てしてVector2Intで返す
		/// </summary>
        public static Vector2Int FloorToInt( this Vector2 self )
        {
            return new Vector2Int
            (
                Mathf.FloorToInt( self.x ), 
                Mathf.FloorToInt( self.y )
            );
        }

        /// <summary>
		/// より近い整数の値をVector2Intで返す（0.5の場合は偶数になる）
		/// </summary>
        public static Vector2Int RoundToInt( this Vector2 self )
        {
            return new Vector2Int
            (
                Mathf.RoundToInt( self.x ), 
                Mathf.RoundToInt( self.y )
            );
        }


        /// <summary>
        /// x座標を設定する
        /// </summary>
        public static Vector2 SetX( this Vector2 self, float x )
        {
            return new Vector2
            (
                x,
                self.y
            );
        }

        /// <summary>
        /// y座標を設定する
        /// </summary>
        public static Vector2 SetY( this Vector2 self, float y )
        {
            return new Vector2
            (
                self.x,
                y
            );
        }

		/// <summary>
		/// 位置を加算する
		/// </summary>
        public static Vector2 Add( this Vector2 self, Vector2 vec )
        {
            return new Vector2
            (
                self.x + vec.x,
                self.y + vec.y
            );
        }

		/// <summary>
		/// 位置を加算する
		/// </summary>
        public static Vector2 Add( this Vector2 self, float x, float y )
        {
            return new Vector2
            (
                self.x + x,
                self.y + y
            );
        }

        /// <summary>
        /// x座標を加算する
        /// </summary>
        public static Vector2 AddX( this Vector2 self, float v )
        {
            return new Vector2
            (
                self.x + v,
                self.y
            );
        }

        /// <summary>
        /// y座標を加算する
        /// </summary>
        public static Vector2 AddY( this Vector2 self, float y )
        {
            return new Vector2
            (
                self.x,
                self.y + y
            );
        }

        /// <summary>
        /// 最小値と最大値の指定して乱数を加算する
        /// </summary>
        public static Vector2 RandomRange( this Vector2 self, float min, float max )
        {
            return new Vector2
            (
                self.x + RandomUtils.Range( min, max ),
                self.y + RandomUtils.Range( min, max )
            );
        }

        /// <summary>
        /// 任意の座標に変換して返す
        /// </summary>
        public static to To( this Vector2 self ) => new to( self );
        public struct to
        {
            private Vector2 v;
            public to( Vector2 v ) { this.v = v; }
            public Vector2Int Int() => new Vector2Int( (int)v.x, (int)v.y );
            public Vector2 YX() => new Vector2( v.y, v.x );
        }
    }
}

