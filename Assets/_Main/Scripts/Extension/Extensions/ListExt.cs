﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Main
{
	/// <summary>
	/// List 型の拡張メソッドを管理するクラス
	/// </summary>
	public static class ListExt
	{
		private static System.Random m_random = new System.Random();

		/// <summary>
		/// 指定したコレクションの要素をリストの末尾に追加します
		/// </summary>
		public static List<T> Add<T>( this List<T> self, IEnumerable<T> collection )
		{
			self.AddRange( collection );
			return self;
		}

		/// <summary>
		/// 指定したコレクションの要素を末尾に追加します
		/// </summary>
		public static List<T> AddRange<T>( this List<T> list, params T[] collection )
		{
			list.AddRange( collection );
			return list;
		}

		/// <summary>
		/// 指定したコレクションの要素をの末尾に追加します
		/// </summary>
		public static List<T> AddRange<T>( this List<T> list, params IList<T>[] collectionList )
		{
			for ( int i = 0; i < collectionList.Length; i++ )
			{
				list.AddRange( collectionList[ i ] );
			}
			return list;
		}

		/// <summary>
		/// すべての要素を削除して、指定したコレクションの要素を追加します
		/// </summary>
		public static List<T> Set<T>( this List<T> list, IEnumerable<T> collection )
		{
			list.Clear();
			list.AddRange( collection );
			return list;
		}

		/// <summary>
		/// すべての要素を削除して、指定したコレクションの要素を追加します
		/// </summary>
		public static List<T> Set<T>( this List<T> list, params T[] collection )
		{
			list.Clear();
			list.AddRange( collection );
			return list;
		}

		/// <summary>
		/// 指定した Comparison<T> を使用して要素を並べ替えます
		/// </summary>
		public static List<T> Sort<T>( this List<T> self, Comparison<T> comparison )
		{
			self.Sort( comparison );
			return self;
		}

		/// <summary>
		/// 指定した Func<TSource, TResult> を使用して要素を並べ替えます
		/// </summary>
		public static List<TSource> Sort<TSource, TResult>( this List<TSource> self, Func<TSource, TResult> selector ) where TResult : IComparable
		{
			self.Sort( ( x, y ) => selector( x ).CompareTo( selector( y ) ) );
			return self;
		}

		/// <summary>
		/// 指定した Func<TSource, TResult> を使用して要素を逆順に並べ替えます
		/// </summary>
		public static List<TSource> SortDescending<TSource, TResult>( this List<TSource> self, Func<TSource, TResult> selector ) where TResult : IComparable
		{
			self.Sort( ( x, y ) => selector( y ).CompareTo( selector( x ) ) );
			return self;
		}

		/// <summary>
		/// 複数のキーを使用して要素を並べ替えます
		/// </summary>
		public static List<TSource> Sort<TSource, TResult1, TResult2>( this List<TSource> self, Func<TSource, TResult1> selector1, Func<TSource, TResult2> selector2 ) where TResult1 : IComparable where TResult2 : IComparable
		{
			self.Sort( ( x, y ) =>
			{
				var result = selector1( x ).CompareTo( selector1( y ) );
				return result != 0 ? result : selector2( x ).CompareTo( selector2( y ) );
			} );
			return self;
		}

		/// <summary>
		/// 複数のキーを使用して要素を降順に並べ替えます
		/// </summary>
		public static List<TSource> SortDescending<TSource, TResult1, TResult2>( this List<TSource> self, Func<TSource, TResult1> selector1, Func<TSource, TResult2> selector2 ) where TResult1 : IComparable where TResult2 : IComparable
		{
			self.Sort( ( x, y ) =>
			{
				var result = selector1( y ).CompareTo( selector1( x ) );
				return result != 0 ? result : selector2( x ).CompareTo( selector2( y ) );
			} );
			return self;
		}

		/// <summary>
		/// 要素をランダムに並び替えます
		/// </summary>
		public static List<T> Shuffle<T>( this List<T> self )
		{
			int n = self.Count;
			while ( 1 < n )
			{
				n--;
				int k = m_random.Next( n + 1 );
				var tmp = self[ k ];
				self[ k ] = self[ n ];
				self[ n ] = tmp;
			}
			return self;
		}

		/// <summary>
		/// match で一致した最初の要素をリストから削除します
		/// </summary>
		public static List<T> Remove<T>( this List<T> self, Predicate<T> match )
		{
			var index = self.FindIndex( match );
			if ( index == -1 ) return self;
			self.RemoveAt( index );
			return self;
		}

		/// <summary>
		/// リスト内の先頭に要素を挿入します
		/// </summary>
		public static List<T> InsertFirst<T>( this List<T> self, T item )
		{
			self.Insert( 0, item );
			return self;
		}

		/// <summary>
		/// 指定された数になるまでリストの末尾の要素を削除します
		/// </summary>
		public static List<T> RemoveSince<T>( this List<T> self, int count )
		{
			while ( count <= self.Count )
			{
				self.RemoveAt( self.Count - 1 );
			}
			return self;
		}

		/// <summary>
		/// 指定された範囲を指定された値で埋めます
		/// </summary>
		public static List<T> Fill<T>( this List<T> self, int startIndex, int endIndex, T value )
		{
			for ( int i = startIndex; i < endIndex; i++ )
			{
				self.Add( value );
			}
			return self;
		}

		/// <summary>
		/// サイズを設定します
		/// </summary>
		public static List<T> SetSize<T>( this List<T> self, int size )
		{
			if ( self.Count <= size ) return self;
			self.RemoveRange( size, self.Count - size );
			return self;
		}
	}
}