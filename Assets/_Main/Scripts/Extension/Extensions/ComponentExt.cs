﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace Main
{
	/// <summary>
	/// Component 型の拡張メソッドを管理するクラス
	/// </summary>
	public static class ComponentExt
	{
		/// <summary>
		/// アクティブかどうかを設定します
		/// </summary>
		public static T SetActiveIfNotNull<T>( this T self, bool isActive ) where T : Component
		{
			if ( self == null ) return self;
			self.SetActive( isActive );
			return self;
		}

		/// <summary>
		/// <para>指定されたコンポーネントが削除済みかどうかを返します</para>
		/// <para>MissingReferenceException を防ぐために使用します</para>
		/// </summary>
		public static bool HasBeenDestroyed( this Component self )
		{
			return self == null || self.gameObject == null;
		}

		/// <summary>
		/// コンポーネントを取得します。コンポーネントがアタッチされていない場合は追加してから取得します
		/// </summary>
		public static T GetOrAddComponent<T>( this Component self ) where T : Component
		{
			var comp = self.gameObject.GetComponent<T>();
			if ( comp == null )
				comp = self.gameObject.AddComponent<T>();
			return comp;
		}

		/// <summary>
		/// コンポーネントを取得します。コンポーネントがアタッチされていない場合は追加してから取得します
		/// </summary>
		public static Component GetOrAddComponent<T>( this T self, Type type ) where T : Component
		{
			var comp = self.gameObject.GetComponent(type);
			if ( comp == null )
				comp = self.gameObject.AddComponent(type);
			return comp;
		}

		/// <summary>
		/// コンポーネントを追加します
		/// </summary>
		public static T AddComponent<T>( this Component self ) where T : Component
		{
			return self.gameObject.AddComponent<T>();
		}

		/// <summary>
		/// すべての子オブジェクトを返します
		/// </summary>
		public static GameObject[] GetChildren( this Component self, bool includeInactive = false )
		{
			return self
				.GetComponentsInChildren<Transform>( includeInactive )
				.Where( c => c != self.transform )
				.Select( c => c.gameObject )
				.ToArray()
			;
		}

		/// <summary>
		/// 自分自身を含まない GetComponentInChildren 関数を実行します
		/// </summary>
		public static T GetComponentInChildrenWithoutSelf<T>( this Component self ) where T : Component
		{
			return self.GetComponentsInChildrenWithoutSelf<T>().FirstOrDefault();
		}

		/// <summary>
		/// 自分自身を含まない GetComponentsInChildren 関数を実行します
		/// </summary>
		public static T[] GetComponentsInChildrenWithoutSelf<T>( this Component self ) where T : Component
		{
			return self.GetComponentsInChildren<T>().Where( c => self.gameObject != c.gameObject ).ToArray();
		}

		/// <summary>
		/// 自分自身を含まない GetComponentsInChildren 関数を実行します
		/// </summary>
		public static T[] GetComponentsInChildrenWithoutSelf<T>( this Component self, bool includeInactive ) where T : Component
		{
			return self.GetComponentsInChildren<T>( includeInactive ).Where( c => self.gameObject != c.gameObject ).ToArray();
		}

		/// <summary>
		/// コンポーネントを削除します
		/// </summary>
		public static Component RemoveComponent<T>( this Component self ) where T : Component
		{
			GameObject.Destroy( self.GetComponent<T>() );
			return self;
		}

		/// <summary>
		/// コンポーネントをすべて削除します
		/// </summary>
		public static Component RemoveComponents<T>( this Component self ) where T : Component
		{
			foreach ( var component in self.GetComponents<T>() )
			{
				GameObject.Destroy( component );
			}
			return self;
		}

		/// <summary>
		/// コンポーネントを即座に削除します
		/// </summary>
		public static Component RemoveComponentImmediate<T>( this Component self ) where T : Component
		{
			GameObject.DestroyImmediate( self.GetComponent<T>() );
			return self;
		}

		/// <summary>
		/// コンポーネントをすべて即座に削除します
		/// </summary>
		public static Component RemoveComponentsImmediate<T>( this Component self ) where T : Component
		{
			foreach ( var component in self.GetComponents<T>() )
			{
				GameObject.DestroyImmediate( component );
			}
			return self;
		}

		/// <summary>
		/// 指定されたコンポーネントを持っているかどうかを返します
		/// </summary>
		public static bool HasComponent<T>( this Component self ) where T : Component
		{
			return self.GetComponent<T>() != null;
		}

		/// <summary>
		/// アクティブかどうかを設定します
		/// </summary>
		public static T SetActive<T>( this T self, bool value ) where T : Component
		{
			self.gameObject.SetActive( value );
			return self;
		}

		/// <summary>
		/// 子オブジェクトを名前で検索します
		/// </summary>
		public static Transform Find( this Component self, string name )
 		{
			return self.transform.Find( name );
		}

		/// <summary>
		/// 子オブジェクトを名前で検索して GameObject 型で取得します
		/// </summary>
		public static GameObject FindGameObject( this Component self, string name )
		{
			var result = self.transform.Find( name );
			return result != null ? result.gameObject : null;
		}

		/// <summary>
		/// 子オブジェクトを検索してコンポーネントを取得
		/// </summary>
		public static T FindGetComponent<T>( this Component self, string name ) where T : Component
		{
			var result = self.transform.Find( name );
			return result != null ? result.GetComponent<T>() : null;
		}

		/// <summary>
		/// 深い階層まで子オブジェクトを名前で検索して GameObject 型で取得します
		/// </summary>
		public static GameObject FindDeep( this Component self, string name, bool includeInactive = false )
		{
			var children = self.GetComponentsInChildren<Transform>( includeInactive );
			foreach ( var transform in children )
			{
				if ( transform.name == name )
				{
					return transform.gameObject;
				}
			}
			return null;
		}

		/// <summary>
		/// 位置を(0, 0, 0)にリセットします
		/// </summary>
		public static T ResetPosition<T>( this T self ) where T : Component
		{
			self.transform.position = Vector3.zero;
			return self;
		}

		/// <summary>
		/// 位置を返します
		/// </summary>
		public static Vector3 GetPosition( this Component self )
		{
			return self.transform.position;
		}

		/// <summary>
		/// X 座標を返します
		/// </summary>
		public static float GetPositionX( this Component self )
		{
			return self.transform.position.x;
		}

		/// <summary>
		/// Y 座標を返します
		/// </summary>
		public static float GetPositionY( this Component self )
		{
			return self.transform.position.y;
		}

		/// <summary>
		/// Z 座標を返します
		/// </summary>
		public static float GetPositionZ( this Component self )
		{
			return self.transform.position.z;
		}

		/// <summary>
		/// X 座標を設定します
		/// </summary>
		public static T SetPositionX<T>( this T self, float x ) where T : Component
		{
			self.transform.position = new Vector3
			(
				x,
				self.transform.position.y,
				self.transform.position.z
			);
			return self;
		}

		/// <summary>
		/// Y 座標を設定します
		/// </summary>
		public static T SetPositionY<T>( this T self, float y ) where T : Component
		{
			self.transform.position = new Vector3
			(
				self.transform.position.x,
				y,
				self.transform.position.z
			);
			return self;
		}

		/// <summary>
		/// Z 座標を設定します
		/// </summary>
		public static T SetPositionZ<T>( this T self, float z ) where T : Component
		{
			self.transform.position = new Vector3
			(
				self.transform.position.x,
				self.transform.position.y,
				z
			);
			return self;
		}

		/// <summary>
		/// Vector3 型で位置を設定します
		/// </summary>
		public static T SetPosition<T>( this T self, Vector3 v ) where T : Component
		{
			self.transform.position = v;
			return self;
		}

		/// <summary>
		/// Vector2 型で位置を設定します
		/// </summary>
		public static T SetPosition<T>( this T self, Vector2 v ) where T : Component
		{
			self.transform.position = new Vector3
			(
				v.x,
				v.y,
				self.transform.position.z
			);
			return self;
		}

		/// <summary>
		/// 位置を設定します
		/// </summary>
		public static T SetPosition<T>( this T self, float x, float y, float? z = null ) where T : Component
		{
			self.transform.position = new Vector3
			(
				x,
				y,
				!z.HasValue ? self.transform.position.z : z.Value
			);
			return self;
		}

		/// <summary>
		/// X 座標に加算します
		/// </summary>
		public static T AddPositionX<T>( this T self, float x ) where T : Component
		{
			self.transform.position = new Vector3
			(
				self.transform.position.x + x,
				self.transform.position.y,
				self.transform.position.z
			);
			return self;
		}

		/// <summary>
		/// Y 座標に加算します
		/// </summary>
		public static T AddPositionY<T>( this T self, float y ) where T : Component
		{
			self.transform.position = new Vector3
			(
				self.transform.position.x,
				self.transform.position.y + y,
				self.transform.position.z
			);
			return self;
		}

		/// <summary>
		/// Z 座標に加算します
		/// </summary>
		public static T AddPositionZ<T>( this T self, float z ) where T : Component
		{
			self.transform.position = new Vector3
			(
				self.transform.position.x,
				self.transform.position.y,
				self.transform.position.z + z
			);
			return self;
		}

		/// <summary>
		/// Vector3 型で位置を加算します
		/// </summary>
		public static T AddPosition<T>( this T self, Vector3 v ) where T : Component
		{
			self.transform.position += v;
			return self;
		}

		/// <summary>
		/// Vector2 型で位置を加算します
		/// </summary>
		public static T AddPosition<T>( this T self, Vector2 v ) where T : Component
		{
			self.transform.position = new Vector3
			(
				self.transform.position.x + v.x,
				self.transform.position.y + v.y,
				self.transform.position.z
			);
			return self;
		}

		/// <summary>
		/// 位置を加算します
		/// </summary>
		public static T AddPosition<T>( this T self, float x, float y, float? z = null ) where T : Component
		{
			self.transform.position = new Vector3
			(
				self.transform.position.x + x,
				self.transform.position.y + y,
				!z.HasValue ? self.transform.position.z : self.transform.position.z + z.Value
			);
			return self;
		}

		/// <summary>
		/// ローカル座標を(0, 0, 0)にリセットします
		/// </summary>
		public static T ResetLocalPosition<T>( this T self ) where T : Component
		{
			self.transform.localPosition = Vector3.zero;
			return self;
		}

		/// <summary>
		/// ローカル座標を返します
		/// </summary>
		public static Vector3 GetLocalPosition( this Component self )
		{
			return self.transform.localPosition;
		}

		/// <summary>
		/// ローカル座標系の X 座標を返します
		/// </summary>
		public static float GetLocalPositionX( this Component self )
		{
			return self.transform.localPosition.x;
		}

		/// <summary>
		/// ローカル座標系の Y 座標を返します
		/// </summary>
		public static float GetLocalPositionY( this Component self )
		{
			return self.transform.localPosition.y;
		}

		/// <summary>
		/// ローカル座標系の Z 座標を返します
		/// </summary>
		public static float GetLocalPositionZ( this Component self )
		{
			return self.transform.localPosition.z;
		}

		/// <summary>
		/// ローカル座標系のX座標を設定します
		/// </summary>
		public static T SetLocalPositionX<T>( this T self, float x ) where T : Component
		{
			self.transform.localPosition = new Vector3
			(
				x,
				self.transform.localPosition.y,
				self.transform.localPosition.z
			);
			return self;
		}

		/// <summary>
		/// ローカル座標系のY座標を設定します
		/// </summary>
		public static T SetLocalPositionY<T>( this T self, float y ) where T : Component
		{
			self.transform.localPosition = new Vector3
			(
				self.transform.localPosition.x,
				y,
				self.transform.localPosition.z
			);
			return self;
		}

		/// <summary>
		/// ローカル座標系のZ座標を設定します
		/// </summary>
		public static T SetLocalPositionZ<T>( this T self, float z ) where T : Component
		{
			self.transform.localPosition = new Vector3
			(
				self.transform.localPosition.x,
				self.transform.localPosition.y,
				z
			);
			return self;
		}

		/// <summary>
		/// Vector3 型でローカル座標を設定します
		/// </summary>
		public static T SetLocalPosition<T>( this T self, Vector3 v ) where T : Component
		{
			self.transform.localPosition = v;
			return self;
		}

		/// <summary>
		/// Vector2 型でローカル座標を設定します
		/// </summary>
		public static T SetLocalPosition<T>( this T self, Vector2 v ) where T : Component
		{
			self.transform.localPosition = new Vector3
			(
				v.x,
				v.y,
				self.transform.localPosition.z
			);
			return self;
		}

		/// <summary>
		/// ローカル座標を設定します
		/// </summary>
		public static T SetLocalPosition<T>( this T self, float x, float y, float? z = null ) where T : Component
		{
			self.transform.localPosition = new Vector3
			(
				x,
				y,
				!z.HasValue ? self.transform.localPosition.z : z.Value
			);
			return self;
		}

		/// <summary>
		/// ローカルのX座標に加算します
		/// </summary>
		public static T AddLocalPositionX<T>( this T self, float x ) where T : Component
		{
			self.transform.localPosition = new Vector3
			(
				self.transform.localPosition.x + x,
				self.transform.localPosition.y,
				self.transform.localPosition.z
			);
			return self;
		}

		/// <summary>
		/// ローカルのY座標に加算します
		/// </summary>
		public static T AddLocalPositionY<T>( this T self, float y ) where T : Component
		{
			self.transform.localPosition = new Vector3
			(
				self.transform.localPosition.x,
				self.transform.localPosition.y + y,
				self.transform.localPosition.z
			);
			return self;
		}

		/// <summary>
		/// ローカルのZ座標に加算します
		/// </summary>
		public static T AddLocalPositionZ<T>( this T self, float z ) where T : Component
		{
			self.transform.localPosition = new Vector3
			(
				self.transform.localPosition.x,
				self.transform.localPosition.y,
				self.transform.localPosition.z + z
			);
			return self;
		}

		/// <summary>
		/// Vector3 型でローカル座標を加算します
		/// </summary>
		public static T AddLocalPosition<T>( this T self, Vector3 v ) where T : Component
		{
			self.transform.localPosition += v;
			return self;
		}

		/// <summary>
		/// Vector2 型でローカル座標を加算します
		/// </summary>
		public static T AddLocalPosition<T>( this T self, Vector2 v ) where T : Component
		{
			self.transform.localPosition = new Vector3
			(
				self.transform.localPosition.x + v.x,
				self.transform.localPosition.y + v.y,
				self.transform.localPosition.z
			);
			return self;
		}

		/// <summary>
		/// ローカル座標を加算します
		/// </summary>
		public static T AddLocalPosition<T>( this T self, float x, float y, float? z = null ) where T : Component
		{
			self.transform.localPosition = new Vector3
			(
				self.transform.localPosition.x + x,
				self.transform.localPosition.y + y,
				!z.HasValue ? self.transform.localPosition.z : self.transform.localPosition.z + z.Value
			);
			return self;
		}

		/// <summary>
		/// ローカル座標系のスケーリング値を(1, 1, 1)にリセットします
		/// </summary>
		public static T ResetLocalScale<T>( this T self ) where T : Component
		{
			self.transform.localScale = Vector3.one;
			return self;
		}

		/// <summary>
		/// ローカル座標系のスケーリング値を返します
		/// </summary>
		public static Vector3 GetLocalScale( this Component self )
		{
			return self.transform.localScale;
		}

		/// <summary>
		/// X 軸方向のローカル座標系のスケーリング値を返します
		/// </summary>
		public static float GetLocalScaleX( this Component self )
		{
			return self.transform.localScale.x;
		}

		/// <summary>
		/// Y 軸方向のローカル座標系のスケーリング値を返します
		/// </summary>
		public static float GetLocalScaleY( this Component self )
		{
			return self.transform.localScale.y;
		}

		/// <summary>
		/// Z 軸方向のローカル座標系のスケーリング値を返します
		/// </summary>
		public static float GetLocalScaleZ( this Component self )
		{
			return self.transform.localScale.z;
		}

		/// <summary>
		/// ワールド座標系のスケーリング値を返します
		/// </summary>
		public static Vector3 GetLossyScale( this Component self )
		{
			return self.transform.lossyScale;
		}

		/// <summary>
		/// X 軸方向のワールド座標系のスケーリング値を返します
		/// </summary>
		public static float GetLossyScaleX( this Component self )
		{
			return self.transform.lossyScale.x;
		}

		/// <summary>
		/// Y 軸方向のワールド座標系のスケーリング値を返します
		/// </summary>
		public static float GetLossyScaleY( this Component self )
		{
			return self.transform.lossyScale.y;
		}

		/// <summary>
		/// Z 軸方向のワールド座標系のスケーリング値を返します
		/// </summary>
		public static float GetLossyScaleZ( this Component self )
		{
			return self.transform.lossyScale.z;
		}

		/// <summary>
		/// XYZの平均値を返します
		/// </summary>
		public static float GetAveScale( this Component self )
		{
			var scale = self.transform.localScale;
			return (scale.x + scale.y + scale.z) / 3f;
		}

		/// <summary>
		/// X 軸方向のローカル座標系のスケーリング値を設定します
		/// </summary>
		public static T SetLocalScaleX<T>( this T self, float x ) where T : Component
		{
			self.transform.localScale = new Vector3
			(
				x,
				self.transform.localScale.y,
				self.transform.localScale.z
			);
			return self;
		}

		/// <summary>
		/// Y 軸方向のローカル座標系のスケーリング値を設定します
		/// </summary>
		public static T SetLocalScaleY<T>( this T self, float y ) where T : Component
		{
			self.transform.localScale = new Vector3
			(
				self.transform.localScale.x,
				y,
				self.transform.localScale.z
			);
			return self;
		}

		/// <summary>
		/// Z 軸方向のローカル座標系のスケーリング値を設定します
		/// </summary>
		public static T SetLocalScaleZ<T>( this T self, float z ) where T : Component
		{
			self.transform.localScale = new Vector3
			(
				self.transform.localScale.x,
				self.transform.localScale.y,
				z
			);
			return self;
		}

		/// <summary>
		/// Vector3 型でローカル座標系のスケーリング値を設定します
		/// </summary>
		public static T SetLocalScale<T>( this T self, Vector3 v ) where T : Component
		{
			self.transform.localScale = v;
			return self;
		}

		/// <summary>
		/// Vector2 型でローカル座標系のスケーリング値を設定します
		/// </summary>
		public static T SetLocalScale<T>( this T self, Vector2 v ) where T : Component
		{
			self.transform.localScale = new Vector3
			(
				v.x,
				v.y,
				self.transform.localScale.z
			);
			return self;
		}

		/// <summary>
		/// ローカル座標系のスケーリング値を設定します
		/// </summary>
		public static T SetLocalScale<T>( this T self, float x, float y, float? z = null ) where T : Component
		{
			self.transform.localScale = new Vector3
			(
				x,
				y,
				!z.HasValue ? self.transform.localScale.z : z.Value
			);
			return self;
		}

		/// <summary>
		/// ローカル座標系のスケーリング値を設定します
		/// </summary>
		public static T SetLocalScale<T>( this T self, float v ) where T : Component
		{
			self.transform.localScale = new Vector3
			(
				v,
				v,
				v
			);
			return self;
		}

		/// <summary>
		/// X 軸方向のローカル座標系のスケーリング値を加算します
		/// </summary>
		public static T AddLocalScaleX<T>( this T self, float x ) where T : Component
		{
			self.transform.localScale = new Vector3
			(
				self.transform.localScale.x + x,
				self.transform.localScale.y,
				self.transform.localScale.z
			);
			return self;
		}

		/// <summary>
		/// Y 軸方向のローカル座標系のスケーリング値を加算します
		/// </summary>
		public static T AddLocalScaleY<T>( this T self, float y ) where T : Component
		{
			self.transform.localScale = new Vector3
			(
				self.transform.localScale.x,
				self.transform.localScale.y + y,
				self.transform.localScale.z
			);
			return self;
		}

		/// <summary>
		/// Z 軸方向のローカル座標系のスケーリング値を加算します
		/// </summary>
		public static T AddLocalScaleZ<T>( this T self, float z ) where T : Component
		{
			self.transform.localScale = new Vector3
			(
				self.transform.localScale.x,
				self.transform.localScale.y,
				self.transform.localScale.z + z
			);
			return self;
		}

		/// <summary>
		/// Vector3 型でローカル座標系のスケーリング値を加算します
		/// </summary>
		public static T AddLocalScale<T>( this T self, Vector3 v ) where T : Component
		{
			self.transform.localScale += v;
			return self;
		}

		/// <summary>
		/// Vector2 型でローカル座標系のスケーリング値を加算します
		/// </summary>
		public static T AddLocalScale<T>( this T self, Vector2 v ) where T : Component
		{
			self.transform.localScale = new Vector3
			(
				self.transform.localScale.x + v.x,
				self.transform.localScale.y + v.y,
				self.transform.localScale.z
			);
			return self;
		}

		/// <summary>
		/// ローカル座標系のスケーリング値を加算します
		/// </summary>
		public static T AddLocalScale<T>( this T self, float x, float y, float? z = null ) where T : Component
		{
			self.transform.localScale = new Vector3
			(
				self.transform.localScale.x + x,
				self.transform.localScale.y + y,
				!z.HasValue ? self.transform.localScale.z : self.transform.localScale.z + z.Value
			);
			return self;
		}

		/// <summary>
		/// ローカル座標系のスケーリング値を加算します
		/// </summary>
		public static T AddLocalScale<T>( this T self, float v ) where T : Component
		{
			self.transform.localScale = new Vector3
			(
				self.transform.localScale.x + v,
				self.transform.localScale.y + v,
				self.transform.localScale.z + v
			);
			return self;
		}

		/// <summary>
		/// 回転角を(0, 0, 0)にリセットします
		/// </summary>
		public static T ResetEulerAngles<T>( this T self ) where T : Component
		{
			self.transform.eulerAngles = Vector3.zero;
			return self;
		}

		/// <summary>
		/// 回転角を返します
		/// </summary>
		public static Vector3 GetEulerAngles( this Component self )
		{
			return self.transform.eulerAngles;
		}

		/// <summary>
		/// X 軸方向の回転角を返します
		/// </summary>
		public static float GetEulerAngleX( this Component self )
		{
			return self.transform.eulerAngles.x;
		}

		/// <summary>
		/// Y 軸方向の回転角を返します
		/// </summary>
		public static float GetEulerAngleY( this Component self )
		{
			return self.transform.eulerAngles.y;
		}

		/// <summary>
		/// Z 軸方向の回転角を返します
		/// </summary>
		public static float GetEulerAngleZ( this Component self )
		{
			return self.transform.eulerAngles.z;
		}

		/// <summary>
		/// 回転角を設定します
		/// </summary>
		public static T SetEulerAngles<T>( this T self, Vector3 v ) where T : Component
		{
			self.transform.eulerAngles = v;
			return self;
		}

		/// <summary>
		/// 回転角を設定します
		/// </summary>
		public static T SetEulerAngles<T>( this T self, float x, float y , float z ) where T : Component
		{
			self.transform.eulerAngles = new Vector3(x, y, z);
			return self;
		}

		/// <summary>
		/// X 軸方向の回転角を設定します
		/// </summary>
		public static T SetEulerAngleX<T>( this T self, float x ) where T : Component
		{
			self.transform.eulerAngles = new Vector3
			(
				x,
				self.transform.eulerAngles.y,
				self.transform.eulerAngles.z
			);
			return self;
		}

		/// <summary>
		/// Y 軸方向の回転角を設定します
		/// </summary>
		public static T SetEulerAngleY<T>( this T self, float y ) where T : Component
		{
			self.transform.eulerAngles = new Vector3
			(
				self.transform.eulerAngles.x,
				y,
				self.transform.eulerAngles.z
			);
			return self;
		}

		/// <summary>
		/// Z 軸方向の回転角を設定します
		/// </summary>
		public static T SetEulerAngleZ<T>( this T self, float z ) where T : Component
		{
			self.transform.eulerAngles = new Vector3
			(
				self.transform.eulerAngles.x,
				self.transform.eulerAngles.y,
				z
			);
			return self;
		}

		/// <summary>
		/// X 軸方向の回転角を加算します
		/// </summary>
		public static T AddEulerAngleX<T>( this T self, float x ) where T : Component
		{
			self.transform.Rotate( x, 0, 0, Space.World );
			return self;
		}

		/// <summary>
		/// Y 軸方向の回転角を加算します
		/// </summary>
		public static T AddEulerAngleY<T>( this T self, float y ) where T : Component
		{
			self.transform.Rotate( 0, y, 0, Space.World );
			return self;
		}

		/// <summary>
		/// Z 軸方向の回転角を加算します
		/// </summary>
		public static T AddEulerAngleZ<T>( this T self, float z ) where T : Component
		{
			self.transform.Rotate( 0, 0, z, Space.World );
			return self;
		}

		/// <summary>
		/// ローカルの回転角を(0, 0, 0)にリセットします
		/// </summary>
		public static T ResetLocalEulerAngles<T>( this T self ) where T : Component
		{
			self.transform.localEulerAngles = Vector3.zero;
			return self;
		}

		/// <summary>
		/// ローカルの回転角を返します
		/// </summary>
		public static Vector3 GetLocalEulerAngles( this Component self )
		{
			return self.transform.localEulerAngles;
		}

		/// <summary>
		/// ローカルの X 軸方向の回転角を返します
		/// </summary>
		public static float GetLocalEulerAngleX( this Component self )
		{
			return self.transform.localEulerAngles.x;
		}

		/// <summary>
		/// ローカルの Y 軸方向の回転角を返します
		/// </summary>
		public static float GetLocalEulerAngleY( this Component self )
		{
			return self.transform.localEulerAngles.y;
		}

		/// <summary>
		/// ローカルの Z 軸方向の回転角を返します
		/// </summary>
		public static float GetLocalEulerAngleZ( this Component self )
		{
			return self.transform.localEulerAngles.z;
		}

		/// <summary>
		/// ローカルの回転角を設定します
		/// </summary>
		public static T SetLocalEulerAngles<T>( this T self, Vector3 v ) where T : Component
		{
			self.transform.localEulerAngles = v;
			return self;
		}

		/// <summary>
		/// ローカルの回転角を設定します
		/// </summary>
		public static T SetLocalEulerAngles<T>( this T self, float x, float y, float z ) where T : Component
		{
			self.transform.localEulerAngles = new Vector3(x, y, z);
			return self;
		}

		/// <summary>
		/// ローカルの X 軸方向の回転角を設定します
		/// </summary>
		public static T SetLocalEulerAngleX<T>( this T self, float x ) where T : Component
		{
			self.transform.localEulerAngles = new Vector3
			(
				x,
				self.transform.localEulerAngles.y,
				self.transform.localEulerAngles.z
			);
			return self;
		}

		/// <summary>
		/// ローカルの Y 軸方向の回転角を設定します
		/// </summary>
		public static T SetLocalEulerAngleY<T>( this T self, float y ) where T : Component
		{
			self.transform.localEulerAngles = new Vector3
			(
				self.transform.localEulerAngles.x,
				y,
				self.transform.localEulerAngles.z
			);
			return self;
		}

		/// <summary>
		/// ローカルの Z 軸方向の回転角を設定します
		/// </summary>
		public static T SetLocalEulerAngleZ<T>( this T self, float z ) where T : Component
		{
			self.transform.localEulerAngles = new Vector3
			(
				self.transform.localEulerAngles.x,
				self.transform.localEulerAngles.y,
				z
			);
			return self;
		}

		/// <summary>
		/// ローカルの X 軸方向の回転角を加算します
		/// </summary>
		public static T AddLocalEulerAngleX<T>( this T self, float x ) where T : Component
		{
			self.transform.Rotate( x, 0, 0, Space.Self );
			return self;
		}

		/// <summary>
		/// ローカルの Y 軸方向の回転角を加算します
		/// </summary>
		public static T AddLocalEulerAngleY<T>( this T self, float y ) where T : Component
		{
			self.transform.Rotate( 0, y, 0, Space.Self );
			return self;
		}

		/// <summary>
		/// ローカルの X 軸方向の回転角を加算します
		/// </summary>
		public static T AddLocalEulerAngleZ<T>( this T self, float z ) where T : Component
		{
			self.transform.Rotate( 0, 0, z, Space.Self );
			return self;
		}

		/// <summary>
		/// 親オブジェクトが存在するかどうかを返します
		/// </summary>
		public static bool HasParent( this Component self )
		{
			return self.transform.parent != null;
		}

		/// <summary>
		/// 親オブジェクトを解除する
		/// </summary>
		public static T ResetParent<T>( this T self ) where T : Component
		{
			self.transform.parent = null;
			return self;
		}

		/// <summary>
		/// 親オブジェクトを設定します
		/// </summary>
		public static T SetParent<T>( this T self, Component parent, Vector3? vec = null ) where T : Component
		{
			self.transform.SetParent( parent != null ? parent.transform : null );
			if ( vec.HasValue )
				self.transform.localPosition = vec.Value;
			return self;
		}

		/// <summary>
		/// 親オブジェクトを設定します
		/// </summary>
		public static T SetParent<T>( this T self, GameObject parent, Vector3? vec = null ) where T : Component
		{
			self.transform.SetParent( parent != null ? parent.transform : null );
			if ( vec.HasValue )
				self.transform.localPosition = vec.Value;
			return self;
		}

		/// <summary>
		/// 親オブジェクトを設定します
		/// </summary>
		public static T SetParent<T>( this T self, Component parent, bool worldPositionStays ) where T : Component
		{
			self.transform.SetParent( parent != null ? parent.transform : null, worldPositionStays );
			return self;
		}

		/// <summary>
		/// 親オブジェクトを設定します
		/// </summary>
		public static T SetParent<T>( this T self, GameObject parent, bool worldPositionStays ) where T : Component
		{
			self.transform.SetParent( parent != null ? parent.transform : null, worldPositionStays );
			return self;
		}

		/// <summary>
		/// ローカル座標を維持して親オブジェクトを設定します
		/// </summary>
		public static T SafeSetParent<T>( this T self, Component parent ) where T : Component
		{
			return SafeSetParent( self, parent.gameObject );
		}

		/// <summary>
		/// ローカル座標を維持して親オブジェクトを設定します
		/// </summary>
		public static T SafeSetParent<T>( this T self, GameObject parent ) where T : Component
		{
			var t = self.transform;
			var localPosition = t.localPosition;
			var localRotation = t.localRotation;
			var localScale = t.localScale;
			t.parent = parent.transform;
			t.localPosition = localPosition;
			t.localRotation = localRotation;
			t.localScale = localScale;
			self.gameObject.layer = parent.layer;
			return self;
		}

		/// <summary>
		/// 子オブジェクトを一括で解除する
		/// </summary>
		public static T DetachChildren<T>( this T self ) where T : Component
		{
			self.transform.DetachChildren();
			return self;
		}

		/// <summary>
		/// 向きを変更します
		/// </summary>
		public static T LookAt<T>( this T self, GameObject target ) where T : Component
		{
			self.transform.LookAt( target.transform );
			return self;
		}

		/// <summary>
		/// 向きを変更します
		/// </summary>
		public static T LookAt<T>( this T self, Transform target ) where T : Component
		{
			self.transform.LookAt( target );
			return self;
		}

		/// <summary>
		/// 向きを変更します
		/// </summary>
		public static T LookAt<T>( this T self, Vector3 worldPosition ) where T : Component
		{
			self.transform.LookAt( worldPosition );
			return self;
		}

		/// <summary>
		/// 向きを変更します
		/// </summary>
		public static T LookAt<T>( this T self, GameObject target, Vector3 worldUp ) where T : Component
		{
			self.transform.LookAt( target.transform, worldUp );
			return self;
		}

		/// <summary>
		/// 向きを変更します
		/// </summary>
		public static T LookAt<T>( this T self, Transform target, Vector3 worldUp ) where T : Component
		{
			self.transform.LookAt( target, worldUp );
			return self;
		}

		/// <summary>
		/// 向きを変更します
		/// </summary>
		public static T LookAt<T>( this T self, Vector3 worldPosition, Vector3 worldUp ) where T : Component
		{
			self.transform.LookAt( worldPosition, worldUp );
			return self;
		}

		/// <summary>
		/// 子オブジェクトの数を返します
		/// </summary>
		public static int ChildCount( this Component self )
		{
			return self.transform.childCount;
		}

		/// <summary>
		/// 子オブジェクトが存在するかどうかを返します
		/// </summary>
		public static bool HasChild( this Component self )
		{
			return 0 < self.transform.childCount;
		}

		/// <summary>
		/// 指定されたインデックスの子オブジェクトを返します
		/// </summary>
		public static Transform GetChild( this Component self, int index = 0 )
		{
			if ( index < 0 ) return null;
			return index <= self.transform.childCount - 1 ? self.transform.GetChild( index ) : null;
		}

		/// <summary>
		/// 子オブジェクトにする
		/// </summary>
		public static T AddChild<T>( this T self, params GameObject[] parameters ) where T : Component
		{
			for ( int i = 0; i < parameters.Length; i++ )
			{
				parameters[ i ].transform.parent = self.transform;
			}
			return self;
		}

		/// <summary>
		/// 子オブジェクトにする
		/// </summary>
		public static T AddChild<T>( this T self, params Component[] parameters ) where T : Component
		{
			for ( int i = 0; i < parameters.Length; i++ )
			{
				parameters[ i ].transform.parent = self.transform;
			}
			return self;
		}

		/// <summary>
		/// 親オブジェクトを返します
		/// </summary>
		public static Transform GetParent( this Component self )
		{
			return self.transform.parent;
		}

		/// <summary>
		/// 親オブジェクトを名前で検索して返します
		/// </summary>
		public static Transform FindParent( this Component self, string name )
		{
			var parent = self.transform.parent;
			while ( parent )
			{
				if ( parent.name == name )
				{
					return parent;
				}
				parent = parent.transform.parent;
			}
			return parent;
		}

		/// <summary>
		/// ルートとなるオブジェクトを返します
		/// </summary>
		public static GameObject GetRoot( this Component self )
		{
			var root = self.transform.root;
			return root != null ? root.gameObject : null;
		}

		/// <summary>
		/// レイヤーを取得します
		/// </summary>
		public static int GetLayer( this Component self )
		{
			return self.gameObject.layer;
		}

		/// <summary>
		/// レイヤーを設定します
		/// </summary>
		public static T SetLayer<T>( this T self, int layer ) where T : Component
		{
			self.gameObject.layer = layer;
			return self;
		}

		/// <summary>
		/// レイヤー名を使用してレイヤーを設定します
		/// </summary>
		public static T SetLayer<T>( this T self, string layerName ) where T : Component
		{
			self.gameObject.layer = LayerMask.NameToLayer( layerName );
			return self;
		}

		/// <summary>
		/// 自分自身を含めたすべての子オブジェクトのレイヤーを設定します
		/// </summary>
		public static T SetLayerRecursively<T>( this T self, int layer ) where T : Component
		{
			self.gameObject.layer = layer;
			foreach ( Transform n in self.gameObject.transform )
			{
				SetLayerRecursively( n, layer );
			}
			return self;
		}

		/// <summary>
		/// 自分自身を含めたすべての子オブジェクトのレイヤーを設定します
		/// </summary>
		public static T SetLayerRecursively<T>( this T self, string layerName ) where T : Component
		{
			self.SetLayerRecursively( LayerMask.NameToLayer( layerName ) );
			return self;
		}

		/// <summary>
		/// グローバル座標系における X 軸方向のスケーリング値を返します
		/// </summary>
		public static float GetGlobalScaleX( this Component self )
		{
			var t = self.transform;
			var x = 1f;
			while ( t != null )
			{
				x *= t.localScale.x;
				t = t.parent;
			}
			return x;
		}

		/// <summary>
		/// グローバル座標系における Y 軸方向のスケーリング値を返します
		/// </summary>
		public static float GetGlobalScaleY( this Component self )
		{
			var t = self.transform;
			var y = 1f;
			while ( t != null )
			{
				y *= t.localScale.y;
				t = t.parent;
			}
			return y;
		}

		/// <summary>
		/// グローバル座標系における Z 軸方向のスケーリング値を返します
		/// </summary>
		public static float GetGlobalScaleZ( this Component self )
		{
			var t = self.transform;
			var z = 1f;
			while ( t != null )
			{
				z *= t.localScale.z;
				t = t.parent;
			}
			return z;
		}

		/// <summary>
		/// グローバル座標系におけるスケーリング値を返します
		/// </summary>
		public static Vector3 GetGlobalScale( this Component self )
		{
			var t = self.transform;
			var scale = Vector3.one;
			while ( t != null )
			{
				scale.x *= t.localScale.x;
				scale.y *= t.localScale.y;
				scale.z *= t.localScale.z;
				t = t.parent;
			}
			return scale;
		}

		/// <summary>
		/// 指定されたゲームオブジェクトが null または非アクティブであるかどうかを示します
		/// </summary>
		public static bool IsNullOrInactive( this Component self )
		{
			return self.gameObject == null || !self.gameObject.activeInHierarchy || !self.gameObject.activeSelf;
		}

		/// <summary>
		/// 指定されたゲームオブジェクトが null ではないかつ非アクティブではないかどうかを示します
		/// </summary>
		public static bool IsNotNullOrInactive( this Component self )
		{
			return !self.IsNullOrInactive();
		}

		/// <summary>
		/// コンポーネントのインターフェースを取得して返します
		/// </summary>
		public static T GetComponentInterface<T>( this Component self ) where T : class
		{
			foreach ( var n in self.GetComponents<Component>() )
			{
				var component = n as T;
				if ( component != null )
				{
					return component;
				}
			}
			return null;
		}

		/// <summary>
		/// コンポーネントのインターフェースを複数取得して返します
		/// </summary>
		public static T[] GetComponentInterfaces<T>( this Component self ) where T : class
		{
			var result = new List<T>();
			foreach ( var n in self.GetComponents<Component>() )
			{
				var component = n as T;
				if ( component != null )
				{
					result.Add( component );
				}
			}
			return result.ToArray();
		}

		/// <summary>
		/// 子のコンポーネントのインターフェースを取得して返します
		/// </summary>
		public static T GetComponentInterfaceInChildren<T>( this Component self ) where T : class
		{
			return self.GetComponentInterfaceInChildren<T>( false );
		}

		/// <summary>
		/// 子のコンポーネントのインターフェースを取得して返します
		/// </summary>
		public static T GetComponentInterfaceInChildren<T>( this Component self, bool includeInactive ) where T : class
		{
			foreach ( var n in self.GetComponentsInChildren<Component>( includeInactive ) )
			{
				var component = n as T;
				if ( component != null )
				{
					return component;
				}
			}
			return null;
		}

		/// <summary>
		/// 子のコンポーネントのインターフェースを複数取得して返します
		/// </summary>
		public static T[] GetComponentInterfacesInChildren<T>( this Component self ) where T : class
		{
			return self.GetComponentInterfacesInChildren<T>( false );
		}

		/// <summary>
		/// 子のコンポーネントのインターフェースを複数取得して返します
		/// </summary>
		public static T[] GetComponentInterfacesInChildren<T>( this Component self, bool includeInactive ) where T : class
		{
			var result = new List<T>();
			foreach ( var n in self.GetComponentsInChildren<Component>( includeInactive ) )
			{
				var component = n as T;
				if ( component != null )
				{
					result.Add( component );
				}
			}
			return result.ToArray();
		}

		/// <summary>
		/// 無効なコンポーネントがアタッチされている場合 true を返します
		/// </summary>
		public static bool HasMissingScript( this Component self )
		{
			return self
				.GetComponents<Component>()
				.Any( c => c == null )
			;
		}

		/// <summary>
		/// アクティブ状態を逆にします
		/// </summary>
		public static T ReverseActive<T>( this T self ) where T : Component
		{
			self.SetActive( !self.gameObject.activeSelf );
			return self;
		}

		/// <summary>
		/// 指定されたアクティブと逆の状態にしてから指定されたアクティブになります
		/// </summary>
		public static T ToggleActive<T>( this T self, bool isActive ) where T : Component
		{
			self.SetActive( !isActive );
			self.SetActive( isActive );
			return self;
		}

		/// <summary>
		/// すべての親オブジェクトを返します
		/// </summary>
		public static GameObject[] GetAllParent( this Component self )
		{
			var result = new List<GameObject>();
			for ( var parent = self.transform.parent; parent != null; parent = parent.parent )
			{
				result.Add( parent.gameObject );
			}
			return result.ToArray();
		}

		/// <summary>
		/// ルートパスを返します
		/// </summary>
		public static string GetRootPath( this Component self )
		{
			var path   = self.name;
			var parent = self.transform.parent;

			while ( parent != null )
			{
				path   = parent.name + "/" + path;
				parent = parent.parent;
			}

			return path;
		}

		/// <summary>
		/// グローバル座標を返します
		/// </summary>
		public static Vector3 GetGlobalPosition( this Component self )
		{
			var result = Vector3.zero;
			while ( self != null )
			{
				var t = self.transform;
				result += t.localPosition;
				self = t.parent;
			}
			return result;
		}

		/// <summary>
		/// ローカル座標系の位置を四捨五入します
		/// </summary>
		public static T RoundLocalPosition<T>( this T self ) where T : Component
		{
			var v = self.transform.localPosition;
			v.x = Mathf.Round( v.x );
			v.y = Mathf.Round( v.y );
			v.z = Mathf.Round( v.z );
			self.transform.localPosition = v;
			return self;
		}

		/// <summary>
		/// ローカル座標系の回転角を四捨五入します
		/// </summary>
		public static T RoundLocalEulerAngles<T>( this T self ) where T : Component
		{
			var v = self.transform.localEulerAngles;
			v.x = Mathf.Round( v.x );
			v.y = Mathf.Round( v.y );
			v.z = Mathf.Round( v.z );
			self.transform.localEulerAngles = v;
			return self;
		}

		/// <summary>
		/// ローカル座標系のスケーリング値を四捨五入します
		/// </summary>
		public static T RoundLocalScale<T>( this T self ) where T : Component
		{
			var v = self.transform.localScale;
			v.x = Mathf.Round( v.x );
			v.y = Mathf.Round( v.y );
			v.z = Mathf.Round( v.z );
			self.transform.localScale = v;
			return self;
		}

		/// <summary>
		/// ローカル座標系の位置、回転角、スケーリング値を四捨五入します
		/// </summary>
		public static T Round<T>( this T self ) where T : Component
		{
			self.RoundLocalPosition();
			self.RoundLocalEulerAngles();
			self.RoundLocalScale();
			return self;
		}

		/// <summary>
		/// 指定された名前にする
		/// </summary>
		public static T SetName<T>( this T self, string name ) where T : Component
		{
			self.name = name;
			return self;
		}

		/// <summary>
		/// 指定されたタグ名にする
		/// </summary>
		public static T SetTag<T>( this T self, string name ) where T : Component
		{
			self.tag = name;
			return self;
		}

		/// <summary>
		/// 指定された名前とタグ名にする
		/// </summary>
		public static T SetNameAndTag<T>( this T self, string name, string tagName ) where T : Component
		{
			self.name = name;
			self.tag = tagName;
			return self;
		}

		/// <summary>
		/// オブジェクトがNullならオブジェクトを生成します。
		/// </summary>
		public static T IfNullInstantiate<T>( this T self, T prefab ) where T : Component
		{
			return self.gameObject != null ? self : GameObject.Instantiate( prefab ) as T;
		}

		/// <summary>
		/// ゲームオブジェクトを生成して座標、スケール、親オブジェクトを同一にします
		/// </summary>
		public static T Clone<T>( this T self ) where T : Component
		{
			if( self.gameObject != null ) return self;

			var go = GameObject.Instantiate( self ) as T;
			go.transform.parent = self.transform.parent;
			go.transform.localPosition = self.transform.localPosition;
			go.transform.localScale = self.transform.localScale;
			return go;
		}

		/// <summary>
        /// イベント送信
        /// </summary>
        public static T SendEvent<T>( this T self ) where T : Component
        {
			if (self == null) return self;
            if (!UnityEngine.EventSystems.ExecuteEvents.Execute<IReceiveEvent>(self.gameObject, null, (target, ev) => target.OnReceive()))
            {
                for (int i = 0; i < self.transform.childCount; i++)
                {
                    self.transform.GetChild(i).SendEvent();
                }
            }
			return self;
        }

		/// <summary>
        /// イベント送信
        /// </summary>
        public static T SendEventObject<T>( this T self, object obj ) where T : Component
        {
			if (self == null) return self;
            if (!UnityEngine.EventSystems.ExecuteEvents.Execute<IReceiveEventObject>(self.gameObject, null, (target, ev) => target.OnReceiveObject( obj )))
            {
                for (int i = 0; i < self.transform.childCount; i++)
                {
                    self.transform.GetChild(i).SendEventObject( obj );
                }
            }
			return self;
        }

		/// <summary>
        /// イベント送信
        /// </summary>
        public static T SendEventObjects<T>( this T self, params object[] args ) where T : Component
        {
			if (self == null) return self;
            if (!UnityEngine.EventSystems.ExecuteEvents.Execute<IReceiveEventObjects>(self.gameObject, null, (target, ev) => target.OnReceiveObjects( args )))
            {
                for (int i = 0; i < self.transform.childCount; i++)
                {
                    self.transform.GetChild(i).SendEventObjects( args );
                }
            }
			return self;
        }

		/// <summary>
        /// selfから一番近いオブジェクトを検索
        /// </summary>
		public static T FindNearObject<T>( this Vector2 self, IList<T> list ) where T : Component
		{
			var tempDis = 0f;
			var nearDis = 0f;
			var result = null as T;
			foreach ( var t in list )
			{
				tempDis = Vector2.Distance( self, t.transform.position );
				if ( nearDis == 0f || nearDis > tempDis )
				{
					nearDis = tempDis;
					result = t;
				}
			}
			return result;
		}

		/// <summary>
        /// selfから一番近いオブジェクトを検索
        /// </summary>
		public static T FindNearObject<T>( this Vector3 self, IList<T> list ) where T : Component
		{
			var tempDis = 0f;
			var nearDis = 0f;
			var result = null as T;
			foreach ( var t in list )
			{
				tempDis = Vector3.Distance( self, t.transform.position );
				if ( nearDis == 0f || nearDis > tempDis )
				{
					nearDis = tempDis;
					result = t;
				}
			}
			return result;
		}

		/// <summary>
        /// selfから一番近いオブジェクトを検索
        /// </summary>
		public static T FindNearObject<T>( this GameObject self, IList<T> list ) where T : Component
		{
			return !self ? null : self.transform.position.FindNearObject( list );
		}

		/// <summary>
        /// selfから一番近いオブジェクトを検索
        /// </summary>
		public static T FindNearObject<T>( this Component self, IList<T> list ) where T : Component
		{
			return !self.gameObject ? null : self.transform.position.FindNearObject( list );
		}

		/// <summary>
        /// selfから一番遠いオブジェクトを検索
        /// </summary>
		public static T FindFarObject<T>( this Vector2 self, IList<T> list ) where T : Component
		{
			var tempDis = 0f;
			var nearDis = 0f;
			var result = null as T;
			foreach ( var t in list )
			{
				tempDis = Vector2.Distance( self, t.transform.position );
				if ( nearDis == 0f || nearDis < tempDis )
				{
					nearDis = tempDis;
					result = t;
				}
			}
			return result;
		}

		/// <summary>
        /// selfから一番遠いオブジェクトを検索
        /// </summary>
		public static T FindFarObject<T>( this Vector3 self, IList<T> list ) where T : Component
		{
			var tempDis = 0f;
			var nearDis = 0f;
			var result = null as T;
			foreach ( var t in list )
			{
				tempDis = Vector3.Distance( self, t.transform.position );
				if ( nearDis == 0f || nearDis < tempDis )
				{
					nearDis = tempDis;
					result = t;
				}
			}
			return result;
		}

		/// <summary>
        /// selfから一番遠いオブジェクトを検索
        /// </summary>
		public static T FindFarObject<T>( this GameObject self, IList<T> list ) where T : Component
		{
			return !self ? null : self.transform.position.FindFarObject( list );
		}

		/// <summary>
        /// selfから一番遠いオブジェクトを検索
        /// </summary>
		public static T FindFarObject<T>( this Component self, IList<T> list ) where T : Component
		{
			return !self.gameObject ? null : self.transform.position.FindFarObject( list );
		}

		/// <summary>
        /// 引数からの距離を昇順
        /// </summary>
		public static List<T> SortByDistance<T>( this List<T> self, Vector2 pos ) where T : Component
		{
			self.Sort( delegate( T a, T b ) { return Vector2.Distance( pos, a.transform.position ).CompareTo( Vector2.Distance( pos, b.transform.position ) ); } );
			return self;
		}

		/// <summary>
        /// 引数からの距離を昇順
        /// </summary>
		public static List<T> SortByDistance<T>( this List<T> self, Vector3 pos ) where T : Component
		{
			self.Sort( delegate( T a, T b ) { return Vector3.Distance( pos, a.transform.position ).CompareTo( Vector3.Distance( pos, b.transform.position ) ); } );
			return self;
		}

		/// <summary>
        /// 引数からの距離を昇順
        /// </summary>
		public static List<T> SortByDistance<T>( this List<T> self, GameObject target ) where T : Component
		{
			return !target ? self : self.SortByDistance( target.transform.position );
		}

		/// <summary>
        /// 引数からの距離を昇順
        /// </summary>
		public static List<T> SortByDistance<T>( this List<T> self, Component target ) where T : Component
		{
			return !target.gameObject ? self : self.SortByDistance( target.transform.position );
		}

		/// <summary>
        /// 引数からの距離を降順
        /// </summary>
		public static List<T> SortDescendingByDistance<T>( this List<T> self, Vector2 pos ) where T : Component
		{
			self.Sort( delegate( T a, T b ) { return Vector2.Distance( pos, b.transform.position ).CompareTo( Vector2.Distance( pos, a.transform.position ) ); } );
			return self;
		}

		/// <summary>
        ///	引数からの距離を降順
        /// </summary>
		public static List<T> SortDescendingByDistance<T>( this List<T> self, Vector3 pos ) where T : Component
		{
			self.Sort( delegate( T a, T b ) { return Vector3.Distance( pos, b.transform.position ).CompareTo( Vector3.Distance( pos, a.transform.position ) ); } );
			return self;
		}

		/// <summary>
        /// 引数からの距離を降順
        /// </summary>
		public static List<T> SortDescendingByDistance<T>( this List<T> self, GameObject target ) where T : Component
		{
			return !target ? self : self.SortDescendingByDistance( target.transform.position );
		}

		/// <summary>
        /// 引数からの距離を降順
        /// </summary>
		public static List<T> SortDescendingByDistance<T>( this List<T> self, Component target ) where T : Component
		{
			return !target.gameObject ? self : self.SortDescendingByDistance( target.transform.position );
		}

		/// <summary>
		/// 新しいシーンを読み込む時に自動で破棄されないようにします
		/// </summary>
		public static T DontDestroyOnLoad<T>( this T self ) where T : Component
		{
			GameObject.DontDestroyOnLoad( self );
			return self;
		}

		/// <summary>
		/// オブジェクトを破棄
		/// </summary>
		public static T Destroy<T>( this T self, float? time = null ) where T : Component
		{
			if ( !time.HasValue )
				GameObject.Destroy( self );
			else
				GameObject.Destroy( self, time.Value );
			return self;
		}

		/// <summary>
		/// オブジェクトを破棄
		/// </summary>
		public static T DestroyImmediate<T>( this T self, bool allowDestroyingAssets = false ) where T : Component
		{
			GameObject.DestroyImmediate( self, allowDestroyingAssets );
			return self;
		}

		/// <summary>
		/// 同階層における順序を返す
		/// </summary>
		public static int GetSiblingIndex( this Component self )
		{
			return self.transform.GetSiblingIndex();
		}

		/// <summary>
		/// 同階層における順序をindex番にして返す
		/// </summary>
		public static T SetSiblingIndex<T>( this T self, int index ) where T : Component
		{
			self.transform.SetSiblingIndex( index );
			return self;
		}

		/// <summary>
		/// 同階層における順序を最初にして返す
		/// </summary>
		public static T SetAsFirstSibling<T>( this T self ) where T : Component
		{
			self.transform.SetAsFirstSibling();
			return self;
		}

		/// <summary>
		/// 同階層における順序を最後にして返す
		/// </summary>
		public static T SetAsLastSibling<T>( this T self ) where T : Component
		{
			self.transform.SetAsLastSibling();
			return self;
		}
	}
}