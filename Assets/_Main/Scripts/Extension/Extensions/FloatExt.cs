﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Main
{
	/// <summary>
	/// float 型の拡張メソッドを管理するクラス
	/// </summary>
	public static class FloatExt
	{
		/// <summary>
		/// しきい値を考慮して指定した値と等しいかどうかを判断します
		/// </summary>
		public static bool SafeEquals( this float self, float obj, float threshold = 0.001f )
		{
			return Mathf.Abs( self - obj ) <= threshold;
		}

		/// <summary>
		/// 値が正常かどうかを返します
		/// </summary>
		public static bool IsValidated( this float self )
		{
			return !float.IsInfinity( self ) && !float.IsNaN( self );
		}

		/// <summary>
		/// 値が不正かどうかを確認し、値を返します
		/// </summary>
		public static float GetValueOrDefault( this float self, float defaultValue = 0 )
		{
			if ( float.IsInfinity( self ) || float.IsNaN( self ) )
			{
				return defaultValue;
			}
			return self;
		}

		/// <summary>
		/// Int型に変換して返す
		/// </summary>
		public static int ToInt( this float self )
		{
			return (int) self;
		}

		/// <summary>
		/// 通貨記号と、必要に応じて桁区切り記号が入った文字列を返します
		/// </summary>
		public static string DigitSeparator( this float self, int numberOfDigits = 0 )
		{
			return self.ToString( "N" + numberOfDigits.ToString() );
		}

		/// <summary>
		/// <para>数値に指定された桁数の固定小数点数を付加した文字列を返します</para>
		/// <para>123.FixedPoint(2) → 123.00</para>
		/// <para>123.FixedPoint(4) → 123.0000</para>
		/// </summary>
		public static string FixedPoint( this float self, int numberOfDigits )
		{
			return self.ToString( "F" + numberOfDigits.ToString() );
		}

		/// <summary>
		/// n 乗して返す
		/// </summary>
		public static float Pow( this float self, int n )
		{
			return Mathf.Pow( self, n );
		}

		/// <summary>
		/// n 乗して返す
		/// </summary>
		public static float Pow( this float self, float n )
		{
			return Mathf.Pow( self, n );
		}

		/// <summary>
		/// 切り上げして返す
		/// </summary>
		public static float Ceil( this float self )
		{
			return Mathf.Ceil( self );
		}

		/// <summary>
		/// 切り捨てして返す
		/// </summary>
		public static float Floor( this float self )
		{
			return Mathf.Floor( self );
		}

		/// <summary>
		/// 四捨五入して返す
		/// </summary>
		public static float Round( this float self )
		{
			return Mathf.Round( self );
		}

		/// <summary>
		/// 小数点第 n 位で四捨五入して返す
		/// </summary>
		public static float Round( this float self, int n )
		{
			return (float) Math.Round( self, n, MidpointRounding.AwayFromZero );
		}

       	/// <summary>
		/// 引数を比較して大きい方を返す
		/// </summary>
		public static float Max( this float self, float max )
		{
			return Mathf.Max( self , max );
		}

       	/// <summary>
		/// 引数を比較して小さい方を返す
		/// </summary>
		public static float Min( this float self, float min )
		{
			return Mathf.Min( self, min );
		}

       	/// <summary>
		/// 引数を最小、最大の範囲内で返す
		/// </summary>
		public static float Clamp( this float self, float min, float max )
		{
			return Mathf.Clamp( self, min, max );
		}

       	/// <summary>
		/// 引数を0 ~ 1の範囲内で返す
		/// </summary>
		public static float Clamp01( this float self )
		{
			return Mathf.Clamp01( self );
		}

		/// <summary>
		/// 切り上げして返す
		/// </summary>
		public static int CeilToInt( this float self )
		{
			return Mathf.CeilToInt( self );
		}

		/// <summary>
		/// 切り捨てして返す
		/// </summary>
		public static int FloorToInt( this float self )
		{
			return Mathf.FloorToInt( self );
		}

		/// <summary>
		/// 四捨五入して返す
		/// </summary>
		public static int RoundToInt( this float self )
		{
			return Mathf.RoundToInt( self );
		}

		/// <summary>
		/// 引数の絶対値を返す
		/// </summary>
		public static float Abs( this float self )
		{
			return Mathf.Abs( self );
		}

		/// <summary>
		/// 引数の平方根を返す
		/// </summary>
		public static float Sqrt( this float self )
		{
			return Mathf.Sqrt( self );
		}

		/// <summary>
		/// 最小値と最大値を任意の比率の値を返す
		/// </summary>
		public static float Lerp( this float self, float min, float max )
		{
			return Mathf.Lerp( min, max, self );
		}

		/// <summary>
		/// 最小値と最大値を任意の比率の値を返す（制限無し）
		/// </summary>
		public static float LerpUnclamped( this float self, float min, float max )
		{
			return Mathf.LerpUnclamped( min, max, self );
		}

		/// <summary>
		/// 最小値と最大値の間での比率を返す
		/// </summary>
		public static float InverseLerp( this float self, float min, float max )
		{
			return Mathf.InverseLerp( min, max, self );
		}

		/// <summary>
		/// 値にラジアンに変換したものをSin関数で返す
		/// </summary>
		public static float SinDeg2Rad( this float self, float value = 1f )
		{
			return Mathf.Sin( self * Mathf.Deg2Rad ) * value;
		}

		/// <summary>
		/// 値にラジアンに変換したものをCos関数で返す
		/// </summary>
		public static float CosDeg2Rad( this float self, float value = 1f )
		{
			return Mathf.Cos( self * Mathf.Deg2Rad ) * value;
		}

		/// <summary>
		/// 値にラジアンに変換したものをTan関数で返す
		/// </summary>
		public static float TanDeg2Rad( this float self, float value = 1f )
		{
			return Mathf.Tan( self * Mathf.Deg2Rad ) * value;
		}

		/// <summary>
		/// min から max の間の浮動小数点数をランダムにして加算する
		/// </summary>
		public static float RandomRange( this float self, float min, float max )
		{
			return self + RandomUtils.Range( min, max );
		}

		/// <summary>
		/// 指定した値を超えたらTrueを返す
		/// </summary>
		public static bool DeltaTimer( this ref float self, float max )
		{
			if ( max <= 0f ) return true;
			self += Time.deltaTime;
			if ( self >= max )
			{
				self = 0f;
				return true;
			}
			return false;
		}

		/// <summary>
		/// 指定した値を超えたらTrueを返す
		/// </summary>
		public static bool FixedDeltaTimer( this ref float self, float max )
		{
			if ( max <= 0f ) return true;
			self += Time.fixedDeltaTime;
			if ( self >= max )
			{
				self = 0f;
				return true;
			}
			return false;
		}

		/// <summary>
		/// 値が正常ではない場合引数を返す
		/// </summary>
		public static float ToDivisionByZero( this float self, float def = default (float) )
		{
			if (float.IsInfinity(self) | float.IsNaN(self))
			{
				return def;
			}
			return self;
		}

		/// <summary>
		/// 任意の座標に変換して返す
		/// </summary>
		public static Vector2 ToVec2( this float self )
		{
			return new Vector2( self, self );
		}

		/// <summary>
		/// 任意の座標に変換して返す
		/// </summary>
		public static Vector2Int ToVec2Int(this float self)
		{
			return new Vector2Int( (int)self, (int)self );
		}

		/// <summary>
		/// 任意の座標に変換して返す
		/// </summary>
		public static Vector3 ToVec3( this float self )
		{
			return new Vector3( self, self, self );
		}

		/// <summary>
		/// 任意の座標に変換して返す
		/// </summary>
		public static Vector3Int ToVec3Int( this float self )
		{
			return new Vector3Int( (int)self, (int)self, (int)self );
		}
	}
}