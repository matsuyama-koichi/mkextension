﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Main
{
    public static class PhysicsMaterial2DExt
    {
        /// <summary>
        /// PhysicsMaterial2Dを設定する
        /// </summary>
        /// <param name="self"></param>
        /// <param name="friction">コライダーの摩擦係数</param>
        /// <param name="bounciness">衝突が表面から弾むときの強さ</param>
        /// <returns></returns>
        public static PhysicsMaterial2D Set( this PhysicsMaterial2D self, float friction, float bounciness )
        {
            self.friction = friction;
            self.bounciness = bounciness;
            return self;
        }

        /// <summary>
        /// PhysicsMaterial2Dを設定する
        /// </summary>
        /// <param name="self"></param>
        /// <param name="v">コライダーの摩擦係数</param>
        /// <returns></returns>
        public static PhysicsMaterial2D SetFriction( this PhysicsMaterial2D self, float v )
        {
            self.friction = v;
            return self;
        }

        /// <summary>
        /// PhysicsMaterial2Dを設定する
        /// </summary>
        /// <param name="self"></param>
        /// <param name="v">衝突が表面から弾むときの強さ</param>
        /// <returns></returns>
        public static PhysicsMaterial2D SetBounciness( this PhysicsMaterial2D self, float v )
        {
            self.bounciness = v;
            return self;
        }
    }
}
