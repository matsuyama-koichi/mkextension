﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Main
{
    public static class PhysicsMaterialExt
    {
        /// <summary>
        /// PhysicMaterialを設定をする
        /// </summary>
        /// <param name="self"></param>
        /// <param name="dynamicFriction">移動している物体に対する摩擦</param>
        /// <param name="staticFriction">面上で静止しているオブジェクトに使用される摩擦</param>
        /// <param name="bounciness">表面の弾性</param>
        /// <param name="frictionCombine">衝突するオブジェクト間の摩擦</param>
        /// <param name="bounceCombine">衝突するオブジェクト間の跳ね返し度合い</param>
        /// <returns></returns>
        public static PhysicMaterial Set( this PhysicMaterial self, float dynamicFriction, float staticFriction, float bounciness, PhysicMaterialCombine frictionCombine = default( PhysicMaterialCombine ), PhysicMaterialCombine bounceCombine = default( PhysicMaterialCombine ) )
        {
            self.dynamicFriction = dynamicFriction;
            self.staticFriction = staticFriction;
            self.bounciness = bounciness;
            self.frictionCombine = frictionCombine;
            self.bounceCombine = bounceCombine;
            return self;
        }

        /// <summary>
        /// PhysicMaterialを設定をする
        /// </summary>
        /// <param name="self"></param>
        /// <param name="v">移動している物体に対する摩擦</param>
        /// <returns></returns>
        public static PhysicMaterial SetDynamicFriction( this PhysicMaterial self, float v )
        {
            self.dynamicFriction = v;
            return self;
        }

        /// <summary>
        /// PhysicMaterialを設定をする
        /// </summary>
        /// <param name="self"></param>
        /// <param name="v">面上で静止しているオブジェクトに使用される摩擦</param>
        /// <returns></returns>
        public static PhysicMaterial SetStaticFrictionn( this PhysicMaterial self, float v )
        {
            self.staticFriction = v;
            return self;
        }

        /// <summary>
        /// PhysicMaterialを設定をする
        /// </summary>
        /// <param name="self"></param>
        /// <param name="v">表面の弾性</param>
        /// <returns></returns>
        public static PhysicMaterial SetBounciness( this PhysicMaterial self, float v )
        {
            self.bounciness = v;
            return self;
        }

        /// <summary>
        /// PhysicMaterialを設定をする
        /// </summary>
        /// <param name="self"></param>
        /// <param name="v">衝突するオブジェクト間の摩擦</param>
        /// <returns></returns>
        public static PhysicMaterial SetFrictionCombine( this PhysicMaterial self, PhysicMaterialCombine v )
        {
            self.frictionCombine = v;
            return self;
        }

        /// <summary>
        /// PhysicMaterialを設定をする
        /// </summary>
        /// <param name="self"></param>
        /// <param name="v">衝突するオブジェクト間の跳ね返し度合い</param>
        /// <returns></returns>
        public static PhysicMaterial SetBounceCombine( this PhysicMaterial self, PhysicMaterialCombine v )
        {
            self.bounceCombine = v;
            return self;
        }
    }
}