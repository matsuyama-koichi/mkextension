﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Main
{
    public static class RenderExt
    {
        public static T Enable<T>( this T self ) where T : Renderer
        {
            self.enabled = true;
            return self;
        }

        public static T Disable<T>( this T self ) where T : Renderer
        {
            self.enabled = false;
            return self;
        }
    }
}