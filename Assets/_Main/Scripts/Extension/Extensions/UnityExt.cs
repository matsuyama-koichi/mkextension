﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Main
{
    public static class UnityExt
    {
        /// <summary>
		/// デバッグログを出します
		/// </summary>
        public static T DebugLog<T>( this T self, GameObject obj = null )
        {
            if( obj == null )
                Debug.Log( self );
            else
                Debug.Log( self, obj );
            
            return self;
        }

        /// <summary>
		/// デバッグログを出します
		/// </summary>
        public static T DebugLog<T>( this T self, string msg ,GameObject obj = null )
        {
            if( obj == null )
                Debug.Log( $"{msg}{self}" );
            else
                Debug.Log( $"{msg}{self}", obj );

            return self;
        }

        /// <summary>
		/// デバッグログを出します
		/// </summary>
		public static List<T> DebugLog<T>( this List<T> self, bool lineFeed = false )
		{
            var msg = null as string;
            for ( int i = 0; i < self.Count; i++ )
            {
                if ( !lineFeed )
                {
                    Debug.Log( $"{self[i]}" );
                }
                else
                {
                    if( i <= 0 )
                        msg = $"{self[i]}";
                    else
                        msg += $"\n{self[i]}";
                }
            }
            if( lineFeed )
                Debug.Log( msg );

			return self;
		}

        /// <summary>
		/// デバッグログを出します
		/// </summary>
		public static Array DebugLog<T>( this T[] self, bool lineFeed = false )
		{
            var msg = null as string;
            for ( int i = 0; i < self.Length; i++ )
            {
                if( !lineFeed )
                {
                    Debug.Log( $"{self[i]}" );
                }
                else
                {
                    if( i <= 0 )
                        msg = $"{self[i]}";
                    else
                        msg += $"\n{self[i]}";
                }
            }
            if( lineFeed )
                Debug.Log( msg );
            
			return self;
		}

        /// <summary>
		/// デバッグログを出します
		/// </summary>
		public static Dictionary<TKey, TValue> DebugLog<TKey, TValue>( this Dictionary<TKey, TValue> self, bool lineFeed = false )
		{
            var msg = null as string;
            var index = 0;
            foreach ( var pair in self )
            {
                if( !lineFeed )
                {
                    Debug.Log( $"{pair.Key} / {pair.Value}" );
                }
                else
                {
                    if( index <= 0 )
                        msg = $"{pair.Key} / {pair.Value}";
                    else
                        msg += $"\n{pair.Key} / {pair.Value}";
                    index++;
                }
            }
            if( lineFeed )
                Debug.Log( msg );

			return self;
		}
    }
}
