﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Main
{
	/// <summary>
	/// Dictionary 型の拡張メソッドを管理するクラス
	/// </summary>
	public static class DictionaryExt
	{
		/// <summary>
		/// 指定したキーに関連付けられている値を取得します。キーが存在しない場合は既定値を返します
		/// </summary>
		public static TValue GetOrDefault<TKey, TValue>( this Dictionary<TKey, TValue> self, TKey key, TValue defaultValue = default( TValue ) )
		{
			TValue value;
			return self.TryGetValue( key, out value ) ? value : defaultValue;
		}

		/// <summary>
		/// Hashtable に変換します
		/// </summary>
		public static Hashtable ToHashtable<TKey, TValue>( this Dictionary<TKey, TValue> self )
		{
			var result = new Hashtable();
			foreach ( var n in self )
			{
				result[ n.Key ] = n.Value;
			}
			return result;
		}

		/// <summary>
		/// ランダムに値を返します
		/// </summary>
		public static TValue ElementAtRandom<TKey, TValue>( this Dictionary<TKey, TValue> self )
		{
			return self.ElementAt( UnityEngine.Random.Range( 0, self.Count ) ).Value;
		}


		/// <summary>
		/// クリアします。null の場合は何も行いません
		/// </summary>
		public static Dictionary<TKey, TValue> ClearIfNotNull<TKey, TValue>( this Dictionary<TKey, TValue> self )
		{
			if ( self == null ) return self;
			self.Clear();
			return self;
		}

		/// <summary>
		/// Dictionary 型のインスタンスの各要素に対して、指定された処理を実行します
		/// </summary>
		public static Dictionary<TKey, TValue> ForEach<TKey, TValue>( this Dictionary<TKey, TValue> self , Action<TKey, TValue> action )
		{
			foreach(var pair in self)
			{
				action( pair.Key, pair.Value );
			}
			return self;
		}

		/// <summary>
		/// Dictionary 型のインスタンスの各要素に対して、指定された処理を実行します
		/// </summary>
		public static Dictionary<TKey, TValue> ForEach<TKey, TValue>( this Dictionary<TKey, TValue> self , Action<TKey, TValue, int> action )
		{
			var index = 0;
			foreach(var pair in self)
			{
				action( pair.Key, pair.Value, index );
				index++;
			}
			return self;
		}

		/// <summary>
		/// 複数指定したキーを存在したら削除します
		/// </summary>
		public static Dictionary<TKey, TValue> RemoveByKey<TKey, TValue>( this Dictionary<TKey, TValue> self, IList<TKey> list )
		{
			for(int i = 0; i < list.Count; i++)
			{
				if(self.ContainsKey(list[i]))
					self.Remove(list[i]);
			}
			return self;
		}

		/// <summary>
		/// 複数指定したキーを存在したら削除します
		/// </summary>
		public static Dictionary<TKey, TValue> RemoveByKey<TKey, TValue>( this Dictionary<TKey, TValue> self, params TKey[] list )
		{
			for(int i = 0; i < list.Length; i++)
			{
				if(self.ContainsKey(list[i]))
					self.Remove(list[i]);
			}
			return self;
		}

		/// <summary>
		/// 複数指定したバリューを存在したら削除します
		/// </summary>
		public static Dictionary<TKey, TValue> RemoveByValue<TKey, TValue>( this Dictionary<TKey, TValue> self, IList<TValue> list )
		{
			for(int i = 0; i < list.Count; i++)
			{
				var removeKeys = self.Where(x => EqualityComparer<TValue>.Default.Equals(x.Value, list[i])).Select(x => x.Key).ToArray();
				foreach (var key in removeKeys)
					self.Remove(key);
			}
			return self;
		}

		/// <summary>
		/// 複数指定したバリューを存在したら削除します
		/// </summary>
		public static Dictionary<TKey, TValue> RemoveByValue<TKey, TValue>( this Dictionary<TKey, TValue> self, params TValue[] list )
		{
			for(int i = 0; i < list.Length; i++)
			{
				var removeKeys = self.Where(x => EqualityComparer<TValue>.Default.Equals(x.Value, list[i])).Select(x => x.Key).ToArray();
				foreach (var key in removeKeys)
					self.Remove(key);
			}
			return self;
		}
	}
}