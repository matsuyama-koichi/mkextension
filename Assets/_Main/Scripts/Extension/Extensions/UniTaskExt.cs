using System;
using UnityEngine;
using System.Threading;
using Cysharp.Threading.Tasks;

namespace Main
{
    public static class UniTaskExt
    {
        public static UniTask DelaySecondsTask(this GameObject self, float seconds)
        {
            var ct = self.GetCancellationTokenOnDestroy();
            return UniTask.Delay(System.TimeSpan.FromSeconds(seconds), cancellationToken: ct);
        }

        public static UniTask DelaySecondsTask(this GameObject self, float seconds, CancellationToken ct)
        {
            return UniTask.Delay(System.TimeSpan.FromSeconds(seconds), cancellationToken: ct);
        }

        public static UniTask DelaySecondsTask(this Component self, float seconds)
        {
            var ct = self.gameObject.GetCancellationTokenOnDestroy();
            return UniTask.Delay(System.TimeSpan.FromSeconds(seconds), cancellationToken: ct);
        }

        public static UniTask DelaySecondsTask(this Component self, float seconds, CancellationToken ct)
        {
            return UniTask.Delay(System.TimeSpan.FromSeconds(seconds), cancellationToken: ct);
        }
    }
}
