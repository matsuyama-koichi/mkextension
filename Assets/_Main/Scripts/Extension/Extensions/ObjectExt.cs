﻿namespace Main
{
	/// <summary>
	/// object 型の拡張メソッドを管理するクラス
	/// </summary>
	public static class ObjectExt
	{
		/// <summary>
		/// 数値を 3 桁区切りの文字列に変換します
		/// </summary>
		public static string FormatWithComma( this object self )
		{
			return string.Format( "{0:#,##0}", self );
		}

		/// <summary>
        /// System.Object 型を任意の型に変換して、型が違う場合defaultを返す
        /// </summary>
        public static T ToValue<T>( this object self, T def = default( T ) )
        {
			if ( self == null ) return def;
            if ( self.GetType() == typeof( T ) )
            {
                return ( T )self;
            }
            return def;
        }
	}
}