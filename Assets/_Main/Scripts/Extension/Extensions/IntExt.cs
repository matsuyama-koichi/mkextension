﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Main
{
	/// <summary>
	/// int 型の拡張メソッドを管理するクラス
	/// </summary>
	public static class IntExt
	{
		/// <summary>
		/// 指定された回数分処理を繰り返します
		/// </summary>
		public static int ForEach( this int self, Action action )
		{
			for ( int i = 0; i < self; i++ )
			{
				action();
			}
			return self;
		}

		/// <summary>
		/// 指定された回数分処理を繰り返します
		/// </summary>
		public static int ForEach( this int self, Action<int> action )
		{
			for ( int i = 0; i < self; i++ )
			{
				action( i );
			}
			return self;
		}

		/// <summary>
		/// 指定された回数分処理を逆順に繰り返します
		/// </summary>
		public static int ForEachReverse( this int self, Action<int> action )
		{
			for ( int i = self - 1; 0 <= i; i-- )
			{
				action( i );
			}
			return self;
		}

		/// <summary>
		/// 通貨記号と、必要に応じて桁区切り記号が入った文字列を返します
		/// </summary>
		public static string DigitSeparator( this int self, int numberOfDigits = 0 )
		{
			return self.ToString( "N" + numberOfDigits.ToString() );
		}

		/// <summary>
		/// <para>数値を指定された桁数で0埋めした文字列を返します</para>
		/// <para>123.ZeroFill( 4 ) → 01234</para>
		/// <para>123.ZeroFill( 8 ) → 000001234</para>
		/// </summary>
		public static string ZeroFill( this int self, int numberOfDigits )
		{
			return self.ToString( "D" + numberOfDigits.ToString() );
		}

		/// <summary>
		/// <para>数値に指定された桁数の固定小数点数を付加した文字列を返します</para>
		/// <para>123.FixedPoint(2) → 123.00</para>
		/// <para>123.FixedPoint(4) → 123.0000</para>
		/// </summary>
		public static string FixedPoint( this int self, int numberOfDigits )
		{
			return self.ToString( "F" + numberOfDigits.ToString() );
		}

		/// <summary>
		/// 数値を加算して、範囲を超えた分は 0 からの値として処理して返します
		/// </summary>
		public static int Repeat( this int self, int value, int max )
		{
			if ( max == 0 ) return self;
			return ( self + value + max ) % max;
		}

		/// <summary>
		/// 偶数かどうかを返します
		/// </summary>
		public static bool IsEven( this int self )
		{
			return self % 2 == 0;
		}

		/// <summary>
		/// 奇数かどうかを返します
		/// </summary>
		public static bool IsOdd( this int self )
		{
			return self % 2 == 1;
		}

		/// <summary>
		/// Float型に変換して返す
		/// </summary>
		public static float ToFloat( this int self )
		{
			return (float) self;
		}

		/// <summary>
		/// n 乗して返す
		/// </summary>
		public static int Pow( this int self, int n )
		{
			return (int) Mathf.Pow( self, n );
		}

		/// <summary>
		/// n 乗して返す
		/// </summary>
		public static float Pow( this int self, float n )
		{
			return Mathf.Pow( self, n );
		}

       	/// <summary>
		/// 引数を比較して大きい方を返す
		/// </summary>
		public static int Max( this int self, int max )
		{
			return Mathf.Max( self, max );
		}

       	/// <summary>
		/// 引数を比較して小さい方を返す
		/// </summary>
		public static int Min( this int self, int min )
		{
			return Mathf.Min( self, min );
		}

       	/// <summary>
		/// 引数を最小、最大の範囲内で返す
		/// </summary>
		public static int Clamp( this int self, int min, int max )
		{
			return Mathf.Clamp( self, min, max) ;
		}

		/// <summary>
		/// 引数の絶対値を返す
		/// </summary>
		public static int Abs( this int self )
		{
			return Mathf.Abs( self );
		}

		/// <summary>
		/// 引数の平方根を返す
		/// </summary>
		public static float Sqrt( this int self )
		{
			return Mathf.Sqrt( self );
		}

		/// <summary>
		/// 最小値と最大値を任意の比率の値を返す
		/// </summary>
		public static float Lerp( this float self, int min, int max )
		{
			return (int) Mathf.Lerp( min, max, self );
		}

		/// <summary>
		/// 最小値と最大値を任意の比率の値を返す（制限無し）
		/// </summary>
		public static int LerpUnclamped( this int self, int min, int max )
		{
			return (int) Mathf.LerpUnclamped( min, max, self );
		}

		/// <summary>
		/// 最小値と最大値の間での比率を返す
		/// </summary>
		public static float InverseLerp( this int self, int min, int max )
		{
			return Mathf.InverseLerp( min, max, self );
		}

		/// <summary>
		/// min から max の間の浮動小数点数をランダムにして加算する
		/// </summary>
		public static int RandomRange( this int self, int min, int max )
		{
			return self + RandomUtils.Range( min, max );
		}

		/// <summary>
		/// 任意の座標に変換して返す
		/// </summary>
		public static Vector2 ToVec2( this int self )
		{
			return new Vector2( self, self );
		}

		/// <summary>
		/// 任意の座標に変換して返す
		/// </summary>
		public static Vector2Int ToVec2Int( this int self )
        {
			return new Vector2Int( self, self );
        }

		/// <summary>
		/// 任意の座標に変換して返す
		/// </summary>
		public static Vector3 ToVec3( this int self )
		{
			return new Vector3( self, self, self );
		}

		/// <summary>
		/// 任意の座標に変換して返す
		/// </summary>
		public static Vector3Int ToVec3Int( this int self )
		{
			return new Vector3Int( self, self, self );
		}
	}
}