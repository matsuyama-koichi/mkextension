﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.EventSystems;

namespace Main
{
    public static class SpriteRendererExt
    {
        public static SpriteRenderer SetSprite( this SpriteRenderer self, Sprite sprite )
        {
            self.sprite = sprite;
            return self;
        }

        public static SpriteRenderer SetMaterial( this SpriteRenderer self, Material material )
        {
            self.material = material;
            return self;
        }

        public static SpriteRenderer SetColor( this SpriteRenderer self, Color color )
        {
            self.color = color;
            return self;
        }

        public static SpriteRenderer SetColor( this SpriteRenderer self, float r, float g, float b, float? a = null )
        {
            self.color = new Color
            (
                r, 
                g, 
                b, 
                !a.HasValue ? self.color.a : a.Value
            );
            return self;
        }

        public static SpriteRenderer SetColorR( this SpriteRenderer self, float r )
        {
            self.color = new Color
            (
                r,
                self.color.g,
                self.color.b,
                self.color.a
            );
            return self;
        }

        public static SpriteRenderer SetColorG( this SpriteRenderer self, float g )
        {
            self.color = new Color
            (
                self.color.r,
                g,
                self.color.b,
                self.color.a
            );
            return self;
        }

        public static SpriteRenderer SetColorB( this SpriteRenderer self, float b )
        {
            self.color = new Color
            (
                self.color.r,
                self.color.g,
                b,
                self.color.a
            );
            return self;
        }

        public static SpriteRenderer SetColorA( this SpriteRenderer self, float a )
        {
            self.color = new Color
            (
                self.color.r,
                self.color.g,
                self.color.b,
                a
            );
            return self;
        }

        public static SpriteRenderer SetSize( this SpriteRenderer self, float x, float y )
        {
            self.size = new Vector2( x, y );
            return self;
        }

        public static SpriteRenderer SetSize( this SpriteRenderer self, Vector2 vec )
        {
            self.size = vec;
            return self;
        }

        public static SpriteRenderer SetSizeX( this SpriteRenderer self, float x )
        {
            self.size = self.size.SetX( x );
            return self;
        }

        public static SpriteRenderer SetSizeY( this SpriteRenderer self, float y )
        {
            self.size = self.size.SetY( y );
            return self;
        }

        public static SpriteRenderer AddSize( this SpriteRenderer self, float x, float y )
        {
            self.size += new Vector2( x, y );
            return self;
        }

        public static SpriteRenderer AddSize( this SpriteRenderer self, Vector2 vec )
        {
            self.size += vec;
            return self;
        }

        public static SpriteRenderer AddSizeX( this SpriteRenderer self, float x )
        {
            self.size = self.size.AddX( x );
            return self;
        }

        public static SpriteRenderer AddSizeY(this SpriteRenderer self, float y)
        {
            self.size = self.size.AddY( y );
            return self;
        }

        public static SpriteRenderer SetMode( this SpriteRenderer self, SpriteDrawMode mode )
        {
            self.drawMode = mode;
            return self;
        }

        public static SpriteRenderer SetOrder( this SpriteRenderer self, int index )
        {
            self.sortingOrder = index;
            return self;
        }

        public static SpriteRenderer SetLayer( this SpriteRenderer self, string name )
        {
            self.sortingLayerName = name;
            return self;
        }

        public static SpriteRenderer SetMask( this SpriteRenderer self, SpriteMaskInteraction mask = default( SpriteMaskInteraction ) )
        {
            self.maskInteraction = mask;
            return self;
        }

        public static SpriteRenderer SetSortPoint( this SpriteRenderer self, SpriteSortPoint sortPoint = default( SpriteSortPoint ) )
        {
            self.spriteSortPoint = sortPoint;
            return self;
        }

        public static SpriteRenderer SetFlipX( this SpriteRenderer self, bool flag )
        {
            self.flipX = flag;
            return self;
        }

        public static SpriteRenderer SetFlipY( this SpriteRenderer self, bool flag )
        {
            self.flipY = flag;
            return self;
        }

        public static SpriteRenderer SetFlip( this SpriteRenderer self, bool x, bool y )
        {
            self.flipX = x;
            self.flipY = y;
            return self;
        }
    }
}

