﻿using System;
using System.Linq;
using UnityEngine;

namespace Main
{
	/// <summary>
	/// Enum 型の拡張メソッドを管理するクラス
	/// </summary>
	public static class EnumExt
	{
		/// <summary>
		/// 現在のインスタンスで 1 つ以上のビットフィールドが設定されているかどうかを判断します
		/// </summary>
		public static bool HasFlag( this Enum self, Enum flag )
		{
			if ( self.GetType() != flag.GetType() ) return false;

			var selfValue = Convert.ToUInt64( self );
			var flagValue = Convert.ToUInt64( flag );

			return ( selfValue & flagValue ) == flagValue;
		}

		/// <summary>
		/// int型で返します
		/// </summary>
		public static int ToInt<T>( this T self ) where T : Enum
		{
			return Convert.ToInt32( self );
		}

		/// <summary>
		/// 前の値に進みます。最小値を超える場合は止まります
		/// </summary>
		public static T Prev<T>( this T self, int? min = null ) where T : Enum
		{
			var orderLevel = EnumUtils.GetValues<T>().OrderBy(x => x.ToInt()).ToList();
        	var minValue = !min.HasValue ? orderLevel.Min(x => x.ToInt()) : min.Value;
			return self.ToInt() > minValue ? orderLevel.FindLast(x => self.ToInt() > x.ToInt()) : EnumUtils.ToObject<T>( minValue );
		}

		/// <summary>
		/// 前の値に進みます。指定した列挙子を超える場合は止まります
		/// </summary>
		public static T Prev<T>( this T self, T min ) where T : Enum
		{
			return self.Prev<T>( min.ToInt() );
		}

		/// <summary>
		/// 前の値に進みます。最小値を超える場合は最大値に戻ります
		/// </summary>
		public static T PrevLoop<T>( this T self, int? min = null, int? max = null ) where T : Enum
		{
			var orderLevel = EnumUtils.GetValues<T>().OrderBy(x => x.ToInt()).ToList();
			var minValue = !min.HasValue ? orderLevel.Min(x => x.ToInt()) : min.Value;
			var maxValue = !max.HasValue ? orderLevel.Max(x => x.ToInt()) : max.Value;
			return self.ToInt() > minValue ? orderLevel.FindLast(x => self.ToInt() > x.ToInt()) : EnumUtils.ToObject<T>( maxValue );
		}

		/// <summary>
		/// 前の値に進みます。指定した列挙子(min)を超える場合は最大値に戻ります
		/// </summary>
		public static T PrevLoop<T>( this T self, T min ) where T : Enum
		{
			return self.PrevLoop( min.ToInt() );
		}

		/// <summary>
		/// 前の値に進みます。指定した列挙子(min)を超える場合は指定した列挙子(max)に戻ります
		/// </summary>
		public static T PrevLoop<T>( this T self, T min, T max ) where T : Enum
		{
			return self.PrevLoop( min.ToInt(), max.ToInt() );
		}

		/// <summary>
		/// 次の値に進みます。最大値を超える場合は止まります
		/// </summary>
		public static T Next<T>( this T self, int? max = null ) where T : Enum
		{
			var orderLevel = EnumUtils.GetValues<T>().OrderBy(x => x.ToInt()).ToList();
			var maxValue = !max.HasValue ? orderLevel.Max(x => x.ToInt()) : max.Value;
			return self.ToInt() < maxValue ? orderLevel.Find(x => self.ToInt() < x.ToInt()) : EnumUtils.ToObject<T>( maxValue );
		}

		/// <summary>
		/// 次の値に進みます。指定した列挙子(max)を超える場合は止まります
		/// </summary>
		public static T Next<T>( this T self, T max ) where T : Enum
		{
			return self.Next( max.ToInt() );
		}

		/// <summary>
		/// 次の値に進みます。最大値を超える場合は初期値に戻ります
		/// </summary>
		public static T NextLoop<T>( this T self, int? max = null, int? min = null ) where T : Enum
		{
			var orderLevel = EnumUtils.GetValues<T>().OrderBy(x => x.ToInt()).ToList();
			var minValue = !min.HasValue ? orderLevel.Min(x => x.ToInt()) : min.Value;
			var maxValue = !max.HasValue ? orderLevel.Max(x => x.ToInt()) : max.Value;
			return self.ToInt() < maxValue ? orderLevel.Find(x => self.ToInt() < x.ToInt()) : EnumUtils.ToObject<T>( minValue );
		}

		/// <summary>
		/// 次の値に進みます。指定した列挙子(max)を超える場合は初期値に戻ります
		/// </summary>
		public static T NextLoop<T>( this T self, T max ) where T : Enum
		{
			return self.NextLoop( max.ToInt() );
		}

		/// <summary>
		/// 次の値に進みます。指定した列挙子(max)を超える場合は指定した列挙子(min)に戻ります
		/// </summary>
		public static T NextLoop<T>( this T self, T min, T max ) where T : Enum
		{
			return self.NextLoop( max.ToInt(), min.ToInt() );
		}

		/// <summary>
		/// 列挙子が最小値なのかを返す
		/// </summary>
		public static bool IsMinValue<T>( this T self ) where T : Enum
		{
			return EnumUtils.MinValue<T>().Equals(self);
		}

		/// <summary>
		/// 列挙子が最大値なのかを返す
		/// </summary>
		public static bool IsMaxValue<T>( this T self ) where T : Enum
		{
			return EnumUtils.MaxValue<T>().Equals(self);
		}
	}
}