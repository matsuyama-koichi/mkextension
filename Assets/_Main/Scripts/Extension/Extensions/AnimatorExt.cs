﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Main
{
    public static class AnimatorExt
    {
        public static Animator SetTrigger<T>( this Animator self, T flag ) where T : Enum
        {
            self.SetTrigger(flag.ToString());
            return self;
        }

        public static Animator SetBool<T>( this Animator self, T flag, bool value ) where T : Enum
        {
            self.SetBool(flag.ToString(), value);
            return self;
        }

        public static Animator SetFloat<T>( this Animator self, T flag, float value ) where T : Enum
        {
            self.SetFloat(flag.ToString(), value);
            return self;
        }

        public static Animator Play<T>( this Animator self, T flag, int layer, float normalizedTime ) where T : Enum
        {
            self.Play(flag.ToString(), layer, normalizedTime);
            return self;
        }

        public static Animator PlayUpdate( this Animator self, string parameterName, float normalizedTime )
        {
            self.Play(parameterName);
            self.Update(normalizedTime);
            return self;
        }

        public static Animator AllResetTrigger( this Animator self, params string[] excludeNames )
        {
            foreach ( var param in self.parameters ) 
            {
                if ( excludeNames.Contains( param.name ) ) continue;
                if ( param.type == AnimatorControllerParameterType.Trigger )
                {
                    self.ResetTrigger( param.name );
                }
            }
            return self;
        }

        public static Animator AllResetBool( this Animator self, params string[] excludeNames )
        {
            foreach ( var param in self.parameters ) 
            {
                if ( excludeNames.Contains( param.name ) ) continue;
                if ( param.type == AnimatorControllerParameterType.Bool )
                {
                    self.SetBool( param.name, param.defaultBool );
                }
            }
            return self;
        }

        public static Animator AllResetFloat( this Animator self, params string[] excludeNames )
        {
            foreach ( var param in self.parameters ) 
            {
                if ( excludeNames.Contains( param.name ) ) continue;
                if ( param.type == AnimatorControllerParameterType.Float )
                {
                    self.SetFloat( param.name, param.defaultFloat );
                }
            }
            return self;
        }

        public static Animator AllResetInteger( this Animator self, params string[] excludeNames )
        {
            foreach ( var param in self.parameters ) 
            {
                if ( excludeNames.Contains( param.name ) ) continue;
                if ( param.type == AnimatorControllerParameterType.Int )
                {
                    self.SetInteger( param.name, param.defaultInt );
                }
            }
            return self;
        }

        public static Animator AllReset( this Animator self, params string[] excludeNames )
        {            
            foreach ( var param in self.parameters ) 
            {
                if ( excludeNames.Contains( param.name ) ) continue;
                if ( param.type == AnimatorControllerParameterType.Trigger )
                {
                    self.ResetTrigger( param.name );
                }
                else if ( param.type == AnimatorControllerParameterType.Bool )
                {
                    self.SetBool( param.name, param.defaultBool );
                }
                else if ( param.type == AnimatorControllerParameterType.Float )
                {
                    self.SetFloat( param.name, param.defaultFloat );
                }
                else if ( param.type == AnimatorControllerParameterType.Int )
                {
                    self.SetInteger( param.name, param.defaultInt );
                }
            }
            return self;
        }

        public static float GetCurrentAnimatorStateInfoNormalizedTime( this Animator self, int layer = 0 )
        {
            var stateInfo = self.GetCurrentAnimatorStateInfo(layer);
            var normalizedTime = stateInfo.normalizedTime;
            var maxValue = Mathf.Ceil(normalizedTime);
            return Mathf.InverseLerp(maxValue - 1f, maxValue, normalizedTime);
        }
    }
}

