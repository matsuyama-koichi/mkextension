﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Main
{
	/// <summary>
	/// GameObject 型の拡張メソッドを管理するクラス
	/// </summary>
	public static class GameObjectExt
	{
		/// <summary>
		/// アクティブかどうかを設定します
		/// </summary>
		public static GameObject SetActiveIfNotNull( this GameObject self, bool isActive )
		{
			if ( self == null ) return self;
			self.SetActive( isActive );
			return self;
		}

		/// <summary>
		/// コンポーネントを取得します。コンポーネントがアタッチされていない場合は追加してから取得します
		/// </summary>
		public static T GetOrAddComponent<T>( this GameObject self ) where T : Component
		{
			var comp = self.GetComponent<T>();
			if ( comp == null )
				comp = self.AddComponent<T>();
			return comp;
		}

		/// <summary>
		/// コンポーネントを取得します。コンポーネントがアタッチされていない場合は追加してから取得します
		/// </summary>
		public static Component GetOrAddComponent( this GameObject self, Type type )
		{
			var comp = self.GetComponent(type);
			if ( comp == null )
				comp = self.AddComponent(type);
			return comp;
		}

		/// <summary>
		/// すべての子オブジェクトを返します
		/// </summary>
		public static GameObject[] GetChildren( this GameObject self, bool includeInactive = false )
		{
			return self
				.GetComponentsInChildren<Transform>( includeInactive )
				.Where( c => c != self.transform )
				.Select( c => c.gameObject )
				.ToArray()
			;
		}

		/// <summary>
		/// 自分自身を含まない GetComponentInChildren 関数を実行します
		/// </summary>
		public static T GetComponentInChildrenWithoutSelf<T>( this GameObject self ) where T : Component
		{
			return self.GetComponentsInChildrenWithoutSelf<T>().FirstOrDefault();
		}

		/// <summary>
		/// 自分自身を含まない GetComponentsInChildren 関数を実行します
		/// </summary>
		public static T[] GetComponentsInChildrenWithoutSelf<T>( this GameObject self ) where T : Component
		{
			return self.GetComponentsInChildren<T>().Where( c => self != c.gameObject ).ToArray();
		}

		/// <summary>
		/// 自分自身を含まない GetComponentsInChildren 関数を実行します
		/// </summary>
		public static T[] GetComponentsInChildrenWithoutSelf<T>( this GameObject self, bool includeInactive ) where T : Component
		{
			return self.GetComponentsInChildren<T>( includeInactive ).Where( c => self != c.gameObject ).ToArray();
		}

		/// <summary>
		/// コンポーネントを削除します
		/// </summary>
		public static GameObject RemoveComponent<T>( this GameObject self ) where T : Component
		{
			GameObject.Destroy( self.GetComponent<T>() );
			return self;
		}

		/// <summary>
		/// コンポーネントをすべて削除します
		/// </summary>
		public static GameObject RemoveComponents<T>( this GameObject self ) where T : Component
		{
			foreach ( var component in self.GetComponents<T>() )
			{
				GameObject.Destroy( component );
			}
			return self;
		}

		/// <summary>
		/// コンポーネントを即座に削除します
		/// </summary>
		public static GameObject RemoveComponentImmediate<T>( this GameObject self ) where T : Component
		{
			GameObject.DestroyImmediate( self.GetComponent<T>() );
			return self;
		}

		/// <summary>
		/// コンポーネントをすべて即座に削除します
		/// </summary>
		public static GameObject RemoveComponentsImmediate<T>( this GameObject self ) where T : Component
		{
			foreach ( var component in self.GetComponents<T>() )
			{
				GameObject.DestroyImmediate( component );
			}
			return self;
		}

		/// <summary>
		/// 指定されたコンポーネントを持っているかどうかを返します
		/// </summary>
		public static bool HasComponent<T>( this GameObject self ) where T : Component
		{
			return self.GetComponent<T>() != null;
		}

		/// <summary>
		/// 子オブジェクトを名前で検索します
		/// </summary>
		public static Transform Find( this GameObject self, string name )
 		{
			return self.transform.Find( name );
		}

		/// <summary>
		/// 子オブジェクトを名前で検索して GameObject 型で取得します
		/// </summary>
		public static GameObject FindGameObject( this GameObject self, string name )
		{
			var result = self.transform.Find( name );
			return result != null ? result.gameObject : null;
		}

		/// <summary>
		/// 子オブジェクトを検索してコンポーネントを取得
		/// </summary>
		public static T FindGetComponent<T>( this GameObject self, string name ) where T : Component
		{
			var result = self.transform.Find( name );
			return result != null ? result.GetComponent<T>() : null;
		}

		/// <summary>
		/// 深い階層まで子オブジェクトを名前で検索して GameObject 型で取得します
		/// </summary>
		public static GameObject FindDeep( this GameObject self, string name, bool includeInactive = false )
		{
			var children = self.GetComponentsInChildren<Transform>( includeInactive );
			foreach ( var transform in children )
			{
				if ( transform.name == name )
				{
					return transform.gameObject;
				}
			}
			return null;
		}

		/// <summary>
		/// 位置を(0, 0, 0)にリセットします
		/// </summary>
		public static GameObject ResetPosition( this GameObject self )
		{
			self.transform.position = Vector3.zero;
			return self;
		}

		/// <summary>
		/// 位置を返します
		/// </summary>
		public static Vector3 GetPosition( this GameObject self )
		{
			return self.transform.position;
		}

		/// <summary>
		/// X 座標を返します
		/// </summary>
		public static float GetPositionX( this GameObject self )
		{
			return self.transform.position.x;
		}

		/// <summary>
		/// Y 座標を返します
		/// </summary>
		public static float GetPositionY( this GameObject self )
		{
			return self.transform.position.y;
		}

		/// <summary>
		/// Z 座標を返します
		/// </summary>
		public static float GetPositionZ( this GameObject self )
		{
			return self.transform.position.z;
		}

		/// <summary>
		/// X 座標を設定します
		/// </summary>
		public static GameObject SetPositionX( this GameObject self, float x )
		{
			self.transform.position = new Vector3
			(
				x,
				self.transform.position.y,
				self.transform.position.z
			);
			return self;
		}

		/// <summary>
		/// Y 座標を設定します
		/// </summary>
		public static GameObject SetPositionY( this GameObject self, float y )
		{
			self.transform.position = new Vector3
			(
				self.transform.position.x,
				y,
				self.transform.position.z
			);
			return self;
		}

		/// <summary>
		/// Z 座標を設定します
		/// </summary>
		public static GameObject SetPositionZ( this GameObject self, float z )
		{
			self.transform.position = new Vector3
			(
				self.transform.position.x,
				self.transform.position.y,
				z
			);
			return self;
		}

		/// <summary>
		/// Vector3 型で位置を設定します
		/// </summary>
		public static GameObject SetPosition( this GameObject self, Vector3 v )
		{
			self.transform.position = v;
			return self;
		}

		/// <summary>
		/// Vector2 型で位置を設定します
		/// </summary>
		public static GameObject SetPosition( this GameObject self, Vector2 v )
		{
			self.transform.position = new Vector3
			(
				v.x,
				v.y,
				self.transform.position.z
			);
			return self;
		}

		/// <summary>
		/// 位置を設定します
		/// </summary>
		public static GameObject SetPosition( this GameObject self, float x, float y, float? z = null )
		{
			self.transform.position = new Vector3
			(
				x,
				y,
				!z.HasValue ? self.transform.position.z : z.Value
			);
			return self;
		}

		/// <summary>
		/// X 座標に加算します
		/// </summary>
		public static GameObject AddPositionX( this GameObject self, float x )
		{
			self.transform.position = new Vector3
			(
				self.transform.position.x + x,
				self.transform.position.y,
				self.transform.position.z
			);
			return self;
		}

		/// <summary>
		/// Y 座標に加算します
		/// </summary>
		public static GameObject AddPositionY( this GameObject self, float y )
		{
			self.transform.position = new Vector3
			(
				self.transform.position.x,
				self.transform.position.y + y,
				self.transform.position.z
			);
			return self;
		}

		/// <summary>
		/// Z 座標に加算します
		/// </summary>
		public static GameObject AddPositionZ( this GameObject self, float z )
		{
			self.transform.position = new Vector3
			(
				self.transform.position.x,
				self.transform.position.y,
				self.transform.position.z + z
			);
			return self;
		}

		/// <summary>
		/// Vector3 型で位置を加算します
		/// </summary>
		public static GameObject AddPosition( this GameObject self, Vector3 v )
		{
			self.transform.position += v;
			return self;
		}

		/// <summary>
		/// Vector2 型で位置を加算します
		/// </summary>
		public static GameObject AddPosition( this GameObject self, Vector2 v )
		{
			self.transform.position = new Vector3
			(
				self.transform.position.x + v.x,
				self.transform.position.y + v.y,
				self.transform.position.z
			);
			return self;
		}

		/// <summary>
		/// 位置を加算します
		/// </summary>
		public static GameObject AddPosition( this GameObject self, float x, float y, float? z = null )
		{
			self.transform.position = new Vector3
			(
				self.transform.position.x + x,
				self.transform.position.y + y,
				!z.HasValue ? self.transform.position.z : self.transform.position.z + z.Value
			);
			return self;
		}

		/// <summary>
		/// ローカル座標を(0, 0, 0)にリセットします
		/// </summary>
		public static GameObject ResetLocalPosition( this GameObject self )
		{
			self.transform.localPosition = Vector3.zero;
			return self;
		}

		/// <summary>
		/// ローカル座標を返します
		/// </summary>
		public static Vector3 GetLocalPosition( this GameObject self )
		{
			return self.transform.localPosition;
		}

		/// <summary>
		/// ローカル座標系の X 座標を返します
		/// </summary>
		public static float GetLocalPositionX( this GameObject self )
		{
			return self.transform.localPosition.x;
		}

		/// <summary>
		/// ローカル座標系の Y 座標を返します
		/// </summary>
		public static float GetLocalPositionY( this GameObject self )
		{
			return self.transform.localPosition.y;
		}

		/// <summary>
		/// ローカル座標系の Z 座標を返します
		/// </summary>
		public static float GetLocalPositionZ( this GameObject self )
		{
			return self.transform.localPosition.z;
		}

		/// <summary>
		/// ローカル座標系のX座標を設定します
		/// </summary>
		public static GameObject SetLocalPositionX( this GameObject self, float x )
		{
			self.transform.localPosition = new Vector3
			(
				x,
				self.transform.localPosition.y,
				self.transform.localPosition.z
			);
			return self;
		}

		/// <summary>
		/// ローカル座標系のY座標を設定します
		/// </summary>
		public static GameObject SetLocalPositionY( this GameObject self, float y )
		{
			self.transform.localPosition = new Vector3
			(
				self.transform.localPosition.x,
				y,
				self.transform.localPosition.z
			);
			return self;
		}

		/// <summary>
		/// ローカル座標系のZ座標を設定します
		/// </summary>
		public static GameObject SetLocalPositionZ( this GameObject self, float z )
		{
			self.transform.localPosition = new Vector3
			(
				self.transform.localPosition.x,
				self.transform.localPosition.y,
				z
			);
			return self;
		}

		/// <summary>
		/// Vector3 型でローカル座標を設定します
		/// </summary>
		public static GameObject SetLocalPosition( this GameObject self, Vector3 v )
		{
			self.transform.localPosition = v;
			return self;
		}

		/// <summary>
		/// Vector2 型でローカル座標を設定します
		/// </summary>
		public static GameObject SetLocalPosition( this GameObject self, Vector2 v )
		{
			self.transform.localPosition = new Vector3
			(
				v.x,
				v.y,
				self.transform.localPosition.z
			);
			return self;
		}

		/// <summary>
		/// ローカル座標を設定します
		/// </summary>
		public static GameObject SetLocalPosition( this GameObject self, float x, float y, float? z = null )
		{
			self.transform.localPosition = new Vector3
			(
				x,
				y,
				!z.HasValue ? self.transform.localPosition.z : z.Value
			);
			return self;
		}

		/// <summary>
		/// ローカルのX座標に加算します
		/// </summary>
		public static GameObject AddLocalPositionX( this GameObject self, float x )
		{
			self.transform.localPosition = new Vector3
			(
				self.transform.localPosition.x + x,
				self.transform.localPosition.y,
				self.transform.localPosition.z
			);
			return self;
		}

		/// <summary>
		/// ローカルのY座標に加算します
		/// </summary>
		public static GameObject AddLocalPositionY( this GameObject self, float y )
		{
			self.transform.localPosition = new Vector3
			(
				self.transform.localPosition.x,
				self.transform.localPosition.y + y,
				self.transform.localPosition.z
			);
			return self;
		}

		/// <summary>
		/// ローカルのZ座標に加算します
		/// </summary>
		public static GameObject AddLocalPositionZ( this GameObject self, float z )
		{
			self.transform.localPosition = new Vector3
			(
				self.transform.localPosition.x,
				self.transform.localPosition.y,
				self.transform.localPosition.z + z
			);
			return self;
		}

		/// <summary>
		/// Vector3 型でローカル座標を加算します
		/// </summary>
		public static GameObject AddLocalPosition( this GameObject self, Vector3 v )
		{
			self.transform.localPosition += v;
			return self;
		}

		/// <summary>
		/// Vector2 型でローカル座標を加算します
		/// </summary>
		public static GameObject AddLocalPosition( this GameObject self, Vector2 v )
		{
			self.transform.localPosition = new Vector3
			(
				self.transform.localPosition.x + v.x,
				self.transform.localPosition.y + v.y,
				self.transform.localPosition.z
			);
			return self;
		}

		/// <summary>
		/// ローカル座標を加算します
		/// </summary>
		public static GameObject AddLocalPosition( this GameObject self, float x, float y, float? z = null )
		{
			self.transform.localPosition = new Vector3
			(
				self.transform.localPosition.x + x,
				self.transform.localPosition.y + y,
				!z.HasValue ? self.transform.localPosition.z : self.transform.localPosition.z + z.Value
			);
			return self;
		}

		/// <summary>
		/// ローカル座標系のスケーリング値を(1, 1, 1)にリセットします
		/// </summary>
		public static GameObject ResetLocalScale( this GameObject self )
		{
			self.transform.localScale = Vector3.one;
			return self;
		}

		/// <summary>
		/// ローカル座標系のスケーリング値を返します
		/// </summary>
		public static Vector3 GetLocalScale( this GameObject self )
		{
			return self.transform.localScale;
		}

		/// <summary>
		/// X 軸方向のローカル座標系のスケーリング値を返します
		/// </summary>
		public static float GetLocalScaleX( this GameObject self )
		{
			return self.transform.localScale.x;
		}

		/// <summary>
		/// Y 軸方向のローカル座標系のスケーリング値を返します
		/// </summary>
		public static float GetLocalScaleY( this GameObject self )
		{
			return self.transform.localScale.y;
		}

		/// <summary>
		/// Z 軸方向のローカル座標系のスケーリング値を返します
		/// </summary>
		public static float GetLocalScaleZ( this GameObject self )
		{
			return self.transform.localScale.z;
		}

		/// <summary>
		/// ワールド座標系のスケーリング値を返します
		/// </summary>
		public static Vector3 GetLossyScale( this GameObject self )
		{
			return self.transform.lossyScale;
		}

		/// <summary>
		/// X 軸方向のワールド座標系のスケーリング値を返します
		/// </summary>
		public static float GetLossyScaleX( this GameObject self )
		{
			return self.transform.lossyScale.x;
		}

		/// <summary>
		/// Y 軸方向のワールド座標系のスケーリング値を返します
		/// </summary>
		public static float GetLossyScaleY( this GameObject self )
		{
			return self.transform.lossyScale.y;
		}

		/// <summary>
		/// Z 軸方向のワールド座標系のスケーリング値を返します
		/// </summary>
		public static float GetLossyScaleZ( this GameObject self )
		{
			return self.transform.lossyScale.z;
		}

		/// <summary>
		/// XYZの平均値を返します
		/// </summary>
		public static float GetAveScale( this GameObject self )
		{
			var scale = self.transform.localScale;
			return (scale.x + scale.y + scale.z) / 3f;
		}

		/// <summary>
		/// X 軸方向のローカル座標系のスケーリング値を設定します
		/// </summary>
		public static GameObject SetLocalScaleX( this GameObject self, float x )
		{
			self.transform.localScale = new Vector3
			(
				x,
				self.transform.localScale.y,
				self.transform.localScale.z
			);
			return self;
		}

		/// <summary>
		/// Y 軸方向のローカル座標系のスケーリング値を設定します
		/// </summary>
		public static GameObject SetLocalScaleY( this GameObject self, float y )
		{
			self.transform.localScale = new Vector3
			(
				self.transform.localScale.x,
				y,
				self.transform.localScale.z
			);
			return self;
		}

		/// <summary>
		/// Z 軸方向のローカル座標系のスケーリング値を設定します
		/// </summary>
		public static GameObject SetLocalScaleZ( this GameObject self, float z )
		{
			self.transform.localScale = new Vector3
			(
				self.transform.localScale.x,
				self.transform.localScale.y,
				z
			);
			return self;
		}

		/// <summary>
		/// Vector3 型でローカル座標系のスケーリング値を設定します
		/// </summary>
		public static GameObject SetLocalScale( this GameObject self, Vector3 v )
		{
			self.transform.localScale = v;
			return self;
		}

		/// <summary>
		/// Vector2 型でローカル座標系のスケーリング値を設定します
		/// </summary>
		public static GameObject SetLocalScale( this GameObject self, Vector2 v )
		{
			self.transform.localScale = new Vector3
			(
				v.x,
				v.y,
				self.transform.localScale.z
			);
			return self;
		}

		/// <summary>
		/// ローカル座標系のスケーリング値を設定します
		/// </summary>
		public static GameObject SetLocalScale( this GameObject self, float x, float y, float? z = null )
		{
			self.transform.localScale = new Vector3
			(
				x,
				y,
				!z.HasValue ? self.transform.localScale.z : z.Value
			);
			return self;
		}

		/// <summary>
		/// ローカル座標系のスケーリング値を設定します
		/// </summary>
		public static GameObject SetLocalScale( this GameObject self, float v )
		{
			self.transform.localScale = new Vector3
			(
				v,
				v,
				v
			);
			return self;
		}

		/// <summary>
		/// X 軸方向のローカル座標系のスケーリング値を加算します
		/// </summary>
		public static GameObject AddLocalScaleX( this GameObject self, float x )
		{
			self.transform.localScale = new Vector3
			(
				self.transform.localScale.x + x,
				self.transform.localScale.y,
				self.transform.localScale.z
			);
			return self;
		}

		/// <summary>
		/// Y 軸方向のローカル座標系のスケーリング値を加算します
		/// </summary>
		public static GameObject AddLocalScaleY( this GameObject self, float y )
		{
			self.transform.localScale = new Vector3
			(
				self.transform.localScale.x,
				self.transform.localScale.y + y,
				self.transform.localScale.z
			);
			return self;
		}

		/// <summary>
		/// Z 軸方向のローカル座標系のスケーリング値を加算します
		/// </summary>
		public static GameObject AddLocalScaleZ( this GameObject self, float z )
		{
			self.transform.localScale = new Vector3
			(
				self.transform.localScale.x,
				self.transform.localScale.y,
				self.transform.localScale.z + z
			);
			return self;
		}

		/// <summary>
		/// Vector3 型でローカル座標系のスケーリング値を加算します
		/// </summary>
		public static GameObject AddLocalScale( this GameObject self, Vector3 v )
		{
			self.transform.localScale += v;
			return self;
		}

		/// <summary>
		/// Vector2 型でローカル座標系のスケーリング値を加算します
		/// </summary>
		public static GameObject AddLocalScale( this GameObject self, Vector2 v )
		{
			self.transform.localScale = new Vector3
			(
				self.transform.localScale.x + v.x,
				self.transform.localScale.y + v.y,
				self.transform.localScale.z
			);
			return self;
		}

		/// <summary>
		/// ローカル座標系のスケーリング値を加算します
		/// </summary>
		public static GameObject AddLocalScale( this GameObject self, float x, float y, float? z = null )
		{
			self.transform.localScale = new Vector3
			(
				self.transform.localScale.x + x,
				self.transform.localScale.y + y,
				!z.HasValue ? self.transform.localScale.z : self.transform.localScale.z + z.Value
			);
			return self;
		}

		/// <summary>
		/// ローカル座標系のスケーリング値を加算します
		/// </summary>
		public static GameObject AddLocalScale( this GameObject self, float v )
		{
			self.transform.localScale = new Vector3
			(
				self.transform.localScale.x + v,
				self.transform.localScale.y + v,
				self.transform.localScale.z + v
			);
			return self;
		}

		/// <summary>
		/// 回転角を(0, 0, 0)にリセットします
		/// </summary>
		public static GameObject ResetEulerAngles( this GameObject self )
		{
			self.transform.eulerAngles = Vector3.zero;
			return self;
		}

		/// <summary>
		/// 回転角を返します
		/// </summary>
		public static Vector3 GetEulerAngles( this GameObject self )
		{
			return self.transform.eulerAngles;
		}

		/// <summary>
		/// X 軸方向の回転角を返します
		/// </summary>
		public static float GetEulerAngleX( this GameObject self )
		{
			return self.transform.eulerAngles.x;
		}

		/// <summary>
		/// Y 軸方向の回転角を返します
		/// </summary>
		public static float GetEulerAngleY( this GameObject self )
		{
			return self.transform.eulerAngles.y;
		}

		/// <summary>
		/// Z 軸方向の回転角を返します
		/// </summary>
		public static float GetEulerAngleZ( this GameObject self )
		{
			return self.transform.eulerAngles.z;
		}

		/// <summary>
		/// 回転角を設定します
		/// </summary>
		public static GameObject SetEulerAngles( this GameObject self, Vector3 v )
		{
			self.transform.eulerAngles = v;
			return self;
		}

		/// <summary>
		/// 回転角を設定します
		/// </summary>
		public static GameObject SetEulerAngles( this GameObject self, float x, float y, float z )
		{
			self.transform.eulerAngles = new Vector3(x, y, z);
			return self;
		}

		/// <summary>
		/// X 軸方向の回転角を設定します
		/// </summary>
		public static GameObject SetEulerAngleX( this GameObject self, float x )
		{
			self.transform.eulerAngles = new Vector3
			(
				x,
				self.transform.eulerAngles.y,
				self.transform.eulerAngles.z
			);
			return self;
		}

		/// <summary>
		/// Y 軸方向の回転角を設定します
		/// </summary>
		public static GameObject SetEulerAngleY( this GameObject self, float y )
		{
			self.transform.eulerAngles = new Vector3
			(
				self.transform.eulerAngles.x,
				y,
				self.transform.eulerAngles.z
			);
			return self;
		}

		/// <summary>
		/// Z 軸方向の回転角を設定します
		/// </summary>
		public static GameObject SetEulerAngleZ( this GameObject self, float z )
		{
			self.transform.eulerAngles = new Vector3
			(
				self.transform.eulerAngles.x,
				self.transform.eulerAngles.y,
				z
			);
			return self;
		}

		/// <summary>
		/// X 軸方向の回転角を加算します
		/// </summary>
		public static GameObject AddEulerAngleX( this GameObject self, float x )
		{
			self.transform.Rotate( x, 0, 0, Space.World );
			return self;
		}

		/// <summary>
		/// Y 軸方向の回転角を加算します
		/// </summary>
		public static GameObject AddEulerAngleY( this GameObject self, float y )
		{
			self.transform.Rotate( 0, y, 0, Space.World );
			return self;
		}

		/// <summary>
		/// Z 軸方向の回転角を加算します
		/// </summary>
		public static GameObject AddEulerAngleZ( this GameObject self, float z )
		{
			self.transform.Rotate( 0, 0, z, Space.World );
			return self;
		}

		/// <summary>
		/// ローカルの回転角を(0, 0, 0)にリセットします
		/// </summary>
		public static GameObject ResetLocalEulerAngles( this GameObject self )
		{
			self.transform.localEulerAngles = Vector3.zero;
			return self;
		}

		/// <summary>
		/// ローカルの回転角を返します
		/// </summary>
		public static Vector3 GetLocalEulerAngles( this GameObject self )
		{
			return self.transform.localEulerAngles;
		}

		/// <summary>
		/// ローカルの X 軸方向の回転角を返します
		/// </summary>
		public static float GetLocalEulerAngleX( this GameObject self )
		{
			return self.transform.localEulerAngles.x;
		}

		/// <summary>
		/// ローカルの Y 軸方向の回転角を返します
		/// </summary>
		public static float GetLocalEulerAngleY( this GameObject self )
		{
			return self.transform.localEulerAngles.y;
		}

		/// <summary>
		/// ローカルの Z 軸方向の回転角を返します
		/// </summary>
		public static float GetLocalEulerAngleZ( this GameObject self )
		{
			return self.transform.localEulerAngles.z;
		}

		/// <summary>
		/// ローカルの回転角を設定します
		/// </summary>
		public static GameObject SetLocalEulerAngle( this GameObject self, Vector3 v )
		{
			self.transform.localEulerAngles = v;
			return self;
		}

		/// <summary>
		/// ローカルの回転角を設定します
		/// </summary>
		public static GameObject SetLocalEulerAngle( this GameObject self, float x, float y, float z )
		{
			self.transform.localEulerAngles = new Vector3(x, y, z);
			return self;
		}

		/// <summary>
		/// ローカルの X 軸方向の回転角を設定します
		/// </summary>
		public static GameObject SetLocalEulerAngleX( this GameObject self, float x )
		{
			self.transform.localEulerAngles = new Vector3
			(
				x,
				self.transform.localEulerAngles.y,
				self.transform.localEulerAngles.z
			);
			return self;
		}

		/// <summary>
		/// ローカルの Y 軸方向の回転角を設定します
		/// </summary>
		public static GameObject SetLocalEulerAngleY( this GameObject self, float y )
		{
			self.transform.localEulerAngles = new Vector3
			(
				self.transform.localEulerAngles.x,
				y,
				self.transform.localEulerAngles.z
			);
			return self;
		}

		/// <summary>
		/// ローカルの Z 軸方向の回転角を設定します
		/// </summary>
		public static GameObject SetLocalEulerAngleZ( this GameObject self, float z )
		{
			self.transform.localEulerAngles = new Vector3
			(
				self.transform.localEulerAngles.x,
				self.transform.localEulerAngles.y,
				z
			);
			return self;
		}

		/// <summary>
		/// ローカルの X 軸方向の回転角を加算します
		/// </summary>
		public static GameObject AddLocalEulerAngleX( this GameObject self, float x )
		{
			self.transform.Rotate( x, 0, 0, Space.Self );
			return self;
		}

		/// <summary>
		/// ローカルの Y 軸方向の回転角を加算します
		/// </summary>
		public static GameObject AddLocalEulerAngleY( this GameObject self, float y )
		{
			self.transform.Rotate( 0, y, 0, Space.Self );
			return self;
		}

		/// <summary>
		/// ローカルの X 軸方向の回転角を加算します
		/// </summary>
		public static GameObject AddLocalEulerAngleZ( this GameObject self, float z )
		{
			self.transform.Rotate( 0, 0, z, Space.Self );
			return self;
		}

		/// <summary>
		/// 親オブジェクトが存在するかどうかを返します
		/// </summary>
		public static bool HasParent( this GameObject self )
		{
			return self.transform.parent != null;
		}

		/// <summary>
		/// 親オブジェクトを解除する
		/// </summary>
		public static GameObject ResetParent( this GameObject self )
		{
			self.transform.parent = null;
			return self;
		}

		/// <summary>
		/// 親オブジェクトを設定します
		/// </summary>
		public static GameObject SetParent( this GameObject self, Component parent, Vector3? vec = null )
		{
			self.transform.SetParent( parent != null ? parent.transform : null );
			if ( vec.HasValue )
				self.transform.localPosition = vec.Value;
			return self;
		}

		/// <summary>
		/// 親オブジェクトを設定します
		/// </summary>
		public static GameObject SetParent( this GameObject self, GameObject parent, Vector3? vec = null )
		{
			self.transform.SetParent( parent != null ? parent.transform : null );
			if ( vec.HasValue )
				self.transform.localPosition = vec.Value;
			return self;
		}

		/// <summary>
		/// 親オブジェクトを設定します
		/// </summary>
		public static GameObject SetParent( this GameObject self, Component parent, bool worldPositionStays )
		{
			self.transform.SetParent( parent != null ? parent.transform : null, worldPositionStays );
			return self;
		}

		/// <summary>
		/// 親オブジェクトを設定します
		/// </summary>
		public static GameObject SetParent( this GameObject self, GameObject parent, bool worldPositionStays )
		{
			self.transform.SetParent( parent != null ? parent.transform : null, worldPositionStays );
			return self;
		}

		/// <summary>
		/// ローカル座標を維持して親オブジェクトを設定します
		/// </summary>
		public static GameObject SafeSetParent( this GameObject self, Component parent )
		{
			return SafeSetParent( self, parent.gameObject );
		}

		/// <summary>
		/// ローカル座標を維持して親オブジェクトを設定します
		/// </summary>
		public static GameObject SafeSetParent( this GameObject self, GameObject parent )
		{
			var t = self.transform;
			var localPosition = t.localPosition;
			var localRotation = t.localRotation;
			var localScale = t.localScale;
			t.parent = parent.transform;
			t.localPosition = localPosition;
			t.localRotation = localRotation;
			t.localScale = localScale;
			self.layer = parent.layer;
			return self;
		}

		/// <summary>
		/// 子オブジェクトを一括で解除する
		/// </summary>
		public static GameObject DetachChildren( this GameObject self )
		{
			self.transform.DetachChildren();
			return self;
		}

		/// <summary>
		/// 向きを変更します
		/// </summary>
		public static GameObject LookAt( this GameObject self, GameObject target )
		{
			self.transform.LookAt( target.transform );
			return self;
		}

		/// <summary>
		/// 向きを変更します
		/// </summary>
		public static GameObject LookAt( this GameObject self, Transform target )
		{
			 self.transform.LookAt( target );
			 return self;
		}

		/// <summary>
		/// 向きを変更します
		/// </summary>
		public static GameObject LookAt( this GameObject self, Vector3 worldPosition )
		{
			self.transform.LookAt( worldPosition );
			return self;
		}

		/// <summary>
		/// 向きを変更します
		/// </summary>
		public static GameObject LookAt( this GameObject self, GameObject target, Vector3 worldUp )
		{
			self.transform.LookAt( target.transform, worldUp );
			return self;
		}

		/// <summary>
		/// 向きを変更します
		/// </summary>
		public static GameObject LookAt( this GameObject self, Transform target, Vector3 worldUp )
		{
			self.transform.LookAt( target, worldUp );
			return self;
		}

		/// <summary>
		/// 向きを変更します
		/// </summary>
		public static GameObject LookAt( this GameObject self, Vector3 worldPosition, Vector3 worldUp )
		{
			self.transform.LookAt( worldPosition, worldUp );
			return self;
		}

		/// <summary>
		/// 子オブジェクトの数を返します
		/// </summary>
		public static int ChildCount( this GameObject self )
		{
			return self.transform.childCount;
		}

		/// <summary>
		/// 子オブジェクトが存在するかどうかを返します
		/// </summary>
		public static bool HasChild( this GameObject self )
		{
			return 0 < self.transform.childCount;
		}

		/// <summary>
		/// 指定されたインデックスの子オブジェクトを返します
		/// </summary>
		public static Transform GetChild( this GameObject self, int index = 0 )
		{
			if ( index < 0 ) return null;
			return index <= self.transform.childCount - 1 ? self.transform.GetChild( index ) : null;
		}

		/// <summary>
		/// 子オブジェクトにする
		/// </summary>
		public static GameObject AddChild( this GameObject self, params GameObject[] parameters )
		{
			for ( int i = 0; i < parameters.Length; i++ )
			{
				parameters[ i ].transform.parent = self.transform;
			}
			return self;
		}

		/// <summary>
		/// 子オブジェクトにする
		/// </summary>
		public static GameObject AddChild( this GameObject self, params Component[] parameters )
		{
			for ( int i = 0; i < parameters.Length; i++ )
			{
				parameters[ i ].transform.parent = self.transform;
			}
			return self;
		}

		/// <summary>
		/// 親オブジェクトを返します
		/// </summary>
		public static Transform GetParent( this GameObject self )
		{
			return self.transform.parent;
		}

		/// <summary>
		/// 親オブジェクトを名前で検索して返します
		/// </summary>
		public static Transform FindParent( this GameObject self, string name )
		{
			var parent = self.transform.parent;
			while ( parent )
			{
				if ( parent.name == name )
				{
					return parent;
				}
				parent = parent.transform.parent;
			}
			return parent;
		}

		/// <summary>
		/// ルートとなるオブジェクトを返します
		/// </summary>
		public static GameObject GetRoot( this GameObject self )
		{
			var root = self.transform.root;
			return root != null ? root.gameObject : null;
		}

		/// <summary>
		/// レイヤーを設定します
		/// </summary>
		public static GameObject SetLayer( this GameObject self, int layer )
		{
			self.layer = layer;
			return self;
		}

		/// <summary>
		/// レイヤー名を使用してレイヤーを設定します
		/// </summary>
		public static GameObject SetLayer( this GameObject self, string layerName )
		{
			self.layer = LayerMask.NameToLayer( layerName );
			return self;
		}

		/// <summary>
		/// 自分自身を含めたすべての子オブジェクトのレイヤーを設定します
		/// </summary>
		public static GameObject SetLayerRecursively( this GameObject self, int layer )
		{
			self.layer = layer;
			foreach ( Transform n in self.transform )
			{
				SetLayerRecursively( n.gameObject, layer );
			}
			return self;
		}

		/// <summary>
		/// 自分自身を含めたすべての子オブジェクトのレイヤーを設定します
		/// </summary>
		public static GameObject SetLayerRecursively( this GameObject self, string layerName )
		{
			return self.SetLayerRecursively( LayerMask.NameToLayer( layerName ) );
		}

		/// <summary>
		/// グローバル座標系における X 軸方向のスケーリング値を返します
		/// </summary>
		public static float GetGlobalScaleX( this GameObject self )
		{
			var t = self.transform;
			var x = 1f;
			while ( t != null )
			{
				x *= t.localScale.x;
				t = t.parent;
			}
			return x;
		}

		/// <summary>
		/// グローバル座標系における Y 軸方向のスケーリング値を返します
		/// </summary>
		public static float GetGlobalScaleY( this GameObject self )
		{
			var t = self.transform;
			var y = 1f;
			while ( t != null )
			{
				y *= t.localScale.y;
				t = t.parent;
			}
			return y;
		}

		/// <summary>
		/// グローバル座標系における Z 軸方向のスケーリング値を返します
		/// </summary>
		public static float GetGlobalScaleZ( this GameObject self )
		{
			var t = self.transform;
			var z = 1f;
			while ( t != null )
			{
				z *= t.localScale.z;
				t = t.parent;
			}
			return z;
		}

		/// <summary>
		/// グローバル座標系におけるスケーリング値を返します
		/// </summary>
		public static Vector3 GetGlobalScale( this GameObject self )
		{
			var t = self.transform;
			var scale = Vector3.one;
			while ( t != null )
			{
				scale.x *= t.localScale.x;
				scale.y *= t.localScale.y;
				scale.z *= t.localScale.z;
				t = t.parent;
			}
			return scale;
		}

		/// <summary>
		/// 指定されたゲームオブジェクトが null または非アクティブであるかどうかを示します
		/// </summary>
		public static bool IsNullOrInactive( this GameObject self )
		{
			return self == null || !self.activeInHierarchy || !self.activeSelf;
		}

		/// <summary>
		/// 指定されたゲームオブジェクトが null ではないかつ非アクティブではないかどうかを示します
		/// </summary>
		public static bool IsNotNullOrInactive( this GameObject self )
		{
			return !self.IsNullOrInactive();
		}

		/// <summary>
		/// コンポーネントのインターフェースを取得して返します
		/// </summary>
		public static T GetComponentInterface<T>( this GameObject self ) where T : class
		{
			foreach ( var n in self.GetComponents<Component>() )
			{
				var component = n as T;
				if ( component != null )
				{
					return component;
				}
			}
			return null;
		}

		/// <summary>
		/// コンポーネントのインターフェースを複数取得して返します
		/// </summary>
		public static T[] GetComponentInterfaces<T>( this GameObject self ) where T : class
		{
			var result = new List<T>();
			foreach ( var n in self.GetComponents<Component>() )
			{
				var component = n as T;
				if ( component != null )
				{
					result.Add( component );
				}
			}
			return result.ToArray();
		}

		/// <summary>
		/// 子のコンポーネントのインターフェースを取得して返します
		/// </summary>
		public static T GetComponentInterfaceInChildren<T>( this GameObject self ) where T : class
		{
			return self.GetComponentInterfaceInChildren<T>( false );
		}

		/// <summary>
		/// 子のコンポーネントのインターフェースを取得して返します
		/// </summary>
		public static T GetComponentInterfaceInChildren<T>( this GameObject self, bool includeInactive ) where T : class
		{
			foreach ( var n in self.GetComponentsInChildren<Component>( includeInactive ) )
			{
				var component = n as T;
				if ( component != null )
				{
					return component;
				}
			}
			return null;
		}

		/// <summary>
		/// 子のコンポーネントのインターフェースを複数取得して返します
		/// </summary>
		public static T[] GetComponentInterfacesInChildren<T>( this GameObject self ) where T : class
		{
			return self.GetComponentInterfacesInChildren<T>( false );
		}

		/// <summary>
		/// 子のコンポーネントのインターフェースを複数取得して返します
		/// </summary>
		public static T[] GetComponentInterfacesInChildren<T>( this GameObject self, bool includeInactive ) where T : class
		{
			var result = new List<T>();
			foreach ( var n in self.GetComponentsInChildren<Component>( includeInactive ) )
			{
				var component = n as T;
				if ( component != null )
				{
					result.Add( component );
				}
			}
			return result.ToArray();
		}

		/// <summary>
		/// 無効なコンポーネントがアタッチされている場合 true を返します
		/// </summary>
		public static bool HasMissingScript( this GameObject self )
		{
			return self
				.GetComponents<Component>()
				.Any( c => c == null )
			;
		}

		/// <summary>
		/// アクティブ状態を逆にします
		/// </summary>
		public static GameObject ReverseActive( this GameObject self )
		{
			self.SetActive( !self.activeSelf );
			return self;
		}

		/// <summary>
		/// 指定されたアクティブと逆の状態にしてから指定されたアクティブになります
		/// </summary>
		public static GameObject ToggleActive( this GameObject self, bool isActive )
		{
			self.SetActive( !isActive );
			self.SetActive( isActive );
			return self;
		}

		/// <summary>
		/// すべての親オブジェクトを返します
		/// </summary>
		public static GameObject[] GetAllParent( this GameObject self )
		{
			var result = new List<GameObject>();
			for ( var parent = self.transform.parent; parent != null; parent = parent.parent )
			{
				result.Add( parent.gameObject );
			}
			return result.ToArray();
		}

		/// <summary>
		/// ルートパスを返します
		/// </summary>
		public static string GetRootPath( this GameObject self )
		{
			var path   = self.name;
			var parent = self.transform.parent;

			while ( parent != null )
			{
				path   = parent.name + "/" + path;
				parent = parent.parent;
			}

			return path;
		}

		/// <summary>
		/// グローバル座標を返します
		/// </summary>
		public static Vector3 GetGlobalPosition( this GameObject self )
		{
			var result = Vector3.zero;
			while ( self != null )
			{
				var t = self.transform;
				result += t.localPosition;
				self.transform.parent = t.parent;
			}
			return result;
		}

		/// <summary>
		/// ローカル座標系の位置を四捨五入します
		/// </summary>
		public static GameObject RoundLocalPosition( this GameObject self )
		{
			var v = self.transform.localPosition;
			v.x = Mathf.Round( v.x );
			v.y = Mathf.Round( v.y );
			v.z = Mathf.Round( v.z );
			self.transform.localPosition = v;
			return self;
		}

		/// <summary>
		/// ローカル座標系の回転角を四捨五入します
		/// </summary>
		public static GameObject RoundLocalEulerAngles( this GameObject self )
		{
			var v = self.transform.localEulerAngles;
			v.x = Mathf.Round( v.x );
			v.y = Mathf.Round( v.y );
			v.z = Mathf.Round( v.z );
			self.transform.localEulerAngles = v;
			return self;
		}

		/// <summary>
		/// ローカル座標系のスケーリング値を四捨五入します
		/// </summary>
		public static GameObject RoundLocalScale( this GameObject self )
		{
			var v = self.transform.localScale;
			v.x = Mathf.Round( v.x );
			v.y = Mathf.Round( v.y );
			v.z = Mathf.Round( v.z );
			self.transform.localScale = v;
			return self;
		}

		/// <summary>
		/// ローカル座標系の位置、回転角、スケーリング値を四捨五入します
		/// </summary>
		public static GameObject Round( this GameObject self )
		{
			self.RoundLocalPosition();
			self.RoundLocalEulerAngles();
			self.RoundLocalScale();
			return self;
		}

		/// <summary>
		/// 指定された名前にする
		/// </summary>
		public static GameObject SetName( this GameObject self, string name )
		{
			self.name = name;
			return self;
		}

		/// <summary>
		/// 指定されたタグ名にする
		/// </summary>
		public static GameObject SetTag( this GameObject self, string name )
		{
			self.tag = name;
			return self;
		}

		/// <summary>
		/// 指定された名前とタグ名にする
		/// </summary>
		public static GameObject SetNameAndTag( this GameObject self, string name, string tagName )
		{
			self.name = name;
			self.tag = tagName;
			return self;
		}

		/// <summary>
		/// オブジェクトがNullならオブジェクトを生成します。
		/// </summary>
		public static GameObject IfNullInstantiate( this GameObject self, GameObject prefab )
		{
			return self != null ? self : GameObject.Instantiate( prefab ) as GameObject;
		}

		/// <summary>
		/// ゲームオブジェクトを生成して座標、スケール、親オブジェクトを同一にします
		/// </summary>
		public static GameObject Clone( this GameObject self )
		{
			if( self != null ) return self;

			var go = GameObject.Instantiate( self ) as GameObject;
			go.transform.parent = self.transform.parent;
			go.transform.localPosition = self.transform.localPosition;
			go.transform.localScale = self.transform.localScale;
			return go;
		}

		/// <summary>
        /// イベント送信
        /// </summary>
        public static GameObject SendEvent( this GameObject self )
        {
			if (self == null) return self;
            if (!UnityEngine.EventSystems.ExecuteEvents.Execute<IReceiveEvent>(self, null, (target, ev) => target.OnReceive()))
            {
                for (int i = 0; i < self.transform.childCount; i++)
                {
                    self.transform.GetChild(i).gameObject.SendEvent();
                }
            }
			return self;
        }

		/// <summary>
        /// イベント送信
        /// </summary>
        public static GameObject SendEventObject( this GameObject self, object obj )
        {
			if (self == null) return self;
            if (!UnityEngine.EventSystems.ExecuteEvents.Execute<IReceiveEventObject>(self, null, (target, ev) => target.OnReceiveObject( obj )))
            {
                for (int i = 0; i < self.transform.childCount; i++)
                {
                    self.transform.GetChild(i).gameObject.SendEventObject( obj );
                }
            }
			return self;
        }

		/// <summary>
        /// イベント送信
        /// </summary>
        public static GameObject SendEventObjects( this GameObject self, params object[] args )
        {
			if (self == null) return self;
            if (!UnityEngine.EventSystems.ExecuteEvents.Execute<IReceiveEventObjects>(self, null, (target, ev) => target.OnReceiveObjects( args )))
            {
                for (int i = 0; i < self.transform.childCount; i++)
                {
                    self.transform.GetChild(i).gameObject.SendEventObjects( args );
                }
            }
			return self;
        }

		/// <summary>
        /// selfから一番近いオブジェクトを検索
        /// </summary>
		public static GameObject FindNearObject( this Vector2 self, IList<GameObject> list )
		{
			var tempDis = 0f;
			var nearDis = 0f;
			var result = null as GameObject;
			foreach ( var go in list )
			{
				tempDis = Vector2.Distance( self, go.transform.position );
				if ( nearDis == 0f || nearDis > tempDis )
				{
					nearDis = tempDis;
					result = go;
				}
			}
			return result;
		}

		/// <summary>
        /// selfから一番近いオブジェクトを検索
        /// </summary>
		public static GameObject FindNearObject( this Vector3 self, IList<GameObject> list )
		{
			var tempDis = 0f;
			var nearDis = 0f;
			var result = null as GameObject;
			foreach ( var go in list )
			{
				tempDis = Vector3.Distance( self, go.transform.position );
				if ( nearDis == 0f || nearDis > tempDis )
				{
					nearDis = tempDis;
					result = go;
				}
			}
			return result;
		}

		/// <summary>
        /// selfから一番近いオブジェクトを検索
        /// </summary>
		public static GameObject FindNearObject( this GameObject self, IList<GameObject> list )
		{
			return !self ? null : self.transform.position.FindNearObject( list );
		}

		/// <summary>
        /// selfから一番近いオブジェクトを検索
        /// </summary>
		public static GameObject FindNearObject( this Component self, IList<GameObject> list )
		{
			return !self.gameObject ? null : self.transform.position.FindNearObject( list );
		}

		/// <summary>
        /// selfから一番遠いオブジェクトを検索
        /// </summary>
		public static GameObject FindFarObject( this Vector2 self, IList<GameObject> list )
		{
			var tempDis = 0f;
			var nearDis = 0f;
			var result = null as GameObject;
			foreach ( var go in list )
			{
				tempDis = Vector2.Distance( self, go.transform.position );
				if ( nearDis == 0f || nearDis < tempDis )
				{
					nearDis = tempDis;
					result = go;
				}
			}
			return result;
		}

		/// <summary>
        /// selfから一番遠いオブジェクトを検索
        /// </summary>
		public static GameObject FindFarObject( this Vector3 self, IList<GameObject> list )
		{
			var tempDis = 0f;
			var nearDis = 0f;
			var result = null as GameObject;
			foreach ( var go in list )
			{
				tempDis = Vector3.Distance( self, go.transform.position );
				if ( nearDis == 0f || nearDis < tempDis )
				{
					nearDis = tempDis;
					result = go;
				}
			}
			return result;
		}

		/// <summary>
        /// selfから一番遠いオブジェクトを検索
        /// </summary>
		public static GameObject FindFarObject( this GameObject self, IList<GameObject> list )
		{
			return !self ? null : self.transform.position.FindFarObject( list );
		}

		/// <summary>
        /// selfから一番遠いオブジェクトを検索
        /// </summary>
		public static GameObject FindFarObject( this Component self, IList<GameObject> list )
		{
			return !self.gameObject ? null : self.transform.position.FindFarObject( list );
		}

		/// <summary>
        /// 引数からの距離を昇順
        /// </summary>
		public static List<GameObject> SortByDistance( this List<GameObject> self, Vector2 pos )
		{
			self.Sort( delegate( GameObject a, GameObject b ) { return Vector2.Distance( pos, a.transform.position ).CompareTo( Vector2.Distance( pos, b.transform.position ) ); } );
			return self;
		}

		/// <summary>
        /// 引数からの距離を昇順
        /// </summary>
		public static List<GameObject> SortByDistance( this List<GameObject> self, Vector3 pos )
		{
			self.Sort( delegate( GameObject a, GameObject b ) { return Vector3.Distance( pos, a.transform.position ).CompareTo( Vector3.Distance( pos, b.transform.position ) ); } );
			return self;
		}

		/// <summary>
        /// 引数からの距離を昇順
        /// </summary>
		public static List<GameObject> SortByDistance( this List<GameObject> self, GameObject target )
		{
			return !target ? self : self.SortByDistance( target.transform.position );
		}

		/// <summary>
        /// 引数からの距離を昇順
        /// </summary>
		public static List<GameObject> SortByDistance( this List<GameObject> self, Component target )
		{
			return !target.gameObject ? self : self.SortByDistance( target.transform.position );
		}

		/// <summary>
        /// 引数からの距離を降順
        /// </summary>
		public static List<GameObject> SortDescendingByDistance( this List<GameObject> self, Vector2 pos )
		{
			self.Sort( delegate( GameObject a, GameObject b ) { return Vector2.Distance( pos, b.transform.position ).CompareTo( Vector2.Distance( pos, a.transform.position ) ); } );
			return self;
		}

		/// <summary>
        /// 引数からの距離を降順
        /// </summary>
		public static List<GameObject> SortDescendingByDistance( this List<GameObject> self, Vector3 pos )
		{
			self.Sort( delegate( GameObject a, GameObject b ) { return Vector3.Distance( pos, b.transform.position ).CompareTo( Vector3.Distance( pos, a.transform.position ) ); } );
			return self;
		}

		/// <summary>
        /// 引数からの距離を降順
        /// </summary>
		public static List<GameObject> SortDescendingByDistance( this List<GameObject> self, GameObject target )
		{
			return !target ? self : self.SortDescendingByDistance( target.transform.position );
		}

		/// <summary>
        /// 引数からの距離を降順
        /// </summary>
		public static List<GameObject> SortDescendingByDistance( this List<GameObject> self, Component target )
		{
			return !target.gameObject ? self : self.SortDescendingByDistance( target.transform.position );
		}

		/// <summary>
		/// 新しいシーンを読み込む時に自動で破棄されないようにします
		/// </summary>
		public static GameObject DontDestroyOnLoad( this GameObject self )
		{
			GameObject.DontDestroyOnLoad( self );
			return self;
		}

		/// <summary>
		/// オブジェクトを破棄
		/// </summary>
		public static GameObject Destroy( this GameObject self, float? time = null )
		{
			if ( !time.HasValue )
				GameObject.Destroy( self );
			else
				GameObject.Destroy( self, time.Value );
			return self;
		}

		/// <summary>
		/// オブジェクトを破棄
		/// </summary>
		public static GameObject DestroyImmediate( this GameObject self, bool allowDestroyingAssets = false )
		{
			GameObject.DestroyImmediate( self, allowDestroyingAssets );
			return self;
		}

		/// <summary>
		/// 同階層における順序を返す
		/// </summary>
		public static int GetSiblingIndex( this GameObject self )
		{
			return self.transform.GetSiblingIndex();
		}

		/// <summary>
		/// 同階層における順序をindex番にして返す
		/// </summary>
		public static GameObject SetSiblingIndex( this GameObject self, int index )
		{
			self.transform.SetSiblingIndex( index );
			return self;
		}

		/// <summary>
		/// 同階層における順序を最初にして返す
		/// </summary>
		public static GameObject SetAsFirstSibling( this GameObject self )
		{
			self.transform.SetAsFirstSibling();
			return self;
		}

		/// <summary>
		/// 同階層における順序を最後にして返す
		/// </summary>
		public static GameObject SetAsLastSibling( this GameObject self )
		{
			self.transform.SetAsLastSibling();
			return self;
		}
	}
}