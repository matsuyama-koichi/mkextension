﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Text;

namespace Main
{
    public static class TextExt
    {
		/// <summary>
		/// テキストを設定します
		/// </summary>
	    public static Text SetText( this Text self, string format, params object[] args )
	    {
	        self.text = string.Format( format, args );
            return self;
	    }

	    /// <summary>
	    /// テキストを設定します
	    /// </summary>
	    public static Text SetText( this Text self, string          text    ) { self.text = text; return self;             }
	    public static Text SetText( this Text self, int             value   ) { return self.SetText( value .ToString() );  }
	    public static Text SetText( this Text self, ushort          value   ) { return self.SetText( value .ToString() );  }
	    public static Text SetText( this Text self, uint            value   ) { return self.SetText( value .ToString() );  }
	    public static Text SetText( this Text self, ulong           value   ) { return self.SetText( value .ToString() );  }
	    public static Text SetText( this Text self, long            value   ) { return self.SetText( value .ToString() );  }
	    public static Text SetText( this Text self, byte            value   ) { return self.SetText( value .ToString() );  }
	    public static Text SetText( this Text self, StringBuilder   sb      ) { return self.SetText( sb    .ToString() );  }
	    public static Text SetText( this Text self, StringAppender  sa      ) { return self.SetText( sa    .ToString() );  }

        public static Text SetColor( this Text self, Color color )
        {
            self.color = color;
            return self;
        }
        
        public static Text SetColor( this Text self, float r, float g, float b, float? a = null )
        {
            self.color = new Color
            (
                r, 
                g, 
                b, 
                !a.HasValue ? self.color.a : a.Value
            );
            return self;
        }

        public static Text SetColorR( this Text self, float r )
        {
            self.color = new Color
            (
                r,
                self.color.g,
                self.color.b,
                self.color.a
            );
            return self;
        }

        public static Text SetColorG (this Text self, float g )
        {
            self.color = new Color
            (
                self.color.r,
                g,
                self.color.b,
                self.color.a
            );
            return self;
        }

        public static Text SetColorB( this Text self, float b )
        {
            self.color = new Color
            (
                self.color.r,
                self.color.g,
                b,
                self.color.a
            );
            return self;
        }

        public static Text SetColorA( this Text self, float a )
        {
            self.color = new Color
            (
                self.color.r,
                self.color.g,
                self.color.b,
                a
            );
            return self;
        }
    }
}