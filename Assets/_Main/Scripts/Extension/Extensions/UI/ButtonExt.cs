﻿using UnityEngine;
using UnityEngine.UI;

namespace Main
{
    public static class ButtonExt
    {
        public static Button SetInteractable( this Button self, bool flag )
        {
            self.interactable = flag;
            return self;
        }

        public static Button SetTransition( this Button self, Button.Transition transition )
        {
            self.transition = transition;
            return self;
        }

        public static Button SetGraphic( this Button self, Graphic graphic )
        {
            self.targetGraphic = graphic;
            return self;
        }

        public static Button SetColorTint( this Button self, Color normalColor, Color highlightedColor, Color pressedColor, Color disabledColor, float colorMultiplier = 1f, float fadeDuration = 0f )
        {
            self.colors = new ColorBlock() 
            { 
                normalColor = normalColor,
                highlightedColor = highlightedColor,
                pressedColor = pressedColor,
                disabledColor = disabledColor,
                colorMultiplier = colorMultiplier,
                fadeDuration = fadeDuration
            };
            return self;
        }

        public static Button SetSpriteSwap( this Button self, Sprite highlightedSprite, Sprite pressedSprite, Sprite disabledSprite )
        {
            self.spriteState = new SpriteState()
            {
                highlightedSprite = highlightedSprite,
                pressedSprite = pressedSprite,
                disabledSprite = disabledSprite
            };
            return self;
        }

        public static Button SetAnimation( this Button self, string normalTrigger, string highlightedTrigger, string pressedTrigger, string disabledTrigger )
        {
            self.animationTriggers = new AnimationTriggers()
            {
                normalTrigger = normalTrigger,
                highlightedTrigger = highlightedTrigger,
                pressedTrigger = pressedTrigger,
                disabledTrigger = disabledTrigger
            };
            return self;
        }

        public static Button SetNavigation( this Button self, Navigation.Mode mode )
        {
            var nav = self.navigation;
            nav.mode = mode;
            self.navigation = nav;
            return self;
        }

        public static Button SetSelectable( this Button self, Button selectOnUp, Button selectOnDown, Button selectOnLeft, Button selectOnRight )
        {
            var nav = self.navigation;
            nav.selectOnUp = selectOnUp;
            nav.selectOnDown = selectOnDown;
            nav.selectOnLeft = selectOnLeft;
            nav.selectOnRight = selectOnRight;
            self.navigation = nav;
            return self;
        }
    }
}