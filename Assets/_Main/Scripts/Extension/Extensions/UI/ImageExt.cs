﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace Main
{
    public static class ImageExt
    {
        /// <summary>
		/// sprite を設定します
		/// </summary>
        public static Image SetSprite( this Image self, Sprite sprite )
        {
            self.sprite = sprite;
            return self;
        }

        /// <summary>
		/// sprite を設定します
		/// </summary>
		public static Image SetSpriteAndSnap( this Image self, Sprite sprite )
		{
			self.sprite = sprite;
			self.SetNativeSize();
            return self;
		}

        public static Image SetMaterial( this Image self, Material material )
        {
            self.material = material;
            return self;
        }

        public static Image SetColor( this Image self, Color color )
        {
            self.color = color;
            return self;
        }

        public static Image SetColor( this Image self, float r, float g, float b, float? a = null )
        {
            self.color = new Color
            (
                r, 
                g, 
                b, 
                !a.HasValue ? self.color.a : a.Value
            );
            return self;
        }

        public static Image SetColorR( this Image self, float r )
        {
            self.color = new Color
            (
                r,
                self.color.g,
                self.color.b,
                self.color.a
            );
            return self;
        }

        public static Image SetColorG( this Image self, float g )
        {
            self.color = new Color
            (
                self.color.r,
                g,
                self.color.b,
                self.color.a
            );
            return self;
        }

        public static Image SetColorB( this Image self, float b )
        {
            self.color = new Color
            (
                self.color.r,
                self.color.g,
                b,
                self.color.a
            );
            return self;
        }

        public static Image SetColorA( this Image self, float a )
        {
            self.color = new Color
            (
                self.color.r,
                self.color.g,
                self.color.b,
                a
            );
            return self;
        }

        public static Image SetType( this Image self, Image.Type type )
        {
            self.type = type;
            return self;
        }

        public static Image SetRaycast( this Image self, bool flag )
        {
            self.raycastTarget = flag;
            return self;
        }
    }
}

