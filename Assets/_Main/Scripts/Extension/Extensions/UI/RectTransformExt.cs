﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

namespace Main
{
    public enum RectAnchorPresets
    {
        TopLeft, TopCenter, TopRight,
        MiddleLeft, MiddleCenter, MiddleRight,
        BottomLeft, BottonCenter, BottomRight, BottomStretch,
        VertStretchLeft, VertStretchRight, VertStretchCenter,
        HorStretchTop, HorStretchMiddle, HorStretchBottom,
        StretchAll
    }

    public enum RectPivotPresets
    {
        TopLeft, TopCenter, TopRight,
        MiddleLeft, MiddleCenter, MiddleRight,
        BottomLeft, BottomCenter, BottomRight,
    }

    public static class RectTransformExt
    {
		/// <summary>
		/// 幅を返します
		/// </summary>
		public static float GetSizeX( this RectTransform self )
		{
			return self.sizeDelta.x;
		}

		/// <summary>
		/// 高さを返します
		/// </summary>
		public static float GetSizeY( this RectTransform self )
		{
			return self.sizeDelta.y;
		}

		/// <summary>
		/// 幅を設定します
		/// </summary>
		public static RectTransform SetSizeX( this RectTransform self, float x )
		{
			self.sizeDelta = new Vector2( x, self.sizeDelta.y );
            return self;
		}

		/// <summary>
		/// 高さを設定します
		/// </summary>
		public static RectTransform SetSizeY( this RectTransform self, float y )
		{
			self.sizeDelta = new Vector2( self.sizeDelta.x, y );
            return self;
		}

        /// <summary>
		/// 幅を加算します
		/// </summary>
		public static RectTransform AddSizeX( this RectTransform self, float x )
		{
			self.sizeDelta = new Vector2( self.sizeDelta.x + x, self.sizeDelta.y );
            return self;
		}

        /// <summary>
		/// 高さを加算します
		/// </summary>
		public static RectTransform AddSizeY( this RectTransform self, float y )
		{
			self.sizeDelta = new Vector2( self.sizeDelta.x, self.sizeDelta.y + y );
            return self;
		}

		/// <summary>
		/// サイズを設定します
		/// </summary>
		public static RectTransform SetSize( this RectTransform self, float x, float y )
		{
			self.sizeDelta = new Vector2( x, y );
            return self;
		}

		/// <summary>
		/// サイズを設定します
		/// </summary>
		public static RectTransform SetSize( this RectTransform self, Vector2 sizeDelta )
		{
			self.sizeDelta = sizeDelta;
            return self;
		}

        /// <summary>
		/// サイズを加算します
		/// </summary>
		public static RectTransform AddSize( this RectTransform self, float x, float y )
		{
			self.sizeDelta += new Vector2(x, y);
            return self;
		}

        /// <summary>
		/// サイズを加算します
		/// </summary>
		public static RectTransform AddSize( this RectTransform self, Vector2 sizeDelta )
		{
			self.sizeDelta += sizeDelta;
            return self;
		}

		/// <summary>
		/// anchoredPosition を設定します
		/// </summary>
		public static RectTransform SetAnchoredPosition( this RectTransform self, Vector2 value )
		{
			self.anchoredPosition = value;
            return self;
		}
		
		/// <summary>
		/// anchoredPosition を設定します
		/// </summary>
		public static RectTransform SetAnchoredPosition( this RectTransform self, float x, float y )
		{
			self.anchoredPosition = new Vector2( x, y );
            return self;
		}

		/// <summary>
		/// anchoredPosition を加算します
		/// </summary>
        public static RectTransform AddAnchoredPosition( this RectTransform self, Vector2 value )
        {
            self.anchoredPosition += value;
            return self;
        }

        /// <summary>
		/// anchoredPosition を加算します
		/// </summary>
        public static RectTransform AddAnchoredPosition( this RectTransform self, float x, float y )
        {
            self.anchoredPosition += new Vector2( x, y );
            return self;
        }
		
		/// <summary>
		/// anchoredPosition.x を設定します
		/// </summary>
		public static RectTransform SetAnchoredPositionX( this RectTransform self, float x )
		{
			var pos = self.anchoredPosition;
			pos.x                 = x;
			self.anchoredPosition = pos;
            return self;
		}

		/// <summary>
		/// anchoredPosition.y を設定します
		/// </summary>
		public static RectTransform SetAnchoredPositionY( this RectTransform self, float y )
		{
			var pos = self.anchoredPosition;
			pos.y                 = y;
			self.anchoredPosition = pos;
            return self;
		}

        /// <summary>
		/// anchoredPosition.x を加算します
		/// </summary>
		public static RectTransform AddAnchoredPositionX( this RectTransform self, float x )
		{
			var pos = self.anchoredPosition;
			pos.x                 += x;
			self.anchoredPosition = pos;
            return self;
		}

        /// <summary>
		/// anchoredPosition.y を加算します
		/// </summary>
		public static RectTransform AddAnchoredPositionY( this RectTransform self, float y )
		{
			var pos = self.anchoredPosition;
			pos.y                 += y;
			self.anchoredPosition = pos;
            return self;
		}
		
		/// <summary>
		/// pivot.x を設定します
		/// </summary>
		public static RectTransform SetPivotX( this RectTransform self, float x )
		{
			var size = self.pivot;
			size.x     = x;
			self.pivot = size;
            return self;
		}
    
		/// <summary>
		/// pivot.y を設定します
		/// </summary>
		public static RectTransform SetPivotY( this RectTransform self, float y )
		{
			var size = self.pivot;
			size.y     = y;
			self.pivot = size;
            return self;
		}
    
		/// <summary>
		/// pivot を設定します
		/// </summary>
		public static RectTransform SetPivot( this RectTransform self, Vector2 pivot )
		{
			self.pivot = pivot;
            return self;
		}

		/// <summary>
		/// pivot を設定します
		/// </summary>
		public static RectTransform SetPivot( this RectTransform self, float x, float y )
		{
			self.pivot = new Vector2( x, y );
            return self;
		}

		/// <summary>
		/// offsetMin.x を設定します
		/// </summary>
		public static RectTransform SetOffsetMinX( this RectTransform self, float x )
		{
			var offsetMin = self.offsetMin;
			offsetMin.x     = x;
			self.offsetMin = offsetMin;
            return self;
		}
    
		/// <summary>
		/// offsetMin.y を設定します
		/// </summary>
		public static RectTransform SetOffsetMinY( this RectTransform self, float y )
		{
			var offsetMin = self.offsetMin;
			offsetMin.y     = y;
			self.offsetMin = offsetMin;
            return self;
		}

        /// <summary>
		/// offsetMin.x を加算します
		/// </summary>
		public static RectTransform AddOffsetMinX( this RectTransform self, float x )
		{
			var offsetMin = self.offsetMin;
			offsetMin.x     += x;
			self.offsetMin = offsetMin;
            return self;
		}

        /// <summary>
		/// offsetMin.y を設定します
		/// </summary>
		public static RectTransform AddOffsetMinY( this RectTransform self, float y )
		{
			var offsetMin = self.offsetMin;
			offsetMin.y     += y;
			self.offsetMin = offsetMin;
            return self;
		}
    
		/// <summary>
		/// offsetMin を設定します
		/// </summary>
		public static RectTransform SetOffsetMin( this RectTransform self, Vector2 offsetMin )
		{
			self.offsetMin = offsetMin;
            return self;
		}

		/// <summary>
		/// offsetMin を設定します
		/// </summary>
		public static RectTransform SetOffsetMin( this RectTransform self, float x, float y )
		{
			self.offsetMin = new Vector2( x, y );
            return self;
		}

        /// <summary>
		/// offsetMin を加算します
		/// </summary>
		public static RectTransform AddOffsetMin( this RectTransform self, Vector2 offsetMin )
		{
			self.offsetMin += offsetMin;
            return self;
		}

        /// <summary>
		/// offsetMin を加算します
		/// </summary>
		public static RectTransform AddOffsetMin( this RectTransform self, float x, float y )
		{
			self.offsetMin += new Vector2( x, y );
            return self;
		}

		/// <summary>
		/// offsetMax.x を設定します
		/// </summary>
		public static RectTransform SetOffsetMaxX( this RectTransform self, float x )
		{
			var offsetMax = self.offsetMax;
			offsetMax.x    = x;
			self.offsetMax = offsetMax;
            return self;
		}
    
		/// <summary>
		/// offsetMax.y を設定します
		/// </summary>
		public static RectTransform SetOffsetMaxY( this RectTransform self, float y )
		{
			var offsetMax = self.offsetMax;
			offsetMax.y    = y;
			self.offsetMax = offsetMax;
            return self;
		}

        /// <summary>
		/// offsetMax.x を加算します
		/// </summary>
		public static RectTransform AddOffsetMaxX( this RectTransform self, float x )
		{
			var offsetMax = self.offsetMax;
			offsetMax.x    += x;
			self.offsetMax = offsetMax;
            return self;
		}

        /// <summary>
		/// offsetMax.y を加算します
		/// </summary>
		public static RectTransform AddOffsetMaxY( this RectTransform self, float y )
		{
			var offsetMax = self.offsetMax;
			offsetMax.y    += y;
			self.offsetMax = offsetMax;
            return self;
		}

		/// <summary>
		/// offsetMax を設定します
		/// </summary>
		public static RectTransform SetOffsetMax( this RectTransform self, Vector2 offsetMax )
		{
			self.offsetMax = offsetMax;
            return self;
		}

		/// <summary>
		/// offsetMax を設定します
		/// </summary>
		public static RectTransform SetOffsetMax( this RectTransform self, float x, float y )
		{
			self.offsetMax = new Vector2( x, y );
            return self;
		}

        /// <summary>
		/// offsetMax を加算します
		/// </summary>
		public static RectTransform AddOffsetMax( this RectTransform self, Vector2 offsetMax )
		{
			self.offsetMax += offsetMax;
            return self;
		}

		/// <summary>
		/// offsetMax を加算します
		/// </summary>
		public static RectTransform AddOffsetMax( this RectTransform self, float x, float y )
		{
			self.offsetMax += new Vector2( x, y );
            return self;
		}

		/// <summary>
		/// 座標を保ったままPivotを変更する
		/// </summary>
		/// <param name="rectTransform">自身の参照</param>
		/// <param name="targetPivot">変更先のPivot座標</param>
		public static void SetPivotWithKeepingPosition(this RectTransform rectTransform, Vector2 targetPivot)
		{
			var diffPivot = targetPivot - rectTransform.pivot;
			rectTransform.pivot = targetPivot;
			var diffPos = new Vector2(rectTransform.sizeDelta.x * diffPivot.x, rectTransform.sizeDelta.y * diffPivot.y);
			rectTransform.anchoredPosition += diffPos;
		}

		/// <summary>
		/// 座標を保ったままPivotを変更する
		/// </summary>
		/// <param name="rectTransform">自身の参照</param>
		/// <param name="x">変更先のPivotのx座標</param>
		/// <param name="y">変更先のPivotのy座標</param>
		public static void SetPivotWithKeepingPosition(this RectTransform rectTransform, float x, float y)
		{
			rectTransform.SetPivotWithKeepingPosition(new Vector2(x, y));
		}

		/// <summary>
		/// 座標を保ったままAnchorを変更する
		/// </summary>
		/// <param name="rectTransform">自身の参照</param>
		/// <param name="targetAnchor">変更先のAnchor座標 (min,maxが共通の場合)</param>
		public static void SetAnchorWithKeepingPosition(this RectTransform rectTransform, Vector2 targetAnchor)
		{
			rectTransform.SetAnchorWithKeepingPosition(targetAnchor, targetAnchor);
		}

		/// <summary>
		/// 座標を保ったままAnchorを変更する
		/// </summary>
		/// <param name="rectTransform">自身の参照</param>
		/// <param name="x">変更先のAnchorのx座標 (min,maxが共通の場合)</param>
		/// <param name="y">変更先のAnchorのy座標 (min,maxが共通の場合)</param>
		public static void SetAnchorWithKeepingPosition(this RectTransform rectTransform, float x, float y)
		{
			rectTransform.SetAnchorWithKeepingPosition(new Vector2(x, y));
		}

		/// <summary>
		/// 座標を保ったままAnchorを変更する
		/// </summary>
		/// <param name="rectTransform">自身の参照</param>
		/// <param name="targetMinAnchor">変更先のAnchorMin座標</param>
		/// <param name="targetMaxAnchor">変更先のAnchorMax座標</param>
		public static void SetAnchorWithKeepingPosition(this RectTransform rectTransform, Vector2 targetMinAnchor, Vector2 targetMaxAnchor)
		{
			var parent = rectTransform.parent as RectTransform;
			if (parent == null) { Debug.LogError("Parent cannot find."); }

			var diffMin = targetMinAnchor - rectTransform.anchorMin;
			var diffMax = targetMaxAnchor - rectTransform.anchorMax;
			// anchorの更新
			rectTransform.anchorMin = targetMinAnchor;
			rectTransform.anchorMax = targetMaxAnchor;
			// 上下左右の距離の差分を計算
			var diffLeft = parent.rect.width * diffMin.x;
			var diffRight = parent.rect.width * diffMax.x;
			var diffBottom = parent.rect.height * diffMin.y;
			var diffTop = parent.rect.height * diffMax.y;
			// サイズと座標の修正
			rectTransform.sizeDelta += new Vector2(diffLeft - diffRight, diffBottom - diffTop);
			var pivot = rectTransform.pivot;
			rectTransform.anchoredPosition -= new Vector2(
				(diffLeft * (1 - pivot.x)) + (diffRight * pivot.x),
				(diffBottom * (1 - pivot.y)) + (diffTop * pivot.y)
			);
		}

		/// <summary>
		/// 座標を保ったままAnchorを変更する
		/// </summary>
		/// <param name="rectTransform">自身の参照</param>
		/// <param name="minX">変更先のAnchorMinのx座標</param>
		/// <param name="minY">変更先のAnchorMinのy座標</param>
		/// <param name="maxX">変更先のAnchorMaxのx座標</param>
		/// <param name="maxY">変更先のAnchorMaxのy座標</param>
		public static void SetAnchorWithKeepingPosition(this RectTransform rectTransform, float minX, float minY, float maxX, float maxY)
		{
			rectTransform.SetAnchorWithKeepingPosition(new Vector2(minX, minY), new Vector2(maxX, maxY));
		}

        public static RectTransform SetPivot(this RectTransform self, RectPivotPresets preset)
        {
            switch (preset)
            {
                case (RectPivotPresets.TopLeft):
                {
                    self.pivot = new Vector2(0, 1);
                    break;
                }
                case (RectPivotPresets.TopCenter):
                {
                    self.pivot = new Vector2(0.5f, 1);
                    break;
                }
                case (RectPivotPresets.TopRight):
                {
                    self.pivot = new Vector2(1, 1);
                    break;
                }
                case (RectPivotPresets.MiddleLeft):
                {
                    self.pivot = new Vector2(0, 0.5f);
                    break;
                }
                case (RectPivotPresets.MiddleCenter):
                {
                    self.pivot = new Vector2(0.5f, 0.5f);
                    break;
                }
                case (RectPivotPresets.MiddleRight):
                {
                    self.pivot = new Vector2(1, 0.5f);
                    break;
                }
                case (RectPivotPresets.BottomLeft):
                {
                    self.pivot = new Vector2(0, 0);
                    break;
                }
                case (RectPivotPresets.BottomCenter):
                {
                    self.pivot = new Vector2(0.5f, 0);
                    break;
                }
                case (RectPivotPresets.BottomRight):
                {
                    self.pivot = new Vector2(1, 0);
                    break;
                }
            }
            return self;
        }

        public static RectTransform SetAnchor(this RectTransform self, RectAnchorPresets preset, int offsetX = 0, int offsetY = 0)
        {
            self.anchoredPosition = new Vector3(offsetX, offsetY, 0);
            switch (preset)
            {
                case (RectAnchorPresets.TopLeft):
                {
                    self.anchorMin = new Vector2(0, 1);
                    self.anchorMax = new Vector2(0, 1);
                    break;
                }
                case (RectAnchorPresets.TopCenter):
                {
                    self.anchorMin = new Vector2(0.5f, 1);
                    self.anchorMax = new Vector2(0.5f, 1);
                    break;
                }
                case (RectAnchorPresets.TopRight):
                {
                    self.anchorMin = new Vector2(1, 1);
                    self.anchorMax = new Vector2(1, 1);
                    break;
                }
                case (RectAnchorPresets.MiddleLeft):
                {
                    self.anchorMin = new Vector2(0, 0.5f);
                    self.anchorMax = new Vector2(0, 0.5f);
                    break;
                }
                case (RectAnchorPresets.MiddleCenter):
                {
                    self.anchorMin = new Vector2(0.5f, 0.5f);
                    self.anchorMax = new Vector2(0.5f, 0.5f);
                    break;
                }
                case (RectAnchorPresets.MiddleRight):
                {
                    self.anchorMin = new Vector2(1, 0.5f);
                    self.anchorMax = new Vector2(1, 0.5f);
                    break;
                }
                case (RectAnchorPresets.BottomLeft):
                {
                    self.anchorMin = new Vector2(0, 0);
                    self.anchorMax = new Vector2(0, 0);
                    break;
                }
                case (RectAnchorPresets.BottonCenter):
                {
                    self.anchorMin = new Vector2(0.5f, 0);
                    self.anchorMax = new Vector2(0.5f, 0);
                    break;
                }
                case (RectAnchorPresets.BottomRight):
                {
                    self.anchorMin = new Vector2(1, 0);
                    self.anchorMax = new Vector2(1, 0);
                    break;
                }
                case (RectAnchorPresets.HorStretchTop):
                {
                    self.anchorMin = new Vector2(0, 1);
                    self.anchorMax = new Vector2(1, 1);
                    break;
                }
                case (RectAnchorPresets.HorStretchMiddle):
                {
                    self.anchorMin = new Vector2(0, 0.5f);
                    self.anchorMax = new Vector2(1, 0.5f);
                    break;
                }
                case (RectAnchorPresets.HorStretchBottom):
                {
                    self.anchorMin = new Vector2(0, 0);
                    self.anchorMax = new Vector2(1, 0);
                    break;
                }
                case (RectAnchorPresets.VertStretchLeft):
                {
                    self.anchorMin = new Vector2(0, 0);
                    self.anchorMax = new Vector2(0, 1);
                    break;
                }
                case (RectAnchorPresets.VertStretchCenter):
                {
                    self.anchorMin = new Vector2(0.5f, 0);
                    self.anchorMax = new Vector2(0.5f, 1);
                    break;
                }
                case (RectAnchorPresets.VertStretchRight):
                {
                    self.anchorMin = new Vector2(1, 0);
                    self.anchorMax = new Vector2(1, 1);
                    break;
                }
                case (RectAnchorPresets.StretchAll):
                {
                    self.anchorMin = new Vector2(0, 0);
                    self.anchorMax = new Vector2(1, 1);
                    break;
                }
            }
            return self;
        }
    }
}
