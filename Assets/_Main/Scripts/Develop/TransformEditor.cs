﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

[DisallowMultipleComponent]
[ExecuteInEditMode]
public class TransformEditor : MonoBehaviour
{
#if UNITY_EDITOR
    [Flags]
    public enum FreezeVector 
    { 
        X = 1 << 0, 
        Y = 1 << 1, 
        Z = 1 << 2 
    }
    [HideInInspector][EnumFlags] public FreezeVector freezePosition;
    [HideInInspector][EnumFlags] public FreezeVector freezeRotation;
    [HideInInspector][EnumFlags] public FreezeVector freezeScale;

    protected Vector3 fixedPosition, fixedRotation, fixedScale;
    public bool isEditable = true;

    void OnEnable()
    {
        fixedPosition = this.transform.position;
        fixedRotation = this.transform.eulerAngles;
        fixedScale = this.transform.localScale;
    }

    // Update is called once per frame
    void Update()
    {
        if (EditorApplication.isPlaying) return;
        if (!isEditable) return;

        // Position
        var position = this.transform.position;
        if (freezePosition.HasFlag(FreezeVector.X))
        {
            position.x = fixedPosition.x;
        }
        if (freezePosition.HasFlag(FreezeVector.Y))
        {
            position.y = fixedPosition.y;
        }
        if (freezePosition.HasFlag(FreezeVector.Z))
        {
            position.z = fixedPosition.z;
        }
        this.transform.position = position;

        // EulerAngles
        var eulerAngles = this.transform.eulerAngles;
        if (freezeRotation.HasFlag(FreezeVector.X))
        {
            eulerAngles.x = fixedRotation.x;
        }
        if (freezeRotation.HasFlag(FreezeVector.Y))
        {
            eulerAngles.y = fixedRotation.y;
        }
        if (freezeRotation.HasFlag(FreezeVector.Z))
        {
            eulerAngles.z = fixedRotation.z;
        }
        this.transform.eulerAngles = eulerAngles;

        // LocalScale
        var localScale = this.transform.localScale;
        if (freezeScale.HasFlag(FreezeVector.X))
        {
            localScale.x = fixedScale.x;
        }
        if (freezeScale.HasFlag(FreezeVector.Y))
        {
            localScale.y = fixedScale.y;
        }
        if (freezeScale.HasFlag(FreezeVector.Z))
        {
            localScale.z = fixedScale.z;
        }
        this.transform.localScale = localScale;
    }

    [CustomEditor(typeof(TransformEditor))]
    public class _Editor : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            var root = target as TransformEditor;
            serializedObject.Update();
            EditorGUI.BeginChangeCheck();

            EditorGUILayout.PropertyField(serializedObject.FindProperty(nameof(root.freezePosition)));
            EditorGUILayout.PropertyField(serializedObject.FindProperty(nameof(root.freezeRotation)));
            EditorGUILayout.PropertyField(serializedObject.FindProperty(nameof(root.freezeScale)));

            if(EditorGUI.EndChangeCheck())
            {
                root.fixedPosition = root.transform.position;
                root.fixedRotation = root.transform.eulerAngles;
                root.fixedScale = root.transform.localScale;
            }

            serializedObject.ApplyModifiedProperties();
        }
    }
#endif
}
