﻿using UnityEngine;
using System.Linq;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif

[DisallowMultipleComponent]
public class LockInspector : MonoBehaviour
{
    public const string PrefixKey = "LOCK_INSPECTOR";

#if UNITY_EDITOR
    [CustomEditor(typeof(LockInspector))]
    public class _Editor : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            if (EditorApplication.isPlaying) return;

            var isLock = PlayerPrefs.GetInt(PrefixKey, 1) == 1;

            var root = target as LockInspector;
            var go = EditorUtility.InstanceIDToObject(root.gameObject.GetInstanceID()) as GameObject;

            var array = go
				.GetComponentsInChildren<Transform>(true)
				.Select(c => c.gameObject)
				.ToArray();

            foreach (var tr in array)
            {
                if (!isLock)
                    tr.hideFlags &= ~HideFlags.NotEditable;
                else
                    tr.hideFlags |= HideFlags.NotEditable;
            }

            if (isLock)
                EditorGUILayout.HelpBox("ロックされています。", MessageType.Warning);
            else
                EditorGUILayout.HelpBox("ロックされていません。", MessageType.Info);

            serializedObject.ApplyModifiedProperties();
        }
    }
#endif
}

#if UNITY_EDITOR
public class EditorWindowLockInspector : EditorWindow
{
    [MenuItem("Custom/Set LockInspector")]
    private static void SetLockInspector()
    {
        var menuPath = "Tools/Set LockInspector";
        var num = PlayerPrefs.GetInt(LockInspector.PrefixKey, 1);
        var isChecked = num == 0;

        Menu.SetChecked(menuPath, isChecked);

        PlayerPrefs.SetInt(LockInspector.PrefixKey, num == 0 ? 1 : 0);
        PlayerPrefs.Save();
    }
}
#endif