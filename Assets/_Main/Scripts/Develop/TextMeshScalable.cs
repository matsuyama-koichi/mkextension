﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class TextMeshScalable : MonoBehaviour
{
		[SerializeField][Range(0, 6)] private float fontScale = 1;

		public float m_fontScale { set { fontScale = value; } get { return fontScale; } }
		const int fontSize = 128;

		float m_preSize;
		TextMesh m_textMesh;

		void Update()
		{
			if (m_preSize != m_fontScale)
			{
				m_preSize = m_fontScale;
				m_textMesh = m_textMesh ?? GetComponent<TextMesh>();
				if (m_textMesh == null) return;

				m_textMesh.fontSize = fontSize;
				var defaultScale = new Vector3(1, 1, 1) * m_fontScale;
				int m_fontSize = m_textMesh.fontSize;
				m_fontSize = m_fontSize == 0 ? 12 : m_fontSize;

				if (transform.parent?.GetComponent<TextMeshScalable>() == null)
				{
						float scale = 0.1f * 128 / m_fontSize;
						transform.localScale = defaultScale * scale;
				}
			}
		}
}
