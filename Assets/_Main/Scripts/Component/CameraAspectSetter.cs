﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DisallowMultipleComponent]
[RequireComponent(typeof(Camera))]
public class CameraAspectSetter : MonoBehaviour
{
    [SerializeField] private LookAtViewPoint _lookAtViewPoint;
    [SerializeField] private Vector2Int _fixScreen = new Vector2Int(640, 1136);
    private const float minScale = 1f;
    private const float maxScale = 2f;
    private const float minDistance = 0f;
    private const float maxDistance = 13f;

    public void SetUp() 
    {
        var aspect = (float)Screen.height / (float)Screen.width; //表示画面のアスペクト比
        var bgAcpect = (float)_fixScreen.y / (float)_fixScreen.x; //理想とするアスペクト比

        var bgScale = Mathf.Max(aspect / bgAcpect, 1f);
        if (bgScale > minScale)
        {
            var value = Mathf.InverseLerp(minScale, maxScale, bgScale);
            var distance = Mathf.Lerp(minDistance, maxDistance, value);

            _lookAtViewPoint.distance += distance;
            _lookAtViewPoint.UpdatePosition();
        }
    }
}