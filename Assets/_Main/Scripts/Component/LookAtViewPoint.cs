﻿using UnityEngine;
using DG.Tweening;
#if UNITY_EDITOR
using UnityEditor;
#endif

[DisallowMultipleComponent]
public sealed class LookAtViewPoint : MonoBehaviour
{
    public GameObject target; // an object to follow

    [HideInInspector] public Vector3 offset; // offset form the target object
    [HideInInspector] public float distance = 20f; // distance from following object
    [HideInInspector] public float polarAngle = 60f; // angle with y-axis
    [HideInInspector] public float azimuthalAngle = 270f; // angle with x-axis  

    private Vector3 _velocity = Vector3.zero;

    public const float MinPolarAngle = 1f;
    public const float MaxPolarAngle = 179f;

    private Vector3 GetPosition(Vector3 position, float distance, float polarAngle, float azimuthalAngle)
    {
        var da = azimuthalAngle * Mathf.Deg2Rad;
        var dp = polarAngle * Mathf.Deg2Rad;
        position.x += distance * Mathf.Sin(dp) * Mathf.Cos(da);
        position.y += distance * Mathf.Cos(dp);
        position.z += distance * Mathf.Sin(dp) * Mathf.Sin(da);
        return position;
    }

    public Vector3 GetPosition(Vector3 position) => this.GetPosition(position, this.distance, this.polarAngle, this.azimuthalAngle) + this.offset;
    public Vector3 GetPosition() => this.GetPosition(this.target.transform.position, this.distance, this.polarAngle, this.azimuthalAngle) + this.offset;

    private void UpdatePosition(Vector3 position, Vector3 offset, float distance, float polarAngle, float azimuthalAngle)
    {
        this.transform.position = this.GetPosition(position, distance, polarAngle, azimuthalAngle);
        this.transform.LookAt(position);
        this.transform.position += offset;
    }

    private void SmoothUpdatePosition(Vector3 position, Vector3 offset, float distance, float polarAngle, float azimuthalAngle, float smoothTime)
    {
        var getPosition = this.GetPosition(position, distance, polarAngle, azimuthalAngle);
        var targetPosition = Vector3.SmoothDamp(this.transform.position, getPosition, ref _velocity, smoothTime);
        this.transform.position = targetPosition;
        this.transform.LookAt(targetPosition);
        this.transform.position += offset;
    }

    public void UpdatePosition() => this.UpdatePosition(this.target.transform.position);
    public void UpdatePosition(GameObject go) => this.UpdatePosition(go.transform.position);
    public void UpdatePosition(Vector3 position) => this.UpdatePosition(position, this.offset, this.distance, this.polarAngle, this.azimuthalAngle);

    public void SmoothUpdatePosition(float smoothTime) => this.SmoothUpdatePosition(this.target.transform.position, smoothTime);
    public void SmoothUpdatePosition(GameObject go, float smoothTime) => this.SmoothUpdatePosition(go.transform.position, smoothTime);
    public void SmoothUpdatePosition(Vector3 position, float smoothTime) => this.SmoothUpdatePosition(position, this.offset, this.distance, this.polarAngle, this.azimuthalAngle, smoothTime);

    public Tween TweenChangeValueOffset(Vector3 offset, float time) => DOTween.To(() => this.offset, n => this.offset = n, offset, time).SetLink(this.gameObject);
    public Tween TweenChangeValueDistance(float distance, float time) => DOTween.To(() => this.distance, n => this.distance = n, distance, time).SetLink(this.gameObject);
    public Tween TweenChangeValuePolarAngle(float polarAngle, float time) => DOTween.To(() => this.polarAngle, n => this.polarAngle = n, polarAngle, time).SetLink(this.gameObject);
    public Tween TweenChangeValueAzimuthalAngle(float azimuthalAngle, float time) => DOTween.To(() => this.azimuthalAngle, n => this.azimuthalAngle = n, azimuthalAngle, time).SetLink(this.gameObject);

    #if UNITY_EDITOR
    [CustomEditor(typeof(LookAtViewPoint))]
    public class _Editor : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            var root = target as LookAtViewPoint;
            serializedObject.Update();

            EditorGUI.BeginChangeCheck();

            var m_offset = serializedObject.FindProperty(nameof(root.offset));
            var m_distance = serializedObject.FindProperty(nameof(root.distance));
            var m_polarAngle = serializedObject.FindProperty(nameof(root.polarAngle));
            var m_azimuthalAngle = serializedObject.FindProperty(nameof(root.azimuthalAngle));

            m_offset.vector3Value = EditorGUILayout.Vector3Field(nameof(root.offset), m_offset.vector3Value);
            m_distance.floatValue = EditorGUILayout.FloatField(nameof(root.distance), m_distance.floatValue);
            m_polarAngle.floatValue = EditorGUILayout.FloatField(nameof(root.polarAngle), Mathf.Repeat(m_polarAngle.floatValue, 360f));
            m_azimuthalAngle.floatValue = EditorGUILayout.FloatField(nameof(root.azimuthalAngle), Mathf.Repeat(m_azimuthalAngle.floatValue, 360f));

            if (EditorGUI.EndChangeCheck())
            {
                if (root.target != null)
                {
                    Undo.RecordObject(root.transform, null);

                    var position = root.target.transform.position;
                    root.UpdatePosition(position, m_offset.vector3Value, m_distance.floatValue, m_polarAngle.floatValue, m_azimuthalAngle.floatValue);
                }
            }

            serializedObject.ApplyModifiedProperties();
        }
    }
    #endif
}
