﻿using System;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Main
{
    public sealed class DataGame : CustomizeScritableObject<DataGame>
    {
        [Serializable]
        public class StageData
        {
            public int index { get; set; }
            public int stageNo { get; set; }
            public GameObject prefab;
        }

        [SerializeField] private List<StageData> _normalStageDatas;
        public IReadOnlyList<StageData> normalStageDatas => _normalStageDatas;

        private List<StageData> _randomStageDatas;
        public IReadOnlyList<StageData> randomStageDatas => _randomStageDatas;

        public static int MaxStageCount { get; private set; }

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
        private static void BeforeSceneLoad()
        {
            var dataGame = DataGame.instance;

            if (dataGame._normalStageDatas != null)
            {
                dataGame._randomStageDatas = new List<StageData>();

                for (var i = 0; i < dataGame._normalStageDatas.Count; i++)
                {
                    var stageData = dataGame._normalStageDatas[i];
                    stageData.index = i;
                    stageData.stageNo = i + 1;

                    MaxStageCount++;

                    dataGame._randomStageDatas.Add(stageData);
                }

                dataGame._randomStageDatas.Shuffle();
            }
        }

    #if UNITY_EDITOR
        [MenuItem ("Tools/CreateAsset/Create DataStage Instance")]
        static void Create ()
        {
            var asset = CreateInstance<DataGame> ();
            AssetDatabase.CreateAsset (asset, $"Assets/Resources/Data/{typeof(DataGame).Name}.asset");
            AssetDatabase.Refresh ();
        }
    #endif
    }
}

