﻿/// <summary>
/// レイヤーマスク番号を定数で管理するクラス
/// </summary>
public static class LayerMaskNo{
	public const int Default       = 1;
	public const int IgnoreRaycast = 4;
	public const int TransparentFX = 2;
	public const int UI            = 32;
	public const int Water         = 16;
}
