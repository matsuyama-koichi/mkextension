﻿/// <summary>
/// シーン番号を定数で管理するクラス
/// </summary>
public static class SceneNo{
	public const int Init = 0;
	public const int Main = 1;
}
