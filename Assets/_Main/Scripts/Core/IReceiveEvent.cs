﻿using UnityEngine.EventSystems;

namespace Main
{
    public interface IReceiveEvent : IEventSystemHandler
    {
        void OnReceive();
    }

    public interface IReceiveEventObject : IEventSystemHandler
    {
        void OnReceiveObject( object obj );
    }

    public interface IReceiveEventObjects : IEventSystemHandler
    {
        void OnReceiveObjects( params object[] args );
    }
}
