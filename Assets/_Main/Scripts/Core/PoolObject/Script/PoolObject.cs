using UnityEngine;
using System.Collections.Generic;

namespace Main.Pool
{
	public abstract class PoolObject<T> : MonoBehaviour 
	{
		public static GameObject original { get; private set; } = null;
		public static Stack<T> pools { get; private set; } = new Stack<T>();

		public static void SetOriginal( GameObject obj ) 
		{
			original = obj;
		}

		public static T Create( params object[] args )
		{
			T obj;
			
			if ( pools.Count > 0 )
			{
				obj = Pop();

				if ( obj as UnityEngine.Object == null )
				{
					Clear();
					obj = Instantiate<GameObject>( original ).GetComponent<T>();
				}
			}
			else
			{
				obj = Instantiate<GameObject>( original ).GetComponent<T>();
			}

			( obj as PoolObject<T> ).Init( args );

			return obj;
		}

		private static T Pop()
		{
			return pools.Pop();
		}

		public static void Pool( T obj, params object[] args )
		{
			( obj as PoolObject<T> ).Sleep( args );
			pools.Push( obj );
		}

		public static void Clear()
		{
			pools.Clear();
		}

		public abstract void Init( params object[] args );
		public abstract void Sleep( params object[] args );
	}
}