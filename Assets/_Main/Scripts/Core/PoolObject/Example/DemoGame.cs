﻿using System;
using System.Collections;
using UnityEngine;

namespace Main.Pool
{
	public class DemoGame : MonoBehaviour 
	{
		public GameObject m_bulletPrefab;

		void Awake()
		{
			DemoBullet.SetOriginal( m_bulletPrefab );
		}

		void Update()
		{
			if ( Input.GetMouseButtonDown(0) )
			{
				var bullet = DemoBullet.Create();
				var ray = Camera.main.ScreenPointToRay( Input.mousePosition );
				var speed = 1f;
				bullet.Shoot( ray.origin, ray.direction * speed );
			}
		}
	}
}