﻿using System.Collections;
using UnityEngine;

namespace Main.Pool
{
	public class DemoBullet : PoolObject<DemoBullet> 
	{
		int m_count = 0;
		private Vector3 m_velocity;

		// Update is called once per frame
		void Update () 
		{
			if ( m_count > 120)
			{
				DemoBullet.Pool(this);
			}
			transform.Translate( m_velocity );
			m_count++;
		}

		public void Shoot( Vector3 pos, Vector3 velocity ) 
		{
			m_velocity = velocity;
			transform.position = pos;
		}

		public override void Init( params object[] args )
		{
			m_count = 0;
			gameObject.SetActive( true );
		}

		public override void Sleep( params object[] args )
		{
			gameObject.SetActive( false );
		}
	}
}