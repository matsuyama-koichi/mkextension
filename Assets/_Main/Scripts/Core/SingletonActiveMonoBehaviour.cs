﻿using UnityEngine;

public abstract class SingletonActiveMonoBehaviour<T> : MonoBehaviour where T : MonoBehaviour
{
    private static T _instance = null;
    public static T instance
    {
        get
        {
            if (_instance == null)
                _instance = (T)FindObjectOfType(typeof(T));
            else
                if (!_instance.isActiveAndEnabled)
                    _instance = (T)FindObjectOfType(typeof(T));
            return _instance;
        }
    }

    public void Destroy()
    {
        _instance = null;
    }
}