﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

public abstract class CustomizeScritableObject<T> : ScriptableObject where T : ScriptableObject
{
    public const string ResourcesPath = "Data/";

    private static T _instance = null;
    public static T instance
    {
        get
        {
            if (_instance == null)
                _instance = Resources.Load<T>($"{ResourcesPath}{typeof(T).Name}");
            return _instance;
        }
    }
}
