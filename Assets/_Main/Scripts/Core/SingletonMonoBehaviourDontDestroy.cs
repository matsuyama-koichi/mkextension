﻿using UnityEngine;

public abstract class SingletonMonoBehaviourDontDestroy<T> : MonoBehaviour where T : MonoBehaviour
{
    private static T _instance = null;
    public static T instance
    {
        get
        {
            if (_instance == null) 
                _instance = (T)FindObjectOfType(typeof(T));
            return _instance;
        }
    }

    protected virtual void Awake()
    {
        if (_instance != null)
        {
            Destroy(this.gameObject);
            return;
        }
        _instance = this as T;
        DontDestroyOnLoad(this.gameObject);
    }

    public void Destroy()
    {
        _instance = null;
    }
}
