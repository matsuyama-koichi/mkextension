﻿using UnityEngine;

namespace Main
{
    using Manager;
    
    public sealed class Init : MonoBehaviour
    {
        public static bool isInitialized { get; private set; }

        private void Start()
        {
            isInitialized = true;
            SceneManager.instance.Load(SceneName.Main);
        }
    }
}