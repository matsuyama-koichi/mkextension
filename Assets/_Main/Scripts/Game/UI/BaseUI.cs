﻿using UnityEngine;

namespace Main
{
    public abstract class BaseUI<T> : MonoBehaviour where T : MonoBehaviour
    {
        private static T _instance = null;
        public static T instance
        {
            get
            {
                if (_instance == null)
                    _instance = (T)FindObjectOfType(typeof(T));
                return _instance;
            }
        }
        protected GameObject root { get; private set; } = null;

        protected virtual void Awake()
        {
            if (this.transform.Find(nameof(root)) != null)
                root = this.transform.Find(nameof(root)).gameObject;
        }

        public virtual void Show()
        {
            root?.SetActive(true);
        }

        public virtual void Hide()
        {
            root?.SetActive(false);
        }
    }
}