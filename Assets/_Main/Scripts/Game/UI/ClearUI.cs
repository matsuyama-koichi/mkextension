﻿using UnityEngine;
using UnityEngine.UI;
using System.Threading;
using Cysharp.Threading.Tasks;
using UniRx;

namespace Main
{
    public sealed class ClearUI : BaseUI<ClearUI>
    {
        [SerializeField] private Button _buttonNext;

        private void Start()
        {
            base.Hide();

            _buttonNext
                .OnClickAsObservable()
                .Take(1)
                .Subscribe(_ => 
            {
                MainController.instance.Next();
            })
            .AddTo(this);
        }

        public async UniTask ShowTask(CancellationToken ct)
        {
            await UniTask.Delay(System.TimeSpan.FromSeconds(0.5f), cancellationToken: ct);
            base.Show();
        }
    }
}