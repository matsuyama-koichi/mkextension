﻿using UnityEngine;
using UnityEngine.UI;
using UniRx;

namespace Main
{
    using Manager;
    
    public sealed class MainUI : BaseUI<MainUI>
    {
        [SerializeField] private Text _textLevel;
        [SerializeField] private Button _buttonStart, _buttonRetry;

        private void Start()
        {
            _textLevel.text = $"LEVEL {GameManager.instance.levelNo}";

            _buttonStart
                .OnClickAsObservable()
                .Take(1)
                .Subscribe(_ => 
                {
                    _buttonStart.SetActive(false);
                    MainController.instance.GameStart();
                })
                .AddTo(this);

            _buttonRetry
                .OnClickAsObservable()
                .Take(1)
                .Subscribe(_ => 
                {
                    MainController.instance.Retry();
                })
                .AddTo(this);
        }
    }
}