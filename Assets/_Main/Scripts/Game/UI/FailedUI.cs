﻿using UnityEngine;
using UnityEngine.UI;
using System.Threading;
using Cysharp.Threading.Tasks;
using UniRx;

namespace Main
{
    public sealed class FailedUI : BaseUI<FailedUI>
    {
        [SerializeField] private Button _buttonRetry;

        private void Start()
        {
            base.Hide();
            
            _buttonRetry
                .OnClickAsObservable()
                .Take(1)
                .Subscribe(_ => 
            {
                MainController.instance.Retry();
            })
            .AddTo(this);
        }

        public async UniTask ShowTask(CancellationToken ct)
        {
            await UniTask.Delay(System.TimeSpan.FromSeconds(0.5f), cancellationToken: ct);
            base.Show();
        }
    }
}