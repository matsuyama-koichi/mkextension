﻿using UnityEngine;

namespace Main
{
    public sealed class MainCamera : SingletonMonoBehaviour<MainCamera>
    {
        [SerializeField] private LookAtViewPoint _lookAtViewPoint;
        public LookAtViewPoint lookAtViewPoint => _lookAtViewPoint;
    }
}