using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading;
using Cysharp.Threading.Tasks;

namespace Main
{
    public sealed class SampleGame : BaseGame
    {
        public override async UniTask<bool> MainTask(CancellationToken ct)
        {
            await UniTask.WaitUntil(() => MainController.instance.isGameStart, cancellationToken: ct);

            return true;
        }
    }
}

