﻿using System.Threading;
using Cysharp.Threading.Tasks;
using UnityEngine;
using DG.Tweening;

namespace Main
{
    using Manager;

    public sealed class MainController : SingletonMonoBehaviour<MainController>
    {
        public bool isClear { get; private set; } = false;
        public bool isFailed { get; private set; } = false;
        public bool isClearOrFailed => isClear | isFailed;
        public bool isGameStart { get; private set; } = false;
        private Stage _stage;

        private void Awake() 
        {
            if (!Init.isInitialized)
            {
                SceneManager.instance.Load(SceneName.Init);
                return;
            }

            Application.targetFrameRate = 60;
            Screen.orientation = ScreenOrientation.Portrait;      

            GameManager.instance.Initialize();
        }

        private void OnEnable() 
        {
            _stage = GameManager.instance.GetStage();
        }

        private async void Start()
        {
            var ct = this.GetCancellationTokenOnDestroy();
            await UniTask.WaitUntil(() => _stage != null, cancellationToken: ct);

            try
            {
                var isClear = await _stage.MainTask(ct);
                if (isClear)
                    await this.ClearTask(ct);
                else
                    await this.FailedTask(ct);
            }
            catch
            {
                return;
            }
        }

        private void OnDestroy() 
        {
            DOTween.KillAll();
        }

        public void GameStart()
        {
            if (isGameStart)
                return;

            isGameStart = true;

            GameManager.instance.Play();
        }

        private async UniTask ClearTask(CancellationToken ct)
        {
            if (isClearOrFailed)
                return;

            isClear = true;

            GameManager.instance.Clear();

            await ClearUI.instance.ShowTask(ct);
        }

        public async UniTask FailedTask(CancellationToken ct)
        {
            if (isClearOrFailed)
                return;

            isFailed = true;

            GameManager.instance.Failed();

            await FailedUI.instance.ShowTask(ct);
        }

        public void Next()
        {
            SceneManager.instance.Reload();
        }

        public void Retry()
        {
            SceneManager.instance.Reload();
        }
    }
}
