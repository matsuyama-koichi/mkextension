using UnityEngine;
using System.Threading;
using Cysharp.Threading.Tasks;

namespace Main
{
    public abstract class BaseGame : MonoBehaviour
    {
        public enum GameType { MoveIKGame }
        public GameType gameType { get; protected set; }

        public abstract UniTask<bool> MainTask(CancellationToken ct);
    }
}