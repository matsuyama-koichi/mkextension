﻿using UnityEngine;
using System.Collections.Generic;
using System.Threading;
using Cysharp.Threading.Tasks;

namespace Main
{
    public sealed class Stage : SingletonMonoBehaviour<Stage>
    {
        [SerializeField] private List<BaseGame> _games;
        public IReadOnlyList<BaseGame> games => _games;

        public BaseGame currentGame { get; private set; }

        private void Awake()
        {
            currentGame = games[0];
        }

        public async UniTask<bool> MainTask(CancellationToken ct)
        {
            foreach (var game in games)
            {
                currentGame = game;
                if (currentGame == null)
                    return true;
                var isClear = await currentGame.MainTask(ct);
                if (!isClear)
                    return false;
            }
            return true;
        }
    }
}