﻿using UnityEngine;

namespace Main.Manager
{
    public sealed class ManagerSetter : MonoBehaviour
    {
        private static ManagerSetter _instance = null;

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
        private static void Create()
        {
            GameObject.Instantiate(Resources.Load<GameObject>(typeof(ManagerSetter).Name)).name = "ManagerSetter";
        }

        private void Awake()
        {
            if (_instance != null)
            {
                GameObject.Destroy(this.gameObject);
                return;
            }
            _instance = this;
            GameObject.DontDestroyOnLoad(this.gameObject);
        }
    }
}