﻿using System;
using System.Threading;
using Cysharp.Threading.Tasks;
using UnityEngine;
using DG.Tweening;

namespace Main.Manager
{
    public sealed class FadeManager : SingletonMonoBehaviourDontDestroy<FadeManager>
    {        
        private Texture2D _texture;
        private float _color = 0f;
        private float _alpha = 0f;
        private Tween _twFade = null;
        public enum ColorType { Black, White }
        
        protected override void Awake()
        {
            base.Awake();

            _texture = new Texture2D(1, 1, TextureFormat.ARGB32, false);
            _texture.SetPixel(0, 0, Color.white);
            _texture.Apply();
        }

        private void OnGUI()
        {
            if(_alpha <= 0) 
                return;

            GUI.color = new Color(_color, _color, _color, _alpha);
            GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), _texture);
        }

        public async UniTask FadeInTask(ColorType colorType, float seconds, CancellationToken ct)
        {
            await this.FadeToTask(colorType, seconds, 1f, 0f, ct);
        }

        public async UniTask FadeOutTask(ColorType colorType, float seconds, CancellationToken ct)
        {
            await this.FadeToTask(colorType, seconds, 0f, 1f, ct);
        }

        private async UniTask FadeToTask(ColorType colorType, float seconds, float startValue, float endValue, CancellationToken ct)
        {
            _twFade?.Kill();
            _color = colorType == ColorType.Black ? 0f: 1f;
            _alpha = startValue;
            _twFade = DOTween.To(() => _alpha, n => _alpha = n, endValue, seconds).SetEase(Ease.Linear);
            await _twFade.AddTo(ct);
        }
    }
}


