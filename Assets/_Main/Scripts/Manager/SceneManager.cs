﻿using UnityEngine;
using DG.Tweening;

namespace Main.Manager
{
    using UnityEngine.SceneManagement;
    using sm = UnityEngine.SceneManagement.SceneManager;
    
    public sealed class SceneManager : SingletonMonoBehaviourDontDestroy<SceneManager>
    {
        public enum LoadMode { Single, Additive }

        public string activeName { get { return sm.GetActiveScene().name; } }
        public int activeIndex { get { return sm.GetActiveScene().buildIndex; } }

        private static string m_prevName = null;
        private static int m_prevIndex = 0;
        public string prevName { get { return m_prevName; } }
        public int prevIndex { get { return m_prevIndex; } }

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
        static void Init()
        {
            m_prevName = sm.GetActiveScene().name;
            m_prevIndex = sm.GetActiveScene().buildIndex;
        }

        private void Start()
        {
            // アクティブなシーンが変更されたとき
            sm.activeSceneChanged += (prevScene, nextScene) => 
            {
                m_prevName = prevScene.name;
                m_prevIndex = prevScene.buildIndex;
            };
            // シーンが読み込まれたとき
            sm.sceneLoaded += (scene, mode) => 
            {
            };
            // シーンが破棄されたとき
            sm.sceneUnloaded += scene => 
            {
            };
        }

        public void Load( int index, LoadMode mode = LoadMode.Single )
        {
            sm.LoadScene(index, mode == LoadMode.Single ? LoadSceneMode.Single : LoadSceneMode.Additive);
        }

        public void Load( string name, LoadMode mode = LoadMode.Single )
        {
            sm.LoadScene(name, mode == LoadMode.Single ? LoadSceneMode.Single : LoadSceneMode.Additive);
        }

        public void Load( int index, float delay, LoadMode mode = LoadMode.Single )
        {
            DOVirtual.DelayedCall( delay, () => Load(index, mode) );
        }

        public void Load( string name, float delay, LoadMode mode = LoadMode.Single )
        {
            DOVirtual.DelayedCall( delay, () => Load(name, mode) );
        }

        public void Reload( LoadMode mode = LoadMode.Single )
        {
            Load(activeIndex, mode);
        }

        public void Reload( float delay, LoadMode mode = LoadMode.Single )
        {
            DOVirtual.DelayedCall( delay, () => Reload(mode) );
        }
    }
}
