﻿using UnityEngine;
using System.Threading;
using Cysharp.Threading.Tasks;

namespace Main.Manager
{
    public sealed class GameManager : SingletonMonoBehaviourDontDestroy<GameManager>
    {
        public int levelNo { get; private set; } = MinLevelNo;
        public int stageNo { get; private set; } = MinStageNo;

        public const int MinLevelNo = MinStageNo;
        public const int MaxLevelNo= 100;
        public const int MinStageNo = 1;

        private bool isInitialized = false;

        protected override void Awake()
        {
            base.Awake();
        }

        public void Initialize()
        {
            if (!isInitialized)
            {
                levelNo = SaveManager.instance.PlayLevelNo();
                isInitialized = true;
            }
            else
                levelNo = this.GetNextLevelNo();
            
            stageNo = this.GetStageNo();
        }

        public void Play()
        {
        }

        public void Clear()
        {
            SaveManager.instance.SaveLevelNo(levelNo);
        }

        public void Failed()
        {
        }

        public Stage GetStage()
        {
            var stage = Stage.instance;

            if (stage == null)
            {
                var prefab = levelNo <= DataGame.MaxStageCount ?
                    DataGame.instance.normalStageDatas[stageNo - 1].prefab :
                    DataGame.instance.randomStageDatas[stageNo - 1].prefab;

                if (prefab != null)
                {
                    stage = GameObject.Instantiate(prefab).GetComponent<Stage>();
                    stage.transform.position = Vector3.zero;
                }

                stage.name = prefab.name;
            }
            return stage;
        }

        private int GetNextLevelNo()
        {
            var no = levelNo;
            no++;
            if (no > MaxLevelNo)
                no = MinLevelNo;
            return no;
        }

        private int GetStageNo()
        {
            var no = stageNo;
            if (levelNo <= DataGame.MaxStageCount)
                return no = (levelNo - 1) % DataGame.MaxStageCount + 1;
            else
                return no = (levelNo - 1) % DataGame.instance.randomStageDatas.Count + 1;
        }

#if UNITY_EDITOR
        private void Update() 
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                if (!UnityEditor.EditorApplication.isPaused)
                    UnityEditor.EditorApplication.isPaused = true;
            }
            if (Input.GetKeyDown(KeyCode.R))
            {
                MainController.instance.Retry();
            }
        }
#endif
    }
}
