﻿using UnityEngine;

namespace Main.Manager
{
    public sealed class SaveManager : SingletonMonoBehaviourDontDestroy<SaveManager>
    {
        public int PlayLevelNo() => ES3.Load<int>(GlobalParams.SaveKey.MainGame.PLAY_LEVEL_NO, GameManager.MinLevelNo);

        protected override void Awake() 
        {
            base.Awake();

            // DeleteData();
        }

        public void DeleteData()
        {
            #if DEVELOPMENT_BUILD || UNITY_EDITOR
            ES3.DeleteFile();
            #endif
        }

        public void SaveLevelNo(int levelNo)
        {
            ES3.Save<int>(GlobalParams.SaveKey.MainGame.PLAY_LEVEL_NO, levelNo);
        }
    }
}